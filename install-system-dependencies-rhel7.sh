#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                                 ESP Health
#                     System-wide Package Install Script
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# @author: Jason McVetta <jason.mcvetta@gmail.com>
# @organization: Channing Laboratory - http://www.channing.harvard.edu
# @contact: http://esphealth.org
# @copyright: (c) 2009-2011 Channing Laboratory
# @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Run this script to install system-wide dependencies - i.e. those which must
# be installed using the OS's package manager, rather than being installed to
# the local Python virtual environment by Pip.
#
# Currently this script only supports recent versions of Ubuntu.  Eventually it
# may support other OSes.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo "installing required redhat-lsb pkg"
echo 
sudo yum install redhat-lsb -y
_pkg_manager=''
_pkg_list=''
_os=`lsb_release -sr`

echo
echo Detected OS: $_os
echo

#
# Check Operating System
#
case $_os in
    "7.6" | "7.8" )
        _pkg_manager='yum'
        _pkg_list="epel_release git gcc httpd mod_wsgi wget python-virtualenv python3-setuptools python-devel postgresql96 postgresql96-server postgresql96-contrib postgresql96-libs pgadmin3 swatch"
        ;;
esac

#
# Install Packages
#
if [ -n "$_pkg_manager" ]; then
    echo "Automatic system package installation is supported!"
    echo
    echo "Installing packages:"
    echo "    $_pkg_list"
    echo ""
else
    echo ERROR:
    echo ""
    echo "This script currently supports only RedHat Enterprise Linux 7.6"
    echo " if this is Rhel7.6 - verify that the redhat-lsb pkg is Installed"
    echo ""
    exit 1
fi

case $_os in
    "7.6" )
        echo "adding the postgres rpm for 7.6"
        echo ""
        sudo yum localinstall https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-redhat96-9.6-3.noarch.rpm -y
	sudo yum localinstall https://epel.mirror.constant.com/7/x86_64/Packages/e/epel-release-7-11.noarch.rpm -y
        ;;
    "7.8" )
        echo "adding the postgres rpm for 7.8"
        echo ""
        sudo yum install https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm -y
        sudo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
        ;;
esac

case $_pkg_manager in
    "yum" )
        # Update package list
        echo "Refreshing yum package list..."
        echo "" 
        sudo yum update -y
        echo 'Installing yum packages...'
        echo "" 
        # Install Redhat packages
        sudo yum install $_pkg_list -y
        echo "done"
        echo "" 
        ;;
    * )
        echo "Internal error - unknown package manager"
        exit 2
        ;;
esac
