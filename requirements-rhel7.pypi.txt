#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                    Electronic Support for Public Health
#                              PIP Requirements
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#-------------------------------------------------------------------------------
#
# Packages from PyPI
#
#-------------------------------------------------------------------------------


psycopg2-binary      # Database Adapter (changed to binary for redhat)
python-dateutil      # Date-math utilities
sqlparse             # SQL pretty-printer
hl7                  # Utilities for working with HL7 message
django_tables2     # Utilities for working with HTML tables in Django
configobj            # Advanced configuration file support
#futures              # Python implementation of "futures" threading paradigm
paramiko             # SSH2 protocol library
#cryptography <= 1.8  # not needed ?
django == 2.1.15      # Web application framework with ORM
pandas           # date parsing for reading dates from CDAs
paramiko           # SSH2 protocol library
djangorestframework	# Django REST Framework
djangorestframework-xml	# REST Framework XML
pyodbc
django-pyodbc-azure
