#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                                 ESP Health
#                     System-wide Package Install Script
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# @author: Jason McVetta <jason.mcvetta@gmail.com>
# @organization: Channing Laboratory - http://www.channing.harvard.edu
# @contact: http://esphealth.org
# @copyright: (c) 2009-2011 Channing Laboratory
# @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Run this script to install system-wide dependencies - i.e. those which must
# be installed using the OS's package manager, rather than being installed to
# the local Python virtual environment by Pip.
#
# Currently this script only supports recent versions of Ubuntu.  Eventually it
# may support other OSes.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_pkg_manager=''
_pkg_list=''
_os=`lsb_release -ds`

echo
echo Detected OS: $_os
echo

#
# Check Operating System
#
case $_os in
    "Ubuntu 18.04.1 LTS" | "Ubuntu 18.04.2 LTS" | "Ubuntu 18.04.3 LTS" | "Ubuntu 18.04.4 LTS" )
        _pkg_manager='apt'
        _pkg_list="python-virtualenv python-pip python-setuptools python-dev python3-dev apache2 libapache2-mod-wsgi-py3 libpq-dev postgresql-client postgresql-9.6 postgresql-server-dev-9.6 unixodbc-dev"
        ;;
    "Ubuntu 16.04.1 LTS" | "Ubuntu 16.04.2 LTS" | "Ubuntu 16.04.3 LTS" )
        _pkg_manager='apt'
        _pkg_list="python-virtualenv python-pip python-setuptools python-dev libpq-dev postgresql-client postgresql-9.5 postgresql-server-dev-9.5"
        ;;
    "Ubuntu 11.10" | "Ubuntu 14.04.1 LTS" | "Ubuntu 12.04 LTS" | "Ubuntu 12.04.5 LTS" | "Ubuntu 12.04.4 LTS" | "Ubuntu 12.04.1 LTS" | "Ubuntu 12.04.2 LTS" )
        _pkg_manager='apt'
        _pkg_list="python-virtualenv python-pip python-setuptools python-dev libpq-dev postgresql-client postgresql-9.3 postgresql-server-dev-9.3 subversion"
        ;;
    "Ubuntu 10.04.4 LTS" | "Ubuntu 11.04" )
        _pkg_manager='apt'
        _pkg_list="python-virtualenv python-pip python-setuptools python-dev libpq-dev subversion"
        ;;
esac

#
# Install Packages
#
if [ -n "$_pkg_manager" ]; then
    echo "Automatic system package installation is supported!"
    echo
    echo Installing packages:
    echo "    $_pkg_list"
    echo
else
    echo ERROR:
    echo
    echo Automatic installation of system-wide dependencies is currently supported only for Ubuntu Linux.
    echo
    exit 1
fi

case $_os in
    "Ubuntu 18.04.1 LTS" | "Ubuntu 18.04.2 LTS" | "Ubuntu 18.04.3 LTS" | "Ubuntu 18.04.4 LTS" )
        #Add the postgres key to the apt sources keyring
        echo 'adding the postgres apt sources key'
	sudo wget -q http://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
        #Add postgresql to apt sources
        echo 'adding postgres to the sources list'
        sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
        ;;
esac

case $_pkg_manager in
    "apt" )
        # Update package list
        echo 'Refreshing apt package list...'
        sudo apt-get update -q
        echo 'Installing apt packages...'
        # Install Ubuntu packages
        sudo apt-get install -q -y $_pkg_list
        ;;
    * )
        echo "Internal error - unknown package manager"
        exit 2
        ;;
esac
