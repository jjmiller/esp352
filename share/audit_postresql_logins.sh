#!/bin/bash
###########################################################################
## This script checks PostreSQL logs for failed logins based on the
## GREPFORLIST and sends an email to the MAILLIST
##
## Please update values for:
## GREPFORLIST, LOGDATE, PLOGDIR, MAILLIST & AUDITLOG
##
## Version 1.03  - 03-05-2019   J. Miller
##########################################################################
## sample Cron entry for "yesterday" - run at 12:05AM every day (default)
## 05 00 * * * /srv/esp30/scripts/audit_postgresql_logins.sh
## sample Cron entry for "today" - run at 11:59PM every day
## 23 59 * * * /srv/esp30/scripts/audit_postgresql_logins.sh
##########################################################################
## GREPVLIST OPTION - to exclued text - use GREPVLIST VAR or file
## set GREPVLIST then run the command - verify your results!
## GREPVLIST=`cat /srv/esp30/scripts/.goodlist_postgres`
## GREPVLIST="(connection authorized|connection received)"
## grep -E $GREPFORLIST" $PLOGDIR/postgresql-$LOGDATE.log | grep -Ev "$GREPVLIST" >> $AUDITLOG
##########################################################################
##
## 1 - UPDATE GREPFORLIST varable below to add to intial search if required
## note there is a space in front of " connection" below.. this is needed!
GREPFORLIST="(failed|FATAL)"
##
## 2 - UPDATE LOGDATE to correspond to date in cron job - today or yesterday
## Ex. LOGDATE="`date +%F`"
LOGDATE="`date --date='yesterday' +%F`"
##
## 3 - UPDATE PLOGDIR with the location to store logs from this script
## when possible use the same directory as the Postgres Audit logs
PLOGDIR="/var/lib/pgsql/9.2/data/pg_log"
##
## 4 - UPDATE MAILLIST to send daily email to appropriate support personnel
#MAILLIST="esp_support@commoninf.com,jmiller@commoninf.com"
MAILLIST="jmiller@commoninf.com"
##
## AUDITLOG - should not need to change this - it will roll monthly
AUDITLOG="$PLOGDIR/PostrgreSQLAudit.`date --date='yesterday' +%b%Y`.log"
##
## Create the AUDITLOG if its not there, echo startline into the local log
if [ -a $AUDITLOG ]
then
echo "Running Postres Login check for $LOGDATE" >> $AUDITLOG
else
touch $AUDITLOG
echo " Running Postres Login check for $LOGDATE" >> $AUDITLOG
fi
##DEBUG echo "PLOGDIR =$PLOGDIR - LOGDATE =$LOGDATE - AUDITLOG =$AUDITLOG"
##DEBUG echo "grep -EB 1 $GREPFORLIST $PLOGDIR/postgresql-$LOGDATE.log | tee -a $AUDITLOG | mailx -s Atrius Daily Postgres Login Audit $MAILLIST "
## run the command and append it to local log and mail the results
grep -EB 1 "$GREPFORLIST" $PLOGDIR/postgresql-$LOGDATE.log | tee -a $AUDITLOG | mailx -s "Atrius Daily Postgres Login Audit" $MAILLIST
echo "##" >> $AUDITLOG