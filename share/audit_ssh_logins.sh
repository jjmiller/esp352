#!/bin/bash
###########################################################################
## This script checks System logs for failed logins based on the
## GREPFORLIST and sends an email to the MAILLIST
##
## Please update values for:
## GREPFORLIST, LOGDATE, SYSLOG, PLOGDIR, MAILLIST & AUDITLOG
##
## Version 1.0.3  -  3-05-2019   J. Miller
##########################################################################
## sample Cron entry for - run at 11:59PM every day - adjust as needed
## 23 59 * * * /srv/esp30/scripts/audit_ssh_logins.sh
##########################################################################
##
## 1 - UPDATE GREPFORLIST varable below to add to intial search if required
GREPFORLIST="(Failed|FATAL)"
##
## 2 - UPDATE LOGDATE to correspond to date in cron job - today or yesterday
## Ex. LOGDATE="`date --date='yesterday' +%F`"
LOGDATE="`date +%F`"
##
## 3 - UPDATE SYSLOG with the location of the audit log file to check
## this can be changed for daily logs, etc..
## Ex. SYSLOG="/var/log/audit/audit.$LOGDATE.log"
SYSLOG="/var/log/secure"
##
## 4 - UPDATE PLOGDIR with the location to store logs from this script
## when possible use the same directory as the Postgres Audit logs
PLOGDIR="/var/lib/pgsql/9.2/data/pg_log"
##
## 5 - UPDATE MAILLIST to send daily email to appropriate support personnel
MAILLIST="esp_support\@commoninf.com,jmiller\@commoninf.com"
##
## AUDITLOG - should not need to change this - it will roll monthly
AUDITLOG="$PLOGDIR/ESPSytemAudit.`date +%b%Y`.log"
##
## Create the AUDITLOG if its not there, echo startline into the local log
if [ -a $AUDITLOG ]
then
echo "Running System Audit check for $LOGDATE" >> $AUDITLOG
else
touch $AUDITLOG
echo " Running System Audit check for $LOGDATE" >> $AUDITLOG
fi
## run the command and append it to local log
grep -E "$GREPFORLIST" $SYSLOG | tee -a $AUDITLOG | mailx -s "Atrius Daily System Audit" $MAILLIST
echo "##" >> $AUDITLOG