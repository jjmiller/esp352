import curses
import logging
import os
import stat
import sys
from curses import panel


class Menu(object):

    def __init__(self, items, stdscreen, x=0, y=0):
        self.window = stdscreen.subwin(x, y)
        self.window.keypad(1)
        self.panel = panel.new_panel(self.window)
        self.panel.hide()
        panel.update_panels()
        self.position = 0
        self.items = self.addlastitem(items)

    def addlastitem(self, items):
        if type(self).__name__ == "Menu":
            items.append(('Perform selected actions', 'exit'))
        return items

    def navigate(self, n):
        self.position += n
        if self.position < 0:
            self.position = 0
        elif self.position >= len(self.items):
            self.position = len(self.items) - 1

    def display(self):
        self.panel.top()
        self.panel.show()
        self.window.clear()

        while True:
            self.window.refresh()
            curses.doupdate()
            for index, item in enumerate(self.items):
                if index == self.position:
                    mode = curses.A_REVERSE
                else:
                    mode = curses.A_NORMAL

                try:
                    msg = item[0].row_element()
                except AttributeError:
                    msg = item[0]

                try:
                    self.window.addstr(1 + index, 1, msg, mode)
                except:
                    pass

            key = self.window.getch()

            if key in [curses.KEY_ENTER, ord('\n')]:
                if self.position == len(self.items) - 1:
                    installation = Installer(self.items)
                    installation.run()
                    break
                else:
                    # these are calls to submenu, and so takes the current item list, updates, and returns.
                    try:
                        self.items[self.position][1](self.items, self.items[self.position][0])
                    except TypeError:
                        pass
                    self.window.refresh()
                    self.panel.top()
                    self.panel.show()

            elif key == curses.KEY_UP:
                self.navigate(-1)

            elif key == curses.KEY_DOWN:
                self.navigate(1)

        self.window.clear()
        self.panel.hide()
        panel.update_panels()
        curses.doupdate()


class Submenu(Menu):
    def display(self, mainItemset, selectedMainItem):
        self.panel.top()
        self.panel.show()
        self.window.clear()

        while True:
            self.window.refresh()
            curses.doupdate()
            for index, item in enumerate(self.items.ACTIONS.keys()):
                if index == self.position:
                    mode = curses.A_REVERSE
                else:
                    mode = curses.A_NORMAL

                msg = '%d. %s' % (index, item)
                self.window.addstr(1 + index, 1, msg, mode)

            key = self.window.getch()

            if key in [curses.KEY_ENTER, ord('\n')]:
                if self.position == len(list(self.items.ACTIONS.keys())):
                    break
                else:  # get the selected value and update items
                    selectedMainItem.action = list(selectedMainItem.ACTIONS.keys())[self.position]
                    i = 0
                    for ItemTup in mainItemset:
                        if ItemTup[0] == selectedMainItem:
                            mainItemset[i] = (
                                ItemTup[0], self.items)
                            break
                        i += 1
                    break

            elif key == curses.KEY_UP:
                self.navigate(-1)

            elif key == curses.KEY_DOWN:
                self.navigate(1)

        self.window.clear()
        self.panel.hide()
        panel.update_panels()
        curses.doupdate()

    def navigate(self, n):
        self.position += n
        if self.position < 0:
            self.position = 0
        elif self.position >= len(list(self.items.ACTIONS.keys())):
            self.position = len(list(self.items.ACTIONS.keys())) - 1


class Installer(object):

    def __init__(self, items):
        self.items = items
        if "Perform" in self.items[-1][0]:
            self.items = self.items[:-1]

    def run(self):
        instfile = open('./tempSetup/installplugins.txt', 'w')
        uninstfile = open('./tempSetup/uninstallplugins.txt', 'w')
        rmfile = open('./tempSetup/rmPlugins.sh', 'w')
        rmfile.write('#!/bin/bash\n')
        for item in self.items:
            plugin = item[1]
            try:
                if plugin.action in ("Install", "Update"):
                    instfile.write(plugin.do())
                elif plugin.action == "Remove":
                    dirname = './src/esp-plugin-{}'.format(plugin.name)
                    uninstall_name = [file for file in os.listdir(dirname) if ".egg-info" in file][0].replace(
                        ".egg-info", "").replace("_", "-")
                    uninstfile.write('{}\n'.format(uninstall_name))
                    rmfile.write('rm -rf ' + dirname)
            except AttributeError:
                pass
        instfile.close()
        uninstfile.close()
        rmfile.close()
        st = os.stat('./tempSetup/rmPlugins.sh')
        os.chmod('./tempSetup/rmPlugins.sh', st.st_mode | stat.S_IEXEC)
        return


class Compose(object):

    def __init__(self, stdscr=None):
        if stdscr is not None:
            self.screen = stdscr
            curses.curs_set(0)
        self.make_menu()

    def make_menu(self):
        menuItemDict = {}
        infile = open('./tempSetup/instUpdtPlugins.txt')
        for line in infile:
            if line.split(',')[0] == '':
                continue
            plugin = Plugin(line.replace("\n", ""))
            menuItemDict[plugin.name] = plugin

        items = []
        submenus = []
        i = 0
        for key in sorted(menuItemDict.keys()):
            menu_item = menuItemDict[key]
            submenus.append(Submenu(menu_item, self.screen, i, len(menu_item.row_element())))
            items.append((menu_item, submenus[i].display))
            i += 1
        main_menu = Menu(items, self.screen)
        main_menu.display()


class Plugin(object):

    def __init__(self, details):
        self.name, self.status, self.target = [x.strip() for x in details.split(',')]
        self.ACTIONS = self.pick_actions(
            {"Nothing": None, "Install": self.install_update(), "Update": self.install_update(),
             "Remove": self.remove()})
        self.action = "Nothing"

    def pick_actions(self, actions):
        if self.status == "not":
            del actions["Update"]
            del actions["Remove"]
        elif self.status == self.target or self.target == "Current":
            del actions["Update"]
            del actions["Install"]
        elif not self.status == self.target:
            del actions["Install"]

        return actions

    def row_element(self, offset=30):
        return "{}:{spacing}{}\t{}\t\tACTION: {}".format(self.name.upper(),
                                                       "{} installed".format(self.status),
                                                       self.target,
                                                       self.action,
                                                       spacing='    ' + ' ' * (offset - len(self.name)))

    def do(self):
        return self.ACTIONS[self.action]

    def install_update(self):
        return '-e git+https://gitlab.com/ESP-Project/plugins/esp-plugin_{plugname}.git@{tag}#egg=esp-plugin_{plugname}\n'.format(
            plugname=self.name, tag=self.target)

    def remove(self):
        pass


if __name__ == '__main__':
    logging.basicConfig(filename='sp.log', level=logging.DEBUG, filemode='w')
    curses.wrapper(Compose)
