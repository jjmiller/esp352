#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                                 ESP Health
#                                Setup Script
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This script must be run by a user with write permission on the 'esphealth'
# folder.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

USAGE_MSG="usage: install.sh [option]

options:
-?  Show this usage message

Run with no options to install ESP.
"

function usage () {
    # Must be in quotes for multiline formatting
    echo "$USAGE_MSG"
}

function activate_virtualenv () {
    . bin/activate
    export PIP_RESPECT_VIRTUALENV=true
    export PIP_REQUIRE_VIRTUALENV=true
}

function install () {
    #
    # Create the virtual environment, then install modules from the frozen
    # list.
    #
    echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    echo +
    echo + Installing ESP...
    echo +
    echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    virtualenv -p /usr/bin/python3.6 . 
    activate_virtualenv
    pip install --upgrade -v -r requirements-rhel7.pypi.txt
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Main Logic
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ -w ./ ] 
then
  while getopts "" options; do
    case $options in
      \? ) usage
           exit 1;;
    esac
  done
else
  echo "Installation Failed. You don't have write access to the current directory."
  exit 1
fi
install
