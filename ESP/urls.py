'''
URLs for entire ESP Health django project
'''
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic.base import TemplateView
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth.views import LogoutView, LoginView
from ESP.settings import MEDIA_ROOT
from ESP.ui.views import status_page
from ESP.ui.views_survey import generate_survey_report, view_survey_report, survey_import
from django.views import static

class ExtraContextTemplateView(TemplateView):
    extra_context = None

    def get_context_data(self, **kwargs):
        context = super(ExtraContextTemplateView, self).get_context_data(**kwargs)
        if self.extra_context:
            context.update(self.extra_context)
        return context

admin.autodiscover()

urlpatterns = [
    
    # Core Application
    url(r'^$', status_page, name='status'),
    
    # Vaers
    url(r'^vaers/', include('ESP.vaers.urls')),
    
    # Survey 
    url(r'^survey_import/', survey_import, name='survey_import'),
    url(r'^view_survey_report/', view_survey_report, name='view_survey_report'),
    url(r'^generate_survey_report/', generate_survey_report, name='generate_survey_report'),

    # Login and Logout
    #url(r'^login/?$', login, {'template_name': 'login.html'}, name='login'),
    #url(r'^logout/?$', logout, {'next_page': '/'}, name='logout'),
    url(r'^login/?$', LoginView.as_view(), {'template_name': 'login.html'}, name='login'),
    url(r'^logout/?$', LogoutView.as_view(), {'next_page': 'logged_out.hml'}, name='logout'),
    
    # About
    url(r'^about/', ExtraContextTemplateView.as_view(
        template_name='about.html',
        extra_context={'title': 'About ESP'}
    ), name='about'),
    
    
    # Django Admin
    url(r'^admin/', admin.site.urls),
#   (r'^admin/doc/', include('django.contrib.admindocs.urls'),

    url(r'^media/(?P<path>.*)$', static.serve, {'document_root': MEDIA_ROOT}),
    
    # Configuration
    url(r'^conf/', include('ESP.conf.urls')),
    
    # Nodis
    #url(r'^nodis/', include('ESP.nodis.urls')),
    
    # Nodis
    url(r'^util/', include('ESP.ui.urls')),

    # cda_input
    url(r'^cda/', include('ESP.cda.urls')),

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #
    #url(r'^codes', code_maintenance),
    #url(r'^json_code_grid', json_code_grid, name='json_code_grid'),
    #url(r'^/$', include('ESP.ui.urls')),
]
