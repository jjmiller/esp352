### for debugging
###import pdb
from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from ESP.emr.models import Diagnosis
from ESP.emr.models import Encounter
from ESP.emr.models import Immunization
from ESP.emr.models import LabResult
from ESP.emr.models import Patient
from ESP.emr.models import Prescription
from ESP.emr.models import Provenance
from ESP.emr.models import Pregnancy
from ESP.conf.models import CdaError
from ESP.conf.models import CdaMapping
from ESP.conf.models import CdaLoadInstruction
from ESP.conf.models import CdaXpath
from ESP.conf.models import OpMapping
from ESP.settings import DATA_DIR
from ESP.static.models import Dx_code
from ESP.static.models import SnomedToIcd10
from ESP.utils.utils import float_or_none
from ESP.utils.utils import strip_int_text
from ESP.utils.utils import strip_float_text
from ESP.utils.utils import temp_str_to_c
from ESP.utils.utils import weight_str_to_kg
from ESP.utils.utils import height_str_to_cm
import multiprocessing as mp
import queue
import traceback
from django import db
from os import listdir
from os import rename
from django.db import connection
import xml.etree.ElementTree as ET
from datetime import datetime
from enum import IntEnum
import socket
import time
import re
import pandas

# Session info for provenance
TIMESTAMP = datetime.now()
HOSTNAME = socket.gethostname()

# process-local variables
class ProcLocals():
    iProc = None
    sPatientUniversalId = ''
    patient = None
    patient_id = None
    provider = ''
    provenance = None
    provenance_id = None
    dRegx = None
    lValidationError = []
    lCdaError = []
    fileName = None
    valErrorsIgnorable = True

    def __init__(self, iProc, fileName):
        self.iProc = iProc
        self.fileName = fileName

class Command(BaseCommand):

    help = 'Import a set of CDA HL7 XML files and populate the emr tables with the data the CDAs contain.'

    ns = '{urn:hl7-org:v3}'

    def add_arguments(self, parser):
        parser.add_argument('--proc', type=int, help='The number of processes to be spawned')

    def handle(self, *fixture_labels, **options):
        count = options['proc']
        nProcess = 1
        if(count):
            nProcess = count
        print("processes: " + str(nProcess))
        self.importAll(nProcess)

    def importAll(self, nProcess):
        # for each file in the data/cda/incoming directory
        srcDir = DATA_DIR + 'cda/incoming'
        lFile = listdir(srcDir)

        # put each file in a thread-safe queue
        qJob = mp.Queue()
        for xmlFile in lFile:
            qJob.put(xmlFile)
            """ for debugging (no process spawning) uncomment the following lines
            pl = ProcLocals(0, xmlFile)
            self.importFile(pl, srcDir)
            """

        #"""for debugging, comment out the following multiprocessing code to the end of the method
        # close database connections, each process will create its own connection
        db.connections.close_all()

        # launch a series of processes that pull filenames from 
        # the queue and import them
        lProcess = [mp.Process(target=self.importFilesFromQueue, args=(i, qJob, srcDir,)) for i in range(nProcess)]

        for proc in lProcess:
            proc.start()

        for proc in lProcess:
            proc.join()
        #"""

    def importFilesFromQueue(self, iProc, qFile, srcDir):

        ### print("process: " + str(iProc) + " qFile size: " + str(qFile.qsize()))
        while(not qFile.empty()):
            try:
                xmlFile = qFile.get(False)
                pl = ProcLocals(iProc, xmlFile)
                print('| '.join("%s: %s" % item for item in vars(pl).items()))
                self.importFile(pl, srcDir)
            except queue.Empty:
                # queue has been emptied, we are done
                return

    def importFile(self, pl, srcDir):
        ns = self.ns

        xmlFullFile = srcDir + '/' + pl.fileName

        try:
            # the universal patient id is the string following the last dash (-) in the filename and preceeding .xml
            lMatch = re.search('\-([^-]+)(\.xml)', pl.fileName)
            if (lMatch):
                pl.sPatientUniversalId = lMatch[1]

            # universal patient ids need to be >= 6 characters long and not contain dashes (-)
            if(len(pl.sPatientUniversalId) >= 6):
                root = ET.parse(xmlFullFile)
                pl.provenance = Provenance(timestamp=TIMESTAMP, source=pl.fileName, 
                    hostname=HOSTNAME, status='attempted')
                pl.provenance.save()
                pl.provenance_id = pl.provenance.provenance_id

                # get the provider name for this CDA
                pl.provider = 'Generic'
                node = root.find(f'./{ns}author/{ns}assignedAuthor/{ns}assignedAuthoringDevice/{ns}softwareName')
                if node is not None:
                    pl.provider = node.text
                else:
                    element = root.find(f'./{ns}id')
                    if 'assigningAuthorityName' in element.attrib.keys():
                        pl.provider = element.attrib['assigningAuthorityName']
                # try to match the provider we found with what is in our mapping tables
                lMapping = CdaMapping.objects.exclude(id=0).values('provider').distinct()
                for mapping in lMapping:
                    length = len(mapping['provider'])
                    if (len(pl.provider) >= length and mapping['provider'].lower() == pl.provider[:length].lower()):
                        pl.provider = mapping['provider']
                        break

                pl.dRegx = self.getProviderRegxDict(pl.provider)

                # validate this CDA
                pl.lValidationError = []
                pl.valErrorsIgnorable = True
                self.importFromTree(pl, root, True)
            else:  # missing universal patient id
                self.addValidationError(pl, '**Missing or invalid universal patient id: ' + pl.sPatientUniversalId)

            sDir = 'error/'
            # if the CDA is valid
            if((pl.valErrorsIgnorable or len(pl.lValidationError) == 0) and len(pl.lCdaError) == 0):
                # import this CDA
                self.importFromTree(pl, root, False)

                # move the file to the archive or error directory
                sDir = 'archive/'
                if(len(pl.lCdaError) > 0):
                    sDir = 'error/'

            self.writeValidationLog(pl)

            # move the file to the appropriate directory
            xmlDest = DATA_DIR + 'cda/' + sDir + pl.fileName
            rename(xmlFullFile, xmlDest)

        except Exception as ex:
            self.saveException(pl, '**Error in importAll()', '', '', ex)
            self.writeValidationLog(pl)

    def writeValidationLog(self, pl):
        if(len(pl.lValidationError) > 0 or len(pl.lCdaError) > 0):
            logDest = DATA_DIR + 'cda/error/' + pl.fileName + '.error.log'
            file = open(logDest, "w")
            if(len(pl.lValidationError) > 0):
                file.write('****Validation errors found in ' + pl.fileName + ':\n')
                file.write('Provider: ' + pl.provider + '\n')
                file.writelines(pl.lValidationError)
            if(len(pl.lCdaError) > 0):
                file.write('****Exceptions found in ' + pl.fileName + ':\n')
                file.write('Provider: ' + pl.provider + '\n')
                for error in pl.lCdaError:
                    file.write(error.section + ':' + error.errMsg + ':' + error.data + '\n')
            file.close()


    def importFromTree(self, pl, root, validate):
        ns = self.ns

        try:
            # if the patient is already in the system
            # NOTE: patient id is stored in XML as follows, but this may vary between vendors
            # So the Customer global universal patient id comes as part of the filename
            # element = root.find(f'./{ns}recordTarget/{ns}patientRole/{ns}id')
            # pl.sPatientUniversalId = element.attrib['extension']
            pl.patient, is_new_patient = Patient.objects.get_or_create(natural_key=pl.sPatientUniversalId, 
                defaults={'provenance':pl.provenance})
            pl.patient.save()

            # import Demographics
            self.importDemographics(pl, root, validate)

            # import pregnancy status
            self.importPregnancies(pl, root, validate)

            # import immunizations
            self.importImmunizations(pl, root, validate)

            # import prescriptions
            self.importPrescriptions(pl, root, validate)

            # import encounters/visits
            self.importEncounters(pl, root, validate)
            self.importSurgeries(pl, root, validate)
            self.importVitals(pl, root, validate)

            # import social history
            self.importSocialHistory(pl, root, validate)

            # import lab test results
            self.importLabs(pl, root, validate)

            # import the diagnostic results
            self.importDiagnostics(pl, root, validate)

        except Exception as ex:
            self.saveException(pl, 'Error in importFromTree()', '', '', ex)

    def getDate(self, pl, sDate, validate):
        dt = None
        sPreParse = sDate

        # look for a date range seperated by " - ", in which case we will use the part before the hyphen
        lMatch = re.search('^(.+)( - )(.+)$', sDate)
        ###print("lMatch:"+str(lMatch) + " lmatch1:" + str(lMatch1)+" date:"+sDate)
        if (lMatch): 
            sPreParse = lMatch[1]

        try:   
            dt = pandas.to_datetime(sPreParse, errors='raise').date()
        except Exception as ex:
            self.addValidationError(pl, 'Could not parse date: ' + sDate, True)

        return dt


    def importPregnancies(self, pl, root, validate):
        ns = self.ns

        lFieldOrdering, lNode = self.getBranchOrderingNodes(pl, root, f'./[{ns}title="Problems"]', 
            self.getProviderRegxList(pl.provider, 'probRegx'), validate)
        if (lNode):
            if(not validate):
                for node in lNode:
                    latestDate = None
                    try:
                        # values must be in order: date, description
                        lValue = []
                        # we ignore exceptions here (validate=True) and continue processing if we have issues
                        # reading the pregnancy marker from the 'problems' section
                        lValue = self.getValuesFromNode(pl, node, lFieldOrdering, True)

                        # for each pregnancy indicator
                        if(lValue[1].lower() == 'normal pregnancy'):
                            encounter = self.getOrCreateEncounter(pl, lValue[0], 'Unknown Encounter', False)
                            # indicate the pregnancy in the encounter
                            encounter.pregnant = True
                            encounter.save()
                            if (not latestDate or encounter.date > latestDate):
                                latestDate = encounter.date
                    except Exception as ex:
                        self.addValidationError(pl, 'importPregnancies() error: ' + str(ex), True)
                        self.addValidationError(pl, 'importPregnancies() data: ' + str(lValue), True)
                        return

            # TODO: Jay
            # possible pregnancy heuristic here
            # if there is no current pregnancy event and the date of this 
            # pregnancy indicator is within the last 9 months, we could concievably
            # create a pregnancy event
            # BUT: We don't know anything from the indicator about the duration
            # of the pregnancy

    def getOrCreateEncounter(self, pl, sDate, sEncDescription, validate):
        dt = self.getDate(pl, sDate, validate)
        sDate = dt.strftime('%Y%m%d')

        # find the first encounter for this date
        encounter = Encounter.objects.filter(patient_id = pl.patient.id, 
            natural_key__startswith=sDate).order_by('id').first()
        if(not encounter):
            naturalKey = sDate + '-' + str(pl.patient.id) + '-' + sEncDescription

            # the encounter may have been created already in another process
            encounter, is_new = Encounter.objects.get_or_create(patient_id=pl.patient.id, natural_key=naturalKey, 
                provider_id=1, provenance_id=pl.provenance_id, date=dt)
            encounter.save()
            ###if isnew:
                ### print("new Encounter: " + naturalKey)

        return encounter

    def importPrescriptions(self, pl, root, validate):
        ns = self.ns

        self.importFromTable(pl, root, Prescription, f'./[{ns}title="Medications"]', 
            self.getProviderRegxList(pl.provider, 'prescripRegx'),
            self.getFieldPositionDict(pl.provider, 'prescription'), validate)

    def importImmunizations(self, pl, root, validate):
        ns = self.ns

        self.importFromTable(pl, root, Immunization, f'./[{ns}title="Immunizations"]', 
            self.getProviderRegxList(pl.provider, 'immunRegx'),
            self.getFieldPositionDict(pl.provider, 'immunization'), validate)

    def importEncounters(self, pl, root, validate):
        ns = self.ns

        self.importFromTable(pl, root, Encounter, f'./[{ns}title="Encounters"]',
            self.getProviderRegxList(pl.provider, 'encRegx'),
            self.getFieldPositionDict(pl.provider, 'encounter'), validate)

    def importSurgeries(self, pl, root, validate):
        ns = self.ns

        self.importFromTable(pl, root, Encounter, f'./[{ns}title="Surgeries"]',
            self.getProviderRegxList(pl.provider, 'surgRegx'),
            self.getFieldPositionDict(pl.provider, 'surgery'), validate)

    def importVitals(self, pl, root, validate):
        ns = self.ns

        self.importVitalsFromTable(pl, root, f'./[{ns}title="Vital Signs"]',
            self.getProviderRegxList(pl.provider, 'vitRegx'),
            self.getFieldDict(pl.provider, 'vitals'), validate)

    def importLabs(self, pl, root, validate):
        ns = self.ns

        # import lab results from the lab result section
        self.importLabResults(pl, root, validate)

        self.importLabResultsFromVitals(pl, root, f'./[{ns}title="Vital Signs"]',  
            self.getProviderRegxList(pl.provider, 'vitRegx'),
            pl.dRegx['nonVitalLab'], validate)

        self.importLabResultsFromVitalsEntries(pl, root,  f'./[{ns}title="Vital Signs"]', pl.dRegx['nonVitalLab'], validate)

    def importLabResults(self, pl, root, validate):
        ns = self.ns

        self.importFromTable(pl, root, Encounter, f'./[{ns}title="Labs"]',
            self.getProviderRegxList(pl.provider, 'labRegx'),
            self.getFieldPositionDict(pl.provider, 'laboratory'), validate)

    def importSocialHistory(self, pl, root, validate):
        ns = self.ns

        self.importFromTable(pl, root, Encounter, f'./[{ns}title="Social History"]',
            self.getProviderRegxList(pl.provider, 'socialRegx'),
            self.getFieldPositionDict(pl.provider, 'social'), validate)

    def importDiagnostics(self, pl, root, validate):
        ns = self.ns

        self.importFromTable(pl, root, Encounter, f'./[{ns}title="Diagnostic Results"]',
            self.getProviderRegxList(pl.provider, 'diagRegx'),
            self.getFieldPositionDict(pl.provider, 'diagnostic'), validate)

        self.importDiagnosticsFromProblems(pl, root, f'./[{ns}title="Problems"]',
            self.getProviderRegxList(pl.provider, 'diagProbRegx'), validate)

        self.importDiagnosticsFromProblemsEntries(pl, root, f'./[{ns}title="Problems"]', validate)

    def getBranchOrderingNodes(self, pl, root, titleXpath, lFieldRegx, validate):
        ns = self.ns
        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        headXpath = f'./{ns}text/{ns}table/{ns}thead/{ns}tr'
        fieldXpath = f'./{ns}th'
        nodeXpath = f'./{ns}text/{ns}table/{ns}tbody/{ns}tr'

        lBranch = root.findall(branchXpath)
        branch = None
        lOrdering = None
        lNode = None
        if lBranch:
            for branchT in lBranch:
                node = branchT.find(titleXpath)
                if node:
                    branch = branchT
                    lOrdering = self.getFieldOrdering(pl, root, branch, headXpath, fieldXpath, lFieldRegx, validate)
                    if (len(lOrdering) > 0):
                        lNode = branch.findall(nodeXpath)
                    break

        return lOrdering, lNode

    def getFieldOrdering(self, pl, root, branch, headXpath, fieldXpath, lFieldRegx, validate):
            # get the ordering of the data
            ### print('tree:' + ET.tostring(branch, encoding='unicode'))
            lFieldOrdering = []
            lNodeText = []
            headNode = branch.find(headXpath)
            if headNode:
                lNode = headNode.findall(fieldXpath)
                ### print('lNode: ' + str(lNode))
                if lNode:
                    for node in lNode:
                        lNodeText.append(node.text)

                    ### print('fieldRegx: '  + str(lFieldRegx))
                    for fieldRegx in lFieldRegx:
                        index = 0
                        for text in lNodeText:
                            ### print('node, index, regx: ' + text + ',' + str(index) + ',' + fieldRegx)
                            if(re.search(fieldRegx, text, re.IGNORECASE)):
                                lFieldOrdering.append(index)
                                break
                            index += 1
                ### print('field ordering:' + str(lFieldOrdering))
                if (validate and len(lFieldOrdering) != len(lFieldRegx)):
                    self.addValidationError(pl, 'getFieldOrdering(): list of required fields not found.', True)
                    self.addValidationError(pl, 'Required: ' + str(lFieldRegx), True)
                    self.addValidationError(pl, 'Found: ' + str(lNodeText), True)

            return lFieldOrdering

    def getValuesFromNode(self, pl, node, lFieldOrdering, validate):
        ns = self.ns

        lValue = len(lFieldOrdering) * [None]
        index = 0
        order = -1
        try: 
            lNodeObj = node.findall(f'./{ns}td')
            for order in lFieldOrdering:
                lValue[index] = lNodeObj[order].text.strip()
                index += 1
        except Exception as ex:
            self.addValidationError(pl, 'Exception in getValuesFromNode(): ' + str(ex), validate)
            self.addValidationError(pl, 'ordering: ' + str(lFieldOrdering), validate)
            self.addValidationError(pl, 'Error fetching value for location: ' + str(index) + ' order: ' + str(order), validate)

        return lValue

    def importLabResultsFromVitals(self, pl, root, titleXpath, lFieldRegx, nonLabRegx, validate):
        ns = self.ns

        # if this provider does not put this data in this section
        try:
            cdaLoad = CdaLoadInstruction.objects.get(provider=pl.provider, operation='LabResultsFromVitals')
        except ObjectDoesNotExist:
            return

        lFieldOrdering, lNode = self.getBranchOrderingNodes(pl, root, titleXpath, lFieldRegx, validate)
        if (lNode):
            for node in lNode:
                lValue = self.getValuesFromNode(pl, node, lFieldOrdering, validate)

                if(validate):
                    # lab results ordering is required to be:
                    # Date, Description ('Total Cholesterol'), Value ('55'), code ('LOINC: 4548-4')
                    # these 4 fields are not optional
                    # if the description looks like a lab result
                    if(len(lValue) < 4):
                        self.addValidationError(pl, 'importLabResultsFromVitals(): minimum lab data set not present.')
                        return
                else:
                    try:
                        description = lValue[1]
                        if (not re.search(nonLabRegx, description, re.IGNORECASE)):
                        
                            # the first 2 fields are used to create the naturalKey
                            dt = self.getDate(pl, lValue[0], validate)
                            lValue[0]=dt

                            if (not validate) and description and dt:
                                # create the natural key from the date and the description 
                                naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) + '-' + description
                                # truncate so we fit into natural key length
                                naturalKey = naturalKey[0:127]

                                lr = LabResult.objects.filter(patient_id = pl.patient.id, 
                                    natural_key = naturalKey).first()

                                # if this record does not already exist
                                if(not lr):
                                    # create it
                                    ### print("new LabResult from vitals: " + naturalKey)
                                    lr, created = LabResult.objects.get_or_create(patient_id=pl.patient.id, 
                                        natural_key=naturalKey, provenance_id=pl.provenance_id, provider_id=1, 
                                        date=dt, result_date=dt)
                                lr.native_name = description
                                lr.native_code = lValue[3]
                                lr.result_string = lValue[2]
                                lr.save()

                    except Exception as ex:
                        self.saveException(pl, 'Error in importLabResultsFromVitals()', 'LabResult', str(lValue), ex)
                        return

    def importLabResultsFromVitalsEntries(self, pl, root, titleXpath, nonLabRegx, validate):
        ns = self.ns

        # if this provider does not put this data in this section
        try:
            cdaLoad = CdaLoadInstruction.objects.get(provider=pl.provider, operation='LabResultsFromVitalsEntries')
        except ObjectDoesNotExist:
            return

        diagCodeType = cdaLoad.codeType.lower()
        if(diagCodeType != 'icd10' and diagCodeType != 'snomed'):
            self.saveException(pl, 'Error in importLabResultsFromVitalsEntries(), unsupported code type', 
                'LabResults', diagCodeType, '')
            return

        # find the root node of the correct area
        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        lBranch = root.findall(branchXpath)
        branch = None
        probRoot = None
        if lBranch:
            for branch in lBranch:
                node = branch.find(titleXpath)
                if node:
                    probRoot = branch
                    # for each entry in this problem section
                    lEntry = probRoot.findall(f'./{ns}entry')
                    if lEntry:
                        for entry in lEntry:

                            # get the date 
                            node = entry.find(f'./{ns}organizer/{ns}component/{ns}observation/{ns}effectiveTime')
                            sDate = None
                            if node is not None and ('value' in node.attrib.keys()):
                                sDate = node.attrib['value']
 
                            dt = self.getDate(pl, sDate, False)
                            if(dt):
                                # get lab result code, description and results
                                node = entry.find(f'./{ns}organizer/{ns}component/{ns}observation/{ns}code')
                                nodeResult = entry.find(f'./{ns}organizer/{ns}component/{ns}observation/{ns}value')
                                ### print("keys:" + str(node.attrib.keys()))
                                if (node is not None and ('code' in node.attrib.keys()) and ('displayName' in node.attrib.keys())
                                        and nodeResult is not None and ('value' in nodeResult.attrib.keys()) 
                                        and ('unit' in nodeResult.attrib.keys())):
                                    code = node.attrib['code']
                                    description = node.attrib['displayName']
                                    result = nodeResult.attrib['value']
                                    unit = nodeResult.attrib['unit']

                                    # if this is not a vital sign we will treat as a lab result
                                    if (not re.search(nonLabRegx, description, re.IGNORECASE)):

                                        #if we are validating, this is sufficient
                                        if validate:
                                            continue

                                        ### print("lrve code:" + code)
                                        ### print("lrve name:" + description)

                                        # create the natural key from the date and the description 
                                        naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) + '-' + description
                                        # truncate so we fit into natural key length
                                        naturalKey = naturalKey[0:127]

                                        lr = LabResult.objects.filter(patient_id = pl.patient.id, 
                                            natural_key = naturalKey).first()
                                        ### print("lrve naturalKey: " + naturalKey)

                                        # if this record does not already exist
                                        if(not lr):
                                            # create it
                                            ### print("new LabResult ProblemEntries: " + naturalKey)
                                            lr, created = LabResult.objects.get_or_create(patient_id=pl.patient.id, 
                                                natural_key=naturalKey, provenance_id=pl.provenance_id, provider_id=1, 
                                                date=dt, result_date=dt)
                                        lr.native_code = code
                                        lr.native_name = description
                                        lr.result_string = result
                                        lr.result_float = float_or_none(result)
                                        lr.ref_unit = unit
                                        lr.save()
                            else:
                                # bad date  
                                self.saveException(pl, 'Error in importLabResultsFromVitalsEntries(), unparseable date',
                                    'LabResults', 'date->'+sDate, '')
                                return

    def importDiagnosticsFromProblems(self, pl, root, titleXpath, lFieldRegx, validate):
        ns = self.ns

        # if this provider does not put this data in this section
        try:
            cdaLoad = CdaLoadInstruction.objects.get(provider=pl.provider, operation='DiagnosticsFromProblems')
        except ObjectDoesNotExist:
            return

        diagCodeType = cdaLoad.codeType.lower()
        diagRegx = cdaLoad.regx
        if(diagCodeType != 'icd10' and diagCodeType != 'snomed'):
            self.saveException(pl, 'Error in importDiagnosticsFromProblems(), unknown code type', 'Diagnostics', diagCodeType, ex)

        lFieldOrdering, lNode = self.getBranchOrderingNodes(pl, root, titleXpath, lFieldRegx, validate)
        ### print("ordering: " + str(lFieldOrdering))
        if (lNode):
            for node in lNode:
                lValue = self.getValuesFromNode(pl, node, lFieldOrdering, validate)

                if(validate):
                    # vital signs ordering is required to be:
                    # Date, Codes (snomed or icd10)
                    # these 2 fields are not optional
                    if(len(lValue) < 2):
                        self.addValidationError(pl, 'importDiagnosticsFromProblems(): minimum diagnostic data set not present.')
                        return
                else:
                    try:
                        # if this 'Problem' is a diagnosis
                        problemDate = lValue[0]                   
                        problemCode = lValue[1]
                        lMatch = re.search(diagRegx, problemCode, re.IGNORECASE)
                        if(lMatch and lMatch[1]):
                            diagCode = lMatch[1].upper()
                            # if an icd10 code of interest
                            if(diagCodeType == 'icd10'):
                                dxCode = Dx_code.objects.filter(code=diagCode, type='icd10').first()
                            # else if a snomed code of interest
                            elif(diagCodeType == 'snomed'):
                                dxCode = self.icdFromSnomed(diagCode)
                            # else -- code we don't know how to process
                            else:
                                self.saveException(pl, 'Unknown Diagnosis code type in importDiagnosticsFromProblems()', '', diagCodeType, ex)
                                continue

                            # at this point we should have found an ICD10 or the ICD9 of 799.9
                            if not dxCode:
                                self.saveException(pl, 'Error in importDiagnosticsFromProblems(), icd9/icd10 code not present in static_dx_code', 
                                    'Diagnostics', '799.9/'+diagCode, ex)
                                continue

                            dt = self.getDate(pl, problemDate, False)
                            if(dt):
                                self.addDiagnosis(pl, diagCodeType, diagCode, dt, dxCode)

                    except Exception as ex:
                        self.saveException(pl, 'Error in importDiagnosticsFromProblems()', 'Diagnostics', str(lValue), ex)
                        return


    def addDiagnosis(self, pl, diagCodeType, diagCode, date, dxCode):
        encounter = self.getOrCreateEncounter(pl, str(date), 'Unknown Encounter', False)

        ### print ("add diagnosis date: " + str(date) + " code: " + str(dxCode))
        # if we don't already have this diagnosis for this encounter, add one 
        encounter.add_diagnosis(dxCode.type, dxCode.combotypecode)

        # if this was a snomed we mapped to 'Unknown'
        if (diagCodeType == 'snomed' and dxCode.code == '799.9'):
            ### print('Adding a diagnosis for an unmapped snomed ')
            # add this to the emr_diagnosis table if it doesn't already exist
            naturalKey = "enc " + str(encounter.id) + ":diag " + diagCode
            diagnosis = Diagnosis.objects.filter(natural_key=naturalKey).first()
            if(diagnosis is None):
                diagnosis, created = Diagnosis.objects.get_or_create(
                    natural_key=naturalKey, provider_id=1, 
                    date=date, mrn=encounter.mrn, code=diagCode, encounter_id=encounter.id,
                    provenance_id=pl.provenance_id, patient_id=encounter.patient_id, codeset='snomed')
                ### print("new Diagnosis: " + naturalKey)
                diagnosis.save()

    def importDiagnosticsFromProblemsEntries(self, pl, root, titleXpath, validate):
        ns = self.ns

        # if this provider does not put this data in this section
        try:
            cdaLoad = CdaLoadInstruction.objects.get(provider=pl.provider, operation='DiagnosticsFromProblemsEntries')
        except ObjectDoesNotExist:
            return

        diagCodeType = cdaLoad.codeType.lower()
        if(diagCodeType != 'icd10' and diagCodeType != 'snomed'):
            self.saveException(pl, 'Error in importDiagnosticsFromProblemsEntries(), unsupported code type', 
                'Diagnostics', diagCodeType, '')
            return

        # find the root node of the Problems area
        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        lBranch = root.findall(branchXpath)
        branch = None
        probRoot = None
        if lBranch:
            for branch in lBranch:
                node = branch.find(titleXpath)
                if node:
                    probRoot = branch
                    # for each entry in this problem section
                    lEntry = probRoot.findall(f'./{ns}entry')
                    if lEntry:
                        for entry in lEntry:

                            # get the date 
                            nodeLow = entry.find(f'./{ns}act/{ns}effectiveTime/{ns}low')
                            nodeHigh = entry.find(f'./{ns}act/{ns}effectiveTime/{ns}high')
                            sDate = None
                            if nodeLow is not None and ('value' in nodeLow.attrib.keys()):
                                sDate = nodeLow.attrib['value']
                            if not sDate and (nodeHigh is not None and ('value' in nodeHigh.attrib.keys())):     
                                sDate = nodeHigh.attrib['value']
 
                            dt = self.getDate(pl, sDate, False)
                            if(dt):
                                # get snomed diagnosis code
                                node = entry.find(f'./{ns}act/{ns}entryRelationship/{ns}observation/{ns}value')
                                ### print("keys:" + str(node.attrib.keys()))
                                if node is not None and ('code' in node.attrib.keys()):
                                    code = node.attrib['code']
                                    ### print("dpe code:" + code)
                                    ### print("dpe name:" + node.attrib['displayName'])

                                    #if we are validating, this is sufficient
                                    if validate:
                                        continue
                                    else:
                                        if diagCodeType == 'snomed':
                                            dxCode = self.icdFromSnomed(code)
                                        else:
                                            # icd10
                                            dxCode = Dx_code.objects.filter(code=code, type='icd10').first()
                                        self.addDiagnosis(pl, diagCodeType, code, dt, dxCode)
                                else:
                                    # missing diagnosis, skip this entry
                                    pass
                            else:
                                # bad date  
                                self.saveException(pl, 'Error in importDiagnosticsFromProblemsEntries(), unparseable date',
                                    'Diagnostics', 'date->'+sDate, '')
                                return
                                              
                    return # done with first Problems section

    def importVitalsFromTable(self, pl, root, titleXpath, lFieldRegx, dCdaToEspField, validate):
        ns = self.ns

        lFieldOrdering, lNode = self.getBranchOrderingNodes(pl, root, titleXpath, lFieldRegx, validate)
        ### print("ordering: " + str(lFieldOrdering))
        if (lNode):
            for node in lNode:
                lValue = self.getValuesFromNode(pl, node, lFieldOrdering, validate)

                if(validate):
                    # vital signs ordering is required to be:
                    # Date, Description ('Temperature'), Value ('98.5f')
                    # these 3 fields are not optional
                    if(len(lValue) < 3):
                        self.addValidationError(pl, 'importVitalsFromTable(): minimum vital sign data set not present.')
                        return
                else:
                    try:
                        cdaName = lValue[1].lower()
                        ### print('vital: ' + cdaName + ' : ' + lValue[2])
                        if(cdaName in dCdaToEspField):
                            encounter = self.getOrCreateEncounter(pl, lValue[0], 'Unknown Encounter', False)

                            # save the vital sign in the encounter
                            self.insertDBValue(pl, Encounter, encounter, dCdaToEspField[cdaName], lValue[2])
                            encounter.save()
                    except Exception as ex:
                        self.saveException(pl, 'Error in importVitalsFromTable()', 'Encounter', str(lValue), ex)
                        return


    def importFromTable(self, pl, root, classType, titleXpath, lFieldRegx, dFieldPosition, validate):
        ns = self.ns

        lFieldOrdering, lNode = self.getBranchOrderingNodes(pl, root, titleXpath, lFieldRegx, validate)
        if (lNode):
            for node in lNode:
                lValue = self.getValuesFromNode(pl, node, lFieldOrdering, validate)

                dt = None
                try:
                    # the first 2 fields (date, description) are non-optional and used to create the naturalKey
                    sDate = lValue[0]
                    ### print("date: " + sDate)
                    dt = self.getDate(pl, sDate, validate)
                    lValue[0]=dt
                    description = lValue[1]
                except Exception as ex:
                    if(validate):
                        self.addValidationError(pl, 'Error in importFromTable(): ' + classType._meta.model_name + ' ' + str(ex))
                    else:
                        self.saveException(pl, 'Error in importFromTable()', classType._meta.model_name, '', ex)
                    return

                if (not validate) and description and dt:
                    # create the natural key from the date and the description 
                    naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) + '-' + description
                    # truncate so we fit into natural key length
                    naturalKey = naturalKey[:127]

                    obj = classType.objects.filter(patient_id = pl.patient.id, 
                        natural_key = naturalKey).first()

                    # if this record does not already exist
                    if(not obj):
                        # create it
                        ### print("new Obj: " + naturalKey)
                        obj, created = classType.objects.get_or_create(patient_id=pl.patient.id, 
                            provider_id=1, natural_key=naturalKey, provenance_id=pl.provenance_id, date=dt)

                    # update the (potentially new) object
                    # add the field values to the object
                    for field in dFieldPosition:
                        self.insertDBValue(pl, classType, obj, field, lValue[dFieldPosition[field]])
                    obj.save()

    def importDemographics(self, pl, root, validate):

        # process xpaths first for all CDAs
        # then for provider specific CDA
        lProv = ['All', pl.provider]

        if(not validate):
            for provider in lProv:
                lCdaXpath = CdaXpath.objects.filter(provider=provider).order_by('id')
                for xPathMapping in lCdaXpath:
                    ### print("mapping: " + str(mapping))
                    if(xPathMapping.provider == provider and
                        xPathMapping.modelName == 'Patient'):

                        try:
                            # first name, last name are required
                            requiredField = (xPathMapping.fieldTo == 'first_name' or xPathMapping.fieldTo == 'last_name')
                            if(not self.processItem(pl, root, xPathMapping, Patient, pl.patient)):
                                if(requiredField):
                                    raise Exception('Invalid patient data, missing required field: ' + xPathMapping.fieldTo)

                        except Exception as ex:
                            if(requiredField):
                                print('Invalid patient data, missing mandatory field: ' + xPathMapping.fieldTo)
                                raise Exception('Invalid patient data, missing mandatory field: ' + xPathMapping.fieldTo)
                            else:
                                # we just ignore other missing demographics data
                                pass

    def processItem(self, pl, root, mapping, classType, obj):
        objDirty = False
        retVal = False

        if mapping.operation == OpMapping.node:
            # fetch the data from the node
            ### print("xpath: " + mapping.xpathFrom)
            node = root.find(mapping.xpathFrom)
            if node is not None:
                value = node.text
                if value is not None:
                    objDirty = True
                    ### print("node value: " + value)
                    self.transformAndInsert(pl, classType, obj, value, mapping.transform,
                        mapping.fieldTo)
                    retVal = True
        
        elif mapping.operation == OpMapping.attrib:
            # fetch the data from the specified attribute of the specified node
            element = root.find(mapping.xpathFrom)
            if element is not None:
                value = element.attrib[mapping.attribute]
                if value is not None:
                    objDirty = True
                    ### print("attrib value: " + value)
                    self.transformAndInsert(pl, classType, obj, value, mapping.transform,
                        mapping.fieldTo)
                    retVal = True
        
        elif mapping.operation == OpMapping.timeNow:
            objDirty=True
            obj.__dict__[mapping.fieldTo] = datetime.now()
            retVal = True

        # only save the object if we actually found data for it in the XML
        if (objDirty):
            obj.save()
        return retVal

    def transformAndInsert(self, pl, classType, obj, value, transform, to):
        if not transform:
            self.insertDBValue(pl, classType, obj, to, value)
            obj.__dict__[to] = value
        else:
            if value:
                # transform the data
                if transform == 'datetime':
                    value = self.getDate(pl, value, False)
                    if (not value):
                        self.saveException(pl, 'Invalid date in transformAndInsert()', classType._meta.model_name, value, '')
                        return
                else:
                    # the transform is a custom function
                    self.saveException(pl, 'Missing custom transform in transformAndInsert()', classType._meta.model_name, 'transform=' + transform, '')
                    return
                obj.__dict__[to] = value

    # Truncate a string to a length that will fit into the 
    # database field
    def truncateDBString(self, classType, field, value):
        if(isinstance(value, str)):
            fieldType = classType._meta.get_field(field).db_type(connection)
            if(fieldType):
                lMatch = re.search('varchar\((\d+)\)', fieldType, re.IGNORECASE)
                if(lMatch):
                    try:
                        length = int(lMatch[1])
                        value = value[0:length-1]
                    except:
                        pass
        return value

    # stip out unit text (98.6 def F) -> (98.6) 
    # from a numeric text string
    def stripNonNumText(self, classType, field, value):
        if(isinstance(value, str)):
            fieldType = classType._meta.get_field(field).db_type(connection)
            ### print(fieldType + ':' + str(value))
            if(fieldType):
                lMatch = re.search('(float|real|numeric|double)', fieldType, re.IGNORECASE)
                if(lMatch):
                    value = strip_float_text(value)
                lMatch = re.search('(int)', fieldType, re.IGNORECASE)
                if(lMatch):
                    value = strip_int_text(value)
        return value

    # ensure that a value will fit in the specified
    # model field and insert it
    def insertDBValue(self, pl, classType, obj, field, value):
        if(not self.insertNormalizeDBValue(pl, classType, obj, field, value)):
            value = self.truncateDBString(classType, field, value)
            value = self.stripNonNumText(classType, field, value)
            obj.__dict__[field] = value

    # normalize certain field values to proper format / units 
    # and update the obj with the normalized value
    # return False if further DB operations are required
    # return True if DB operations for this field are complete
    def insertNormalizeDBValue(self, pl, classType, obj, field, value):
        try:
            if(classType == Encounter):
                if (field == 'raw_weight'):
                    # assume the weight is in kg, check for lbs
                    kg = weight_str_to_kg(value)
                    if(kg == None):
                        # parse out the number
                        kg = strip_float_text(value)
                    obj.weight = kg
                elif (field == 'raw_height'):
                    # assume the height is in cm, check for english units
                    cm = height_str_to_cm(value)
                    if(cm == None):
                        # parse out the number
                        cm = strip_float_text(value)
                    obj.height = cm
                elif (field == 'temperature'):
                    degc = temp_str_to_c(value)
                    obj.temperature = degc
                    return True
                elif (field == 'raw_bmi'):
                    obj.bmi = strip_float_text(value)
                elif (field == 'raw_bp_systolic'):
                    obj.bp_systolic = strip_float_text(value)
                elif (field == 'raw_bp_diastolic'):
                    obj.bp_diastolic = strip_float_text(value)

        except Exception as ex:
            self.saveException(pl, 'Error in insertNormalizedDBValue()', classType._meta.model_name, value, ex)

        return False

    def getProviderRegxDict(self, provider):
        d = {}
        lMapping = CdaMapping.objects.filter(provider=provider, section__isnull=True).order_by('id')
        for mapping in lMapping:
            d[mapping.key] = mapping.value
            ### print('provider: ' + provider + ' key: ' + mapping.key + ' value: ' + mapping.value)
        return d

    def getFieldPositionDict(self, provider, section):
        d = {}
        lMapping = CdaMapping.objects.filter(provider=provider, section=section).order_by('id')
        for mapping in lMapping:
            d[mapping.key] = int(mapping.value)
        return d

    def getFieldDict(self, provider, section):
        d = {}
        lMapping = CdaMapping.objects.filter(provider=provider, section=section).order_by('id')
        for mapping in lMapping:
            d[mapping.key] = mapping.value
        return d

    def getProviderRegxList(self, provider, section):
        l = []
        lMapping = CdaMapping.objects.filter(provider=provider, section=section).order_by('id')
        for mapping in lMapping:
            l.append(mapping.value)
        return l

    def saveException(self, pl, description, section, data, ex):
        sErr = description + ':' + section + ': ' + str(ex) + ' ' + traceback.format_exc()
        error = CdaError(fileName = pl.fileName, errMsg = sErr[:512], section=section,
            data = data, timestamp = datetime.now())
        error.save()
        pl.lCdaError.append(error)

    def addValidationError(self, pl, sError, ignorable=False):
        sPrefix = 'Ignorable: '
        if (not ignorable):
            pl.valErrorsIgnorable = False
            sPrefix = 'NOT Ignorable: '
        ### print('Validation: ' + sPrefix + sError)
        pl.lValidationError.append(sPrefix + sError + '\n')

    def icdFromSnomed(self, snomedCode):
        # if a snomed code of interest
        lIcd10 = SnomedToIcd10.objects.filter(snomed=snomedCode)
        if(lIcd10):
            icd10 = lIcd10[0].icd10.upper()
            ### print('Mapped snomed: ' + diagCode + " to icd10: " + icd10)
            dxCode = Dx_code.objects.filter(code=icd10, type='icd10').first()
        else:
            # didn't find this snomed in the mapping table, meaning it doesn't map to a condition
            # we track
            # use an icd9 code for unknown morbitity/mortality
            dxCode = Dx_code.objects.filter(code='799.9', type='icd9').first()
 
        return dxCode

