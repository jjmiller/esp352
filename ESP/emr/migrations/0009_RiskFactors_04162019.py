# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0008_provider_npi'),
    ]

    operations = [
        migrations.CreateModel(
            name='Risk_Factors',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('natural_key', models.CharField(help_text='Unique Record identifier in source EMR system', max_length=128, unique=True, null=True, blank=True)),
                ('created_timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated_timestamp', models.DateTimeField(auto_now=True, db_index=True)),
                ('mrn', models.CharField(max_length=50, null=True, verbose_name='Medical Record Number', blank=True)),
                ('encounter_date', models.DateField(null=True, verbose_name='Encounter Date', blank=True)),
                ('ever_sex_with_male', models.CharField(max_length=20, null=True, verbose_name='ever_sex_with_male', blank=True)),
                ('past12mo_sex_with_male', models.CharField(max_length=20, null=True, verbose_name='past12mo_sex_with_male', blank=True)),
                ('ever_sex_with_female', models.CharField(max_length=20, null=True, verbose_name='ever_sex_with_female', blank=True)),
                ('past12mo_sex_with_female', models.CharField(max_length=20, null=True, verbose_name='past12mo_sex_with_female', blank=True)),
                ('ever_sex_with_hiv', models.CharField(max_length=20, null=True, verbose_name='ever_sex_with_hiv', blank=True)),
                ('past12mo_sex_with_hiv', models.CharField(max_length=20, null=True, verbose_name='past12mo_sex_with_hiv', blank=True)),
                ('ever_sex_with_hepc', models.CharField(max_length=20, null=True, verbose_name='ever_sex_with_hepc', blank=True)),
                ('past12mo_sex_with_hepc', models.CharField(max_length=20, null=True, verbose_name='past12mo_sex_with_hepc', blank=True)),
                ('ever_sex_with_ivuser', models.CharField(max_length=20, null=True, verbose_name='Ever_sex_with_ivuser', blank=True)),
                ('past12mo_sex_with_ivuser', models.CharField(max_length=20, null=True, verbose_name='Past12mo_sex_with_ivuser', blank=True)),
                ('ever_exchange_sex', models.CharField(max_length=20, null=True, verbose_name='ever_exchange_sex', blank=True)),
                ('past12mo_exchange_sex', models.CharField(max_length=20, null=True, verbose_name='past12mo_exchange_sex', blank=True)),
                ('ever_sex_while_ui', models.CharField(max_length=20, null=True, verbose_name='ever_sex_while_ui', blank=True)),
                ('past12mo_sex_while_ui', models.CharField(max_length=20, null=True, verbose_name='past12mo_sex_while_ui', blank=True)),
                ('ever_injected_nonrx_drug', models.CharField(max_length=20, null=True, verbose_name='ever_injected_nonrx_drug', blank=True)),
                ('past12mo_injected_nonrx_drug', models.CharField(max_length=20, null=True, verbose_name='past12mo_ injected_nonrx_drug', blank=True)),
                ('ever_nonrx_intranasal_drug_use', models.CharField(max_length=20, null=True, verbose_name='ever_nonrx_intranasal_drug_use', blank=True)),
                ('past12mo_nonrx_intranasal_drug_use', models.CharField(max_length=20, null=True, verbose_name='past12mo_nonrx_intranasal_drug_use', blank=True)),
                ('ever_nonrx_drug_use', models.CharField(max_length=20, null=True, verbose_name='ever_nonrx_drug_use', blank=True)),
                ('past12mo_nonrx_drug_use', models.CharField(max_length=20, null=True, verbose_name='past12mo_nonrx_drug_use', blank=True)),
                ('ever_share_drug_equip', models.CharField(max_length=20, null=True, verbose_name='ever_share_drug_equip', blank=True)),
                ('past12mo_share_drug_equip', models.CharField(max_length=20, null=True, verbose_name='past12mo_share_drug_equip', blank=True)),
                ('ever_incarcerated', models.CharField(max_length=20, null=True, verbose_name='ever_incarcerated', blank=True)),
                ('past12mo_incarcerated', models.CharField(max_length=20, null=True, verbose_name='past12mo_incarcerated', blank=True)),
                ('ever_homeless', models.CharField(max_length=20, null=True, verbose_name='ever_homeless', blank=True)),
                ('past12mo_homeless', models.CharField(max_length=20, null=True, verbose_name='past12mo_homeless', blank=True)),
                ('other_risk_factors', models.CharField(max_length=128, null=True, verbose_name='other_risk_factors', blank=True)),
                ('patient', models.ForeignKey(blank=True, to='emr.Patient', null=True, on_delete=models.CASCADE)),
                ('provenance', models.ForeignKey(to='emr.Provenance', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Risk Factors',
            },
        ),
    ]
