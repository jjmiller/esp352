# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0010_Facility_ProviderIDs_04162019'),
    ]

    operations = [
        migrations.RemoveField( model_name='encounter', name='risk_screen',),
        migrations.AddField( model_name='encounter', name='primary_payer', field=models.CharField(max_length=100, null=True, verbose_name='Primary Payer', blank=True),),
        migrations.RenameField('encounter', 'hiv_test_recommended', 'general_use1'), 
        migrations.RenameField('encounter', 'hiv_result_date', 'general_use2'), 
        migrations.RenameField('encounter', 'health_dept_hiv_refer', 'general_use3'), 
        migrations.RenameField('encounter', 'risk_reduction_counseling_date', 'general_use4'), 
        migrations.RenameField('encounter', 'hiv_care_visit_flag', 'general_use5'), 
        migrations.RenameField('encounter', 'drvs_service_line', 'general_use6'), 
        ]
