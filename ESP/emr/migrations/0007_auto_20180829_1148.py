# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0006_auto_20170925_1308'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='allergy',
            options={'verbose_name_plural': 'Allergies'},
        ),
        migrations.AlterModelOptions(
            name='labinfo',
            options={'verbose_name_plural': 'Lab Info'},
        ),
        migrations.AlterModelOptions(
            name='order_idinfo',
            options={'verbose_name_plural': 'Order ID Info'},
        ),
        migrations.AlterModelOptions(
            name='patient_extradata',
            options={'verbose_name_plural': 'Patient Extra Data'},
        ),
        migrations.AlterModelOptions(
            name='pregnancy',
            options={'ordering': ['edd'], 'verbose_name_plural': 'Pregnancies'},
        ),
        migrations.AlterModelOptions(
            name='provider_idinfo',
            options={'verbose_name_plural': 'Provider ID Info'},
        ),
        migrations.AlterModelOptions(
            name='provider_phones',
            options={'verbose_name': 'Provider Phones', 'verbose_name_plural': 'Provider Phones'},
        ),
        migrations.AlterModelOptions(
            name='socialhistory',
            options={'verbose_name_plural': 'Social Histories'},
        ),
        migrations.AlterModelOptions(
            name='stiencounterextended',
            options={'verbose_name_plural': 'STI Encounter Extended'},
        ),
    ]
