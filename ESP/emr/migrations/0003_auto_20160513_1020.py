# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0002_auto_20151204_1116'),
    ]

    operations = [
        migrations.CreateModel(
            name='STIEncounterExtended',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('natural_key', models.CharField(help_text='Unique Record identifier in source EMR system', max_length=128, unique=True, null=True, blank=True)),
                ('created_timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated_timestamp', models.DateTimeField(auto_now=True, db_index=True)),
                ('date', models.DateField(db_index=True)),
                ('mrn', models.CharField(max_length=50, null=True, verbose_name='Medical Record Number', blank=True)),
                ('insurance_status', models.CharField(max_length=100, null=True, verbose_name='Patient Insurance', blank=True)),
                ('primary_purpose', models.CharField(max_length=50, null=True, verbose_name='Primary Purpose', blank=True)),
                ('primary_contraception', models.CharField(max_length=50, null=True, verbose_name='Primary Contraception', blank=True)),
                ('sti_symptoms_yn', models.CharField(max_length=20, null=True, verbose_name='STI Symptoms', blank=True)),
                ('sti_exposure_yn', models.CharField(max_length=20, null=True, verbose_name='STI Exposure', blank=True)),
                ('pelvic_exam_yn', models.CharField(max_length=20, null=True, verbose_name='Pelvic Exam', blank=True)),
                ('sex_mwb', models.CharField(max_length=100, null=True, verbose_name='Sex with Men/Women/Both', blank=True)),
                ('num_male_partners', models.IntegerField(null=True, verbose_name='nummalepartners', blank=True)),
                ('num_female_partners', models.IntegerField(null=True, verbose_name='numfemalepartners', blank=True)),
                ('total_partners', models.IntegerField(null=True, verbose_name='totalpartners', blank=True)),
                ('sexuality', models.CharField(max_length=50, null=True, verbose_name='sexuality', blank=True)),
                ('new_partners_yn', models.CharField(max_length=20, null=True, verbose_name='newpartners', blank=True)),
                ('anal_sex_yn', models.CharField(max_length=20, null=True, verbose_name='analsex', blank=True)),
                ('oral_sex_yn', models.CharField(max_length=20, null=True, verbose_name='oralsex', blank=True)),
                ('ept_accepted_yn', models.CharField(max_length=20, null=True, verbose_name='ept', blank=True)),
                ('hiv_test_ever_yn', models.CharField(max_length=20, null=True, verbose_name='hivtestinghistory', blank=True)),
                ('hiv_test_date', models.DateField(null=True, verbose_name='hivtestdate', blank=True)),
                ('hiv_result', models.CharField(max_length=100, null=True, verbose_name='hivresult', blank=True)),
                ('hiv_test_refuse_yn', models.CharField(max_length=20, null=True, verbose_name='hivtestrefuse', blank=True)),
                ('hpv_vac_yn', models.CharField(max_length=20, null=True, verbose_name='hpv', blank=True)),
                ('ab_pain_rptd', models.CharField(max_length=20, null=True, verbose_name='abpainrep', blank=True)),
                ('dysuria_rptd', models.CharField(max_length=20, null=True, verbose_name='dysuriarep', blank=True)),
                ('discharge_rptd', models.CharField(max_length=20, null=True, verbose_name='dischargerep', blank=True)),
                ('gen_lesion_rptd', models.CharField(max_length=20, null=True, verbose_name='genlesionrep', blank=True)),
                ('vag_discharge_on_exam', models.CharField(max_length=20, null=True, verbose_name='vagdischargexam', blank=True)),
                ('lower_ab_pain_on_exam', models.CharField(max_length=20, null=True, verbose_name='lowerabpainexam', blank=True)),
                ('cerv_tend_on_exam', models.CharField(max_length=20, null=True, verbose_name='cervmottenderexam', blank=True)),
                ('adnexal_tend_on_exam', models.CharField(max_length=20, null=True, verbose_name='adnexaltenderexam', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='encounter',
            name='drvs_service_line',
            field=models.CharField(max_length=50, null=True, verbose_name='Service line value from DRVS', blank=True),
        ),
        migrations.AddField(
            model_name='encounter',
            name='health_dept_hiv_refer',
            field=models.CharField(max_length=50, null=True, verbose_name='Patient referred to health department for partner services (PS)', blank=True),
        ),
        migrations.AddField(
            model_name='encounter',
            name='hiv_care_visit_flag',
            field=models.CharField(max_length=20, null=True, verbose_name='HIV care visit flag', blank=True),
        ),
        migrations.AddField(
            model_name='encounter',
            name='hiv_result_date',
            field=models.DateField(null=True, verbose_name='Date HIV result delivered', blank=True),
        ),
        migrations.AddField(
            model_name='encounter',
            name='hiv_test_recommended',
            field=models.CharField(max_length=50, null=True, verbose_name='Recommended HIV test', blank=True),
        ),
        migrations.AddField(
            model_name='encounter',
            name='risk_reduction_counseling_date',
            field=models.DateField(null=True, verbose_name='Date patient provided with risk reduction counseling', blank=True),
        ),
        migrations.AddField(
            model_name='encounter',
            name='risk_screen',
            field=models.CharField(max_length=50, null=True, verbose_name='Sexual and drug using risk screen conducted', blank=True),
        ),
        migrations.AddField(
            model_name='patient',
            name='birth_country',
            field=models.CharField(max_length=50, null=True, verbose_name='Country of Birth', blank=True),
        ),
        migrations.AddField(
            model_name='patient',
            name='housing_status',
            field=models.CharField(max_length=20, null=True, verbose_name='Housing Status', blank=True),
        ),
        migrations.AddField(
            model_name='patient',
            name='income_level',
            field=models.CharField(max_length=20, null=True, verbose_name='Income Level', blank=True),
        ),
        migrations.AddField(
            model_name='patient',
            name='insurance_status',
            field=models.CharField(max_length=20, null=True, verbose_name='Insurance Status', blank=True),
        ),
        migrations.AlterField(
            model_name='allergy',
            name='date_noted',
            field=models.DateField(db_index=True, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='allergy',
            name='name',
            field=models.CharField(db_index=True, max_length=300, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='allergy',
            name='status',
            field=models.CharField(db_index=True, max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='encounter',
            name='dx_codes',
            field=models.ManyToManyField(to='static.Dx_code', db_index=True, blank=True),
        ),
        migrations.AlterField(
            model_name='encounter',
            name='priority',
            field=models.CharField(default='', max_length=10, db_index=True, blank=True, choices=[('3', '3'), ('2', '2'), ('1', '1')]),
        ),
        migrations.AlterField(
            model_name='hospital_problem',
            name='overview',
            field=models.CharField(max_length=800, null=True, verbose_name='Overview', blank=True),
        ),
        migrations.AlterField(
            model_name='hospital_problem',
            name='present_on_adm',
            field=models.CharField(db_index=True, max_length=20, null=True, verbose_name='Present on admission', blank=True),
        ),
        migrations.AlterField(
            model_name='hospital_problem',
            name='present_on_adm_code',
            field=models.IntegerField(null=True, verbose_name='Present on admission code', blank=True),
        ),
        migrations.AlterField(
            model_name='hospital_problem',
            name='principal_prob',
            field=models.CharField(db_index=True, max_length=20, null=True, verbose_name='Principal hospital problem', blank=True),
        ),
        migrations.AlterField(
            model_name='hospital_problem',
            name='principal_prob_code',
            field=models.IntegerField(null=True, verbose_name='Principal hospital problem code', blank=True),
        ),
        migrations.AlterField(
            model_name='hospital_problem',
            name='priority',
            field=models.CharField(db_index=True, max_length=20, null=True, verbose_name='Priority', blank=True),
        ),
        migrations.AlterField(
            model_name='hospital_problem',
            name='priority_code',
            field=models.IntegerField(null=True, verbose_name='Priority code', blank=True),
        ),
        migrations.AlterField(
            model_name='hospital_problem',
            name='status',
            field=models.CharField(db_index=True, max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='immunization',
            name='cpt_code',
            field=models.CharField(max_length=20, null=True, verbose_name='CPT code', blank=True),
        ),
        migrations.AlterField(
            model_name='immunization',
            name='imm_status',
            field=models.CharField(max_length=20, null=True, verbose_name='Immunization Order Status', blank=True),
        ),
        migrations.AlterField(
            model_name='immunization',
            name='patient_class',
            field=models.CharField(max_length=50, null=True, verbose_name='Patient class', blank=True),
        ),
        migrations.AlterField(
            model_name='immunization',
            name='patient_status',
            field=models.CharField(max_length=50, null=True, verbose_name='Patient status', blank=True),
        ),
        migrations.AlterField(
            model_name='labinfo',
            name='zip5',
            field=models.CharField(db_index=True, max_length=5, null=True, verbose_name='5-digit zip', blank=True),
        ),
        migrations.AlterField(
            model_name='laborder',
            name='group_id',
            field=models.CharField(max_length=15, null=True, verbose_name='Placer Order Group', blank=True),
        ),
        migrations.AlterField(
            model_name='laborder',
            name='obs_end_date',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='laborder',
            name='obs_start_date',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='laborder',
            name='order_info',
            field=models.CharField(max_length=100, null=True, verbose_name='Clinical information', blank=True),
        ),
        migrations.AlterField(
            model_name='laborder',
            name='parent_res',
            field=models.CharField(max_length=128, null=True, verbose_name='Parent lab result natural key', blank=True),
        ),
        migrations.AlterField(
            model_name='laborder',
            name='patient_class',
            field=models.CharField(max_length=50, null=True, verbose_name='Patient class', blank=True),
        ),
        migrations.AlterField(
            model_name='laborder',
            name='patient_status',
            field=models.CharField(max_length=50, null=True, verbose_name='Patient status', blank=True),
        ),
        migrations.AlterField(
            model_name='laborder',
            name='reason_code',
            field=models.CharField(max_length=15, null=True, verbose_name='Reason for Order', blank=True),
        ),
        migrations.AlterField(
            model_name='laborder',
            name='reason_code_type',
            field=models.CharField(max_length=25, null=True, verbose_name='Reason code type', blank=True),
        ),
        migrations.AlterField(
            model_name='laborder',
            name='test_status',
            field=models.CharField(max_length=5, null=True, verbose_name='Test status', blank=True),
        ),
        migrations.AlterField(
            model_name='labresult',
            name='collection_date_end',
            field=models.DateTimeField(null=True, verbose_name='Lab Collection End date', blank=True),
        ),
        migrations.AlterField(
            model_name='labresult',
            name='order_type',
            field=models.CharField(max_length=20, null=True, verbose_name='Order type', blank=True),
        ),
        migrations.AlterField(
            model_name='labresult',
            name='patient_class',
            field=models.CharField(max_length=50, null=True, verbose_name='Patient class', blank=True),
        ),
        migrations.AlterField(
            model_name='labresult',
            name='patient_status',
            field=models.CharField(max_length=50, null=True, verbose_name='Patient status', blank=True),
        ),
        migrations.AlterField(
            model_name='labresult',
            name='specimen_num',
            field=models.CharField(max_length=100, null=True, verbose_name='Specimen ID Number', blank=True),
        ),
        migrations.AlterField(
            model_name='labresult',
            name='status_date',
            field=models.DateTimeField(null=True, verbose_name='Result interpretation/status change date', blank=True),
        ),
        migrations.AlterField(
            model_name='labresult_details',
            name='comparator',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='order_extension',
            name='question',
            field=models.CharField(max_length=1000, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='filler_ord_eid',
            field=models.CharField(max_length=100, null=True, verbose_name='Filler Order Number Entity ID', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='filler_ord_nid',
            field=models.CharField(max_length=100, null=True, verbose_name='Filler Order Number Namespace ID', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='filler_ord_uid',
            field=models.CharField(max_length=100, null=True, verbose_name='Filler Order Number Universal ID', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='filler_ord_uid_type',
            field=models.CharField(max_length=100, null=True, verbose_name='Filler Order Number Universal ID Type', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='placer_grp_eid',
            field=models.CharField(max_length=100, null=True, verbose_name='Placer Group Number Entity ID', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='placer_grp_nid',
            field=models.CharField(max_length=100, null=True, verbose_name='Placer Group Number Namespace ID', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='placer_grp_uid',
            field=models.CharField(max_length=100, null=True, verbose_name='Placer Group Number Universal ID', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='placer_grp_uid_type',
            field=models.CharField(max_length=100, null=True, verbose_name='Placer Group Number Universal ID Type', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='placer_ord_eid',
            field=models.CharField(max_length=100, null=True, verbose_name='Placer Order Number Entity ID', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='placer_ord_nid',
            field=models.CharField(max_length=100, null=True, verbose_name='Placer Order Number Namespace ID', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='placer_ord_uid',
            field=models.CharField(max_length=100, null=True, verbose_name='Placer Order Number Universal ID', blank=True),
        ),
        migrations.AlterField(
            model_name='order_idinfo',
            name='placer_ord_uid_type',
            field=models.CharField(max_length=100, null=True, verbose_name='Placer Order Number Universal ID Type', blank=True),
        ),
        migrations.AlterField(
            model_name='patient',
            name='gender',
            field=models.CharField(default='', max_length=20, blank=True, null=True, verbose_name='Gender', db_index=True),
        ),
        migrations.AlterField(
            model_name='patient',
            name='last_update',
            field=models.DateTimeField(null=True, verbose_name='Date when patient information was last updated', blank=True),
        ),
        migrations.AlterField(
            model_name='patient',
            name='zip5',
            field=models.CharField(db_index=True, max_length=5, null=True, verbose_name='5-digit zip', blank=True),
        ),
        migrations.AlterField(
            model_name='patient_addr',
            name='zip5',
            field=models.CharField(db_index=True, max_length=5, null=True, verbose_name='5-digit zip', blank=True),
        ),
        migrations.AlterField(
            model_name='patient_guardian',
            name='zip5',
            field=models.CharField(db_index=True, max_length=5, null=True, verbose_name='5-digit zip', blank=True),
        ),
        migrations.AlterField(
            model_name='prescription',
            name='patient_class',
            field=models.CharField(max_length=50, null=True, verbose_name='Patient Class', blank=True),
        ),
        migrations.AlterField(
            model_name='prescription',
            name='patient_status',
            field=models.CharField(max_length=50, null=True, verbose_name='Patient status', blank=True),
        ),
        migrations.AlterField(
            model_name='problem',
            name='hospital_pl_yn',
            field=models.CharField(max_length=1, null=True, verbose_name='Hospital-based problem, Y or null', blank=True),
        ),
        migrations.AlterField(
            model_name='problem',
            name='status',
            field=models.CharField(db_index=True, max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='specimen',
            name='Received_date',
            field=models.DateTimeField(null=True, verbose_name='Received datetime', blank=True),
        ),
        migrations.AlterField(
            model_name='specimen',
            name='amount_id',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='specimen',
            name='analysis_date',
            field=models.DateTimeField(null=True, verbose_name='Analysis datetime', blank=True),
        ),
        migrations.AlterField(
            model_name='specimen',
            name='fill_nid',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='specimen',
            name='fill_uid',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='specimen',
            name='fill_uidtype',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='specimen',
            name='order_natural_key',
            field=models.CharField(max_length=128, null=True, verbose_name='order_natural_key', blank=True),
        ),
        migrations.AlterField(
            model_name='specimen',
            name='range_enddt',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='specimen',
            name='range_startdt',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='specimen',
            name='specimen_num',
            field=models.CharField(max_length=100, null=True, verbose_name='Specimen ID Number', blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='response_float',
            field=models.FloatField(default=0, null=True, verbose_name='response float', blank=True),
        ),
        migrations.AddField(
            model_name='stiencounterextended',
            name='enc_natural_key',
            field=models.OneToOneField(to='emr.Encounter', to_field='natural_key', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='stiencounterextended',
            name='patient',
            field=models.ForeignKey(blank=True, to='emr.Patient', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='stiencounterextended',
            name='provenance',
            field=models.ForeignKey(to='emr.Provenance', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='stiencounterextended',
            name='provider',
            field=models.ForeignKey(blank=True, to='emr.Provider', null=True, on_delete=models.CASCADE),
        ),
    ]
