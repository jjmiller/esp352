# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0009_RiskFactors_04162019'),

    ]

    operations = [
        migrations.AddField(
            model_name='labresult',
            name='facility_provider',
            field=models.ForeignKey(related_name='lx_facility_provider', blank=True, to='emr.Provider', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='prescription',
            name='facility_provider',
            field=models.ForeignKey(related_name='rx_facility_provider', blank=True, to='emr.Provider', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='prescription',
            name='managing_provider',
            field=models.ForeignKey(related_name='managing_provider', blank=True, to='emr.Provider', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='provider',
            name='provider_type',
            field=models.IntegerField(null=True, verbose_name='Provider Type', blank=True, choices=[(2, 2), (1, 1)]),
        ),
        migrations.AlterField(
            model_name='provider',
            name='npi',
            field=models.CharField(max_length=20, null=True, verbose_name='National Provider ID', blank=True),
        ),
    ]
