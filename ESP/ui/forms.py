'''
                              ESP Health Project
                         Notifiable Diseases Framework
                              User Interface Forms


@authors: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@copyright: (c) 2009 Channing Laboratory
@license: LGPL
'''

from django import forms
from django.db.models import BLANK_CHOICE_DASH

from ESP.hef.base import AbstractLabTest
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import STATUS_CHOICES
from ESP.vaers.heuristics import VaersLxHeuristic
from ESP.vaers.models import LabRule
from ESP.ui.other_labs import STANDARD_LABS, STANDARD_SEARCH_STRINGS
from ESP.settings import STATUS_REPORT_TYPE

class CaseStatusForm(forms.Form):
    '''
    Form for mapping Native Code to LOINC
    '''
    status = forms.ChoiceField(choices=STATUS_CHOICES[:4], required=False)
    comment = forms.CharField(widget=forms.Textarea, required=False)


class CodeMapForm(forms.Form):
    # fake instantiation to get the labs in the drop down
    TEST_CHOICES = [(name, name) for name in AbstractLabTest.get_all_names()]
    standard_labs = STANDARD_LABS
    try:
        rules = LabRule.objects.filter(name='wbc')[0]
    except:
        rules = False
    if rules:
        vaerslabs = VaersLxHeuristic(rules)
        # FIXING MULTIPLE BILIRUBINS IN LIST 
        for name in vaerslabs.get_all_names():
            if (name, name) not in TEST_CHOICES:
                TEST_CHOICES.append((name, name))
    for name in standard_labs:
        if (name, name) not in TEST_CHOICES:
            TEST_CHOICES.append((name, name))

    TEST_CHOICES.sort()
    test_name = forms.ChoiceField(choices=BLANK_CHOICE_DASH + TEST_CHOICES, required=True)
    threshold = forms.FloatField(required=False)
    notes = forms.CharField(widget=forms.Textarea, required=False)
    output_code = forms.CharField(label='LOINC', max_length=100, required=False)
    snomed_pos = forms.CharField(label='SNOMED positive code', max_length=255, required=False)
    snomed_neg = forms.CharField(label='SNOMED neg code', max_length=255, required=False)
    snomed_ind = forms.CharField(label='SNOMED indeterminate code', max_length=255, required=False)


class ConditionForm(forms.Form):
    # condition = forms.ChoiceField(choices=DiseaseDefinition.get_all_condition_choices())
    # old code
    # Condition.condition_choices(wildcard=True))
    pass


class ReferenceCaseForm(forms.Form):
    notes = forms.CharField(required=False, widget=forms.Textarea)
    ignore = forms.BooleanField(required=False)


class ConditionChoiceForm(forms.Form):
    standard_search_strings = STANDARD_SEARCH_STRINGS
    CONDITION_CHOICES = DiseaseDefinition.get_all_test_name_search_strings() + standard_search_strings
    if STATUS_REPORT_TYPE in ['VAERS','BOTH']:
        CONDITION_CHOICES += VaersLxHeuristic.test_name_search_strings
    CONDITION_CHOICES = list(dict.fromkeys(CONDITION_CHOICES)) ## remove duplicates
    CONDITION_CHOICES.sort()
    CONDITION_CHOICES = [(name, name) for name in CONDITION_CHOICES]
    CONDITION_CHOICES = [('ALL', 'All Conditions*')] + CONDITION_CHOICES
    CONDITION_CHOICES = [(None, 'No Conditions')] + CONDITION_CHOICES
    condition = forms.ChoiceField(label='Condition Search', choices=CONDITION_CHOICES, required=False,
                                  help_text='*Search by All Conditions may be very slow')
