STANDARD_LABS = ['alt', 'ast', 'rbc', 'wbc', 'platelet_count', 'cd4', 'hepatitis_c_genotype','creatinine','hematocrit']
STANDARD_SEARCH_STRINGS = ['alt','sgpt','ast','aminotrans','sgot','rbc','red bl','wbc','white bl','platelet','plt','cd4','helper','genotype','creat','hematocrit','hct']
