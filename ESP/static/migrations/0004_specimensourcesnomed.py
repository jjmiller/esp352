# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('static', '0003_load_initial_data'),
    ]

    operations = [
        migrations.CreateModel(
            name='SpecimenSourceSnomed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=64)),
                ('snomed', models.CharField(max_length=15)),
            ],
        ),
    ]
