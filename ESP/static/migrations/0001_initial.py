# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Allergen',
            fields=[
                ('code', models.CharField(max_length=100, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=300, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DrugSynonym',
            fields=[
                ('drugynonym_id', models.AutoField(serialize=False, primary_key=True)),
                ('generic_name', models.CharField(max_length=100, verbose_name='Generic name')),
                ('other_name', models.CharField(max_length=100, verbose_name='Other name')),
                ('comment', models.TextField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Drug Synonym',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Dx_code',
            fields=[
                ('combotypecode', models.CharField(max_length=20, serialize=False, verbose_name='Code type:Code value', primary_key=True)),
                ('code', models.CharField(max_length=10, verbose_name='Code value')),
                ('type', models.CharField(max_length=10, verbose_name='Code type')),
                ('name', models.CharField(max_length=150, verbose_name='Name')),
                ('longname', models.CharField(max_length=1000, verbose_name='Long Name')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FakeAllergen',
            fields=[
                ('code', models.CharField(max_length=100, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=300, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'FAKEALLELRGEN',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FakeDx_Codes',
            fields=[
                ('fakedx_code_id', models.AutoField(serialize=False, primary_key=True)),
                ('dx_codes', models.CharField(max_length=3350, verbose_name='dx_codes')),
                ('group_name', models.CharField(max_length=100, verbose_name='group_name')),
                ('list_length', models.IntegerField(null=True, blank=True)),
                ('weight', models.FloatField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'FAKEDX_CODE',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FakeLabs',
            fields=[
                ('fakelabs_id', models.AutoField(serialize=False, primary_key=True)),
                ('native_code', models.TextField(null=True, blank=True)),
                ('native_name', models.TextField(null=True, blank=True)),
                ('long_name', models.TextField(null=True, blank=True)),
                ('test_sub_category', models.CharField(max_length=100, null=True, verbose_name='Test Sub category', blank=True)),
                ('loinc', models.CharField(max_length=100, null=True, verbose_name='LOINC', blank=True)),
                ('loinc_flag', models.CharField(max_length=100, null=True, verbose_name='LOINC flag', blank=True)),
                ('specimen_source', models.CharField(max_length=100, null=True, verbose_name='Specimen Source', blank=True)),
                ('units_ms', models.CharField(max_length=100, null=True, verbose_name='MS Result Units', blank=True)),
                ('conversion_factor', models.FloatField(null=True, verbose_name='Conversion factor', blank=True)),
                ('units_std', models.CharField(max_length=100, null=True, verbose_name='Std Result Units', blank=True)),
                ('units', models.TextField(null=True, blank=True)),
                ('px', models.CharField(max_length=100, null=True, verbose_name='PX', blank=True)),
                ('px_type', models.CharField(max_length=100, null=True, verbose_name='PX code type', blank=True)),
                ('datatype', models.TextField(null=True, blank=True)),
                ('normal_low', models.FloatField(null=True, blank=True)),
                ('normal_high', models.FloatField(null=True, blank=True)),
                ('critical_low', models.FloatField(null=True, blank=True)),
                ('critical_high', models.FloatField(null=True, blank=True)),
                ('qual_orig', models.TextField(null=True, verbose_name='Qualitative values', blank=True)),
                ('qual_map', models.TextField(null=True, verbose_name='Qualitative map values', blank=True)),
                ('cpt_code', models.TextField(null=True, blank=True)),
                ('weight', models.FloatField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'FAKELABS',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FakeMeds',
            fields=[
                ('fakemeds_id', models.AutoField(serialize=False, primary_key=True)),
                ('long_name', models.TextField(null=True, blank=True)),
                ('ndc_code', models.TextField(null=True, blank=True)),
                ('route', models.TextField(null=True, blank=True)),
                ('weight', models.FloatField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'FAKEMEDS',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FakeVitals',
            fields=[
                ('fakevitals_id', models.AutoField(serialize=False, primary_key=True)),
                ('short_name', models.TextField(null=True, blank=True)),
                ('long_name', models.TextField(null=True, blank=True)),
                ('normal_low', models.FloatField(null=True, blank=True)),
                ('normal_high', models.FloatField(null=True, blank=True)),
                ('units', models.TextField(null=True, blank=True)),
                ('very_low', models.FloatField(null=True, blank=True)),
                ('very_high', models.FloatField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'FAKEVITALS',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='hl7_vocab',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=26, verbose_name='HL7 value')),
                ('description', models.CharField(max_length=250, verbose_name='HL7 value description')),
                ('codesys', models.CharField(max_length=10, verbose_name='HL7 code system name')),
                ('version', models.CharField(max_length=25, verbose_name='HL7 version')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ILI_encounter_type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw_encounter_type', models.CharField(unique=True, max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ImmunizationManufacturer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=3)),
                ('full_name', models.CharField(max_length=200)),
                ('active', models.BooleanField(default=True)),
                ('use_instead', models.ForeignKey(to='static.ImmunizationManufacturer', null=True, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Loinc',
            fields=[
                ('loinc_num', models.CharField(max_length=20, serialize=False, primary_key=True)),
                ('component', models.TextField(null=True, blank=True)),
                ('property', models.TextField(null=True, blank=True)),
                ('time_aspct', models.TextField(null=True, blank=True)),
                ('system', models.TextField(null=True, blank=True)),
                ('scale_typ', models.TextField(null=True, blank=True)),
                ('method_typ', models.TextField(null=True, blank=True)),
                ('relat_nms', models.TextField(null=True, blank=True)),
                ('loinc_class_field', models.TextField(null=True, blank=True)),
                ('source', models.TextField(null=True, blank=True)),
                ('dt_last_ch', models.TextField(null=True, blank=True)),
                ('chng_type', models.TextField(null=True, blank=True)),
                ('comments', models.TextField(null=True, blank=True)),
                ('answerlist', models.TextField(null=True, blank=True)),
                ('status', models.TextField(null=True, blank=True)),
                ('map_to', models.TextField(null=True, blank=True)),
                ('scope', models.TextField(null=True, blank=True)),
                ('norm_range', models.TextField(null=True, blank=True)),
                ('ipcc_units', models.TextField(null=True, blank=True)),
                ('reference', models.TextField(null=True, blank=True)),
                ('exact_cmp_sy', models.TextField(null=True, blank=True)),
                ('molar_mass', models.TextField(null=True, blank=True)),
                ('classtype', models.TextField(null=True, blank=True)),
                ('formula', models.TextField(null=True, blank=True)),
                ('species', models.TextField(null=True, blank=True)),
                ('exmpl_answers', models.TextField(null=True, blank=True)),
                ('acssym', models.TextField(null=True, blank=True)),
                ('base_name', models.TextField(null=True, blank=True)),
                ('final', models.TextField(null=True, blank=True)),
                ('naaccr_id', models.TextField(null=True, blank=True)),
                ('code_table', models.TextField(null=True, blank=True)),
                ('setroot', models.TextField(null=True, blank=True)),
                ('panelelements', models.TextField(null=True, blank=True)),
                ('survey_quest_text', models.TextField(null=True, blank=True)),
                ('survey_quest_src', models.TextField(null=True, blank=True)),
                ('unitsrequired', models.TextField(null=True, blank=True)),
                ('submitted_units', models.TextField(null=True, blank=True)),
                ('relatednames2', models.TextField(null=True, blank=True)),
                ('shortname', models.TextField(null=True, blank=True)),
                ('order_obs', models.TextField(null=True, blank=True)),
                ('cdisc_common_tests', models.TextField(null=True, blank=True)),
                ('hl7_field_subfield_id', models.TextField(null=True, blank=True)),
                ('external_copyright_notice', models.TextField(null=True, blank=True)),
                ('example_units', models.TextField(null=True, blank=True)),
                ('inpc_percentage', models.TextField(null=True, blank=True)),
                ('long_common_name', models.TextField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'LOINC',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ndc',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label_code', models.CharField(db_index=True, max_length=10, null=True, verbose_name='NDC Label Code (leading zeros are meaningless)', blank=True)),
                ('product_code', models.CharField(db_index=True, max_length=5, null=True, verbose_name='NDC Product Code', blank=True)),
                ('trade_name', models.CharField(max_length=200, verbose_name='NDC Trade Name')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Site',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('ili_site', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sitegroup',
            fields=[
                ('group', models.CharField(max_length=100, serialize=False, primary_key=True)),
                ('cdc_site_id', models.CharField(max_length=50, null=True)),
                ('location', models.CharField(max_length=100, null=True)),
                ('zip5', models.CharField(max_length=5, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Vaccine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=128)),
                ('short_name', models.CharField(max_length=60)),
                ('name', models.CharField(max_length=300)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='drugsynonym',
            unique_together=set([('generic_name', 'other_name')]),
        ),
        migrations.AddField(
            model_name='site',
            name='group',
            field=models.ForeignKey(to='static.Sitegroup', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='immunizationmanufacturer',
            name='vaccines_produced',
            field=models.ManyToManyField(to='static.Vaccine'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='allergen',
            name='vaccines',
            field=models.ManyToManyField(to='static.Vaccine', null=True),
            preserve_default=True,
        ),
    ]
