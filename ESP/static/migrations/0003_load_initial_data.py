# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from ESP.utils.utils import LoadFixtureData
from ESP import settings
from django.db import models, migrations
from django.core.management import call_command
from django.conf import settings

db_engine = settings.DATABASES['default']['ENGINE']

if db_engine == 'django.db.backends.postgresql_psycopg2':
  class Migration(migrations.Migration):

    dependencies = [
        ('static', '0002_auto_20160513_1020'),
    ]

    operations = [
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'data') + ".json")),
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'dx_code') + ".json")),
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'hl7_vocab') + ".json")),
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'loinc') + ".json")),
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'ndc') + ".json")),
        migrations.RunSQL("select setval('static_allergen_vaccines_id_seq',(select max(id) from static_allergen_vaccines)+1);"),
        migrations.RunSQL("select setval('static_drugsynonym_drugynonym_id_seq',(select max(drugynonym_id) from static_drugsynonym)+1);"),
        migrations.RunSQL("select setval('static_fakedx_codes_fakedx_code_id_seq',(select max(fakedx_code_id) from static_fakedx_codes)+1);"),
        migrations.RunSQL("select setval('static_fakelabs_fakelabs_id_seq',(select max(fakelabs_id) from static_fakelabs)+1);"),
        migrations.RunSQL("select setval('static_fakemeds_fakemeds_id_seq',(select max(fakemeds_id) from static_fakemeds)+1);"),
        migrations.RunSQL("select setval('static_fakevitals_fakevitals_id_seq',(select max(fakevitals_id) from static_fakevitals)+1);"),
        migrations.RunSQL("select setval('static_hl7_vocab_id_seq',(select max(id) from static_hl7_vocab)+1);"),
        migrations.RunSQL("select setval('static_ili_encounter_type_id_seq',(select max(id) from static_ili_encounter_type)+1);"),
        migrations.RunSQL("select setval('static_immunizationmanufacturer_id_seq',(select max(id) from static_immunizationmanufacturer)+1);"),
        migrations.RunSQL("select setval('static_immunizationmanufacturer_vaccines_produced_id_seq',(select max(id) from static_immunizationmanufacturer_vaccines_produced)+1);"),
        migrations.RunSQL("select setval('static_ndc_id_seq',(select max(id) from static_ndc)+1);"),
        migrations.RunSQL("select setval('static_rx_lookup_id_seq',(select max(id) from static_rx_lookup)+1);"),
        migrations.RunSQL("select setval('static_rx_type_id_seq',(select max(id) from static_rx_type)+1);"),
        migrations.RunSQL("select setval('static_site_id_seq',(select max(id) from static_site)+1);"),
        migrations.RunSQL("select setval('static_vaccine_id_seq',(select max(id) from static_vaccine)+1);"),
    ]

else:
  class Migration(migrations.Migration):

    dependencies = [
        ('static', '0002_auto_20160513_1020'),
    ]

    operations = [
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'data') + ".json")),
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'dx_code') + ".json")),
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'hl7_vocab') + ".json")),
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'loinc') + ".json")),
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'ndc') + ".json")),
    ]
