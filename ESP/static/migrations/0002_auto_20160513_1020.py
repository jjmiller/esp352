# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('static', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ESPCondition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('centerid', models.CharField(max_length=1, null=True)),
                ('patid', models.CharField(max_length=128)),
                ('condition', models.CharField(max_length=100)),
                ('date', models.IntegerField()),
                ('age_at_detect', models.IntegerField(null=True)),
                ('age_group_5yr', models.CharField(max_length=5, null=True)),
                ('age_group_10yr', models.CharField(max_length=5, null=True)),
                ('age_group_ms', models.CharField(max_length=5, null=True)),
                ('criteria', models.CharField(max_length=2000, null=True)),
                ('status', models.CharField(max_length=32, null=True)),
                ('notes', models.TextField(null=True)),
            ],
            options={
                'db_table': 'esp_condition',
            },
        ),
        migrations.CreateModel(
            name='RX_Lookup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField(max_length=501)),
                ('rx_dose', models.TextField(null=True)),
                ('conversion_factor', models.FloatField(null=True)),
                ('dosage_strength', models.FloatField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='RX_Type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(unique=True, max_length=50)),
            ],
        ),
        migrations.AlterField(
            model_name='allergen',
            name='vaccines',
            field=models.ManyToManyField(to='static.Vaccine'),
        ),
        migrations.AddField(
            model_name='rx_lookup',
            name='type',
            field=models.ForeignKey(to='static.RX_Type', on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name='espcondition',
            unique_together=set([('patid', 'condition', 'date')]),
        ),
    ]
