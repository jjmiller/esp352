'''
                                  ESP Health
                          Heuristic Events Framework
                              Command Line Runner


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@copyright: (c) 2009-2010 Channing Laboratory
@license: LGPL
'''

from django.core.management.base import BaseCommand

from ESP.settings import HEF_THREAD_COUNT
from ESP.hef.base import BaseHeuristic


#===============================================================================
#
#--- ~~~ Main ~~~
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    
class Command(BaseCommand):
    
    help = 'Generate heuristic events. Runs heuristics for NAMEs if specified; otherwise, runs all heuristics.'

    def add_arguments(self, parser):
        parser.add_argument('NAMES', nargs='*', type=str)
        parser.add_argument('--list', action='store_true', dest='list',
            help='List all abstract tests')
        parser.add_argument('--threads', action='store', dest='thread_count', default=HEF_THREAD_COUNT,
            type=int, metavar='COUNT', help='Run in COUNT threads')
        parser.add_argument('-n', '--new', action='store_true', dest='new')


    def handle(self, *args, **options):
        if options['list']:
            test =[]
            for h in BaseHeuristic.get_all():
                if not test.__contains__(h.short_name):
                    print(h.short_name)
                    test.append( h.short_name ) 
            return
        if not options['NAMES']:
            BaseHeuristic.generate_all(thread_count=options['thread_count'])
        else:
            BaseHeuristic.generate_by_name(name_list=options['NAMES'], thread_count=options['thread_count'])
