'''
                                  ESP Health
                         Notifiable Diseases Framework
                                  Data Models
                                  
@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@contact: http://esphealth.org
@copyright: (c) 2009-2010 Channing Laboratory
@license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
'''

import datetime

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Max
from django.db.models import Q

from ESP.conf.models import ConditionConfig
from ESP.conf.models import LabTestMap
from ESP.conf.models import ReportableLab, ReportableExtended_Variables
from ESP.conf.models import ReportableMedication
from ESP.conf.models import STATUS_CHOICES
from ESP.emr.models import Encounter
from ESP.emr.models import LabResult
from ESP.emr.models import Patient
from ESP.emr.models import Prescription
from ESP.emr.models import Provider
from ESP.hef.base import BaseLabResultHeuristic
from ESP.hef.base import DiagnosisHeuristic
from ESP.hef.models import Event
from ESP.hef.models import Timespan
from ESP.nodis.base import UnknownDiseaseException
from ESP.settings import REQUEUE_REF_DATE
from ESP.static.models import DrugSynonym
from ESP.static.models import Dx_code
from ESP.utils import date_from_str
from ESP.utils.utils import log

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DISPOSITIONS = [
    ('exact', 'Exact'),
    ('similar', 'Similar'),
    ('missing', 'Missing'),
    ('new', 'New'),
]


# -------------------------------------------------------------------------------
#
# --- Exceptions
#
# -------------------------------------------------------------------------------

class NodisException(BaseException):
    '''
    A Nodis-specific error
    '''


class InvalidPattern(NodisException):
    '''
    Could not understand the specified pattern
    '''


class InvalidHeuristic(NodisException):
    '''
    You specified a heuristic that is not registered with the system.
    '''


# -------------------------------------------------------------------------------
#
# --- Pattern Matching Logic
#
# -------------------------------------------------------------------------------


class Case(models.Model):
    '''
    A case of (reportable) disease
    '''
    condition = models.CharField('Common English name for this medical condition',
                                 max_length=100, blank=False, db_index=True)
    date = models.DateField(blank=False, db_index=True)
    patient = models.ForeignKey(Patient, blank=False, on_delete=models.CASCADE)
    provider = models.ForeignKey(Provider, blank=False, on_delete=models.CASCADE)
    criteria = models.CharField('Criteria on which case was diagnosed',
                                max_length=2000, blank=True, null=True, db_index=True)
    source = models.CharField('What algorithm created this case?',
                              max_length=255, blank=False)
    status = models.CharField('Case status', max_length=32,
                              blank=False, choices=STATUS_CHOICES, default='AR')
    notes = models.TextField(blank=True, null=True)
    reportables = models.TextField(blank=True, null=True)
    # Timestamps:
    created_timestamp = models.DateTimeField(auto_now_add=True, blank=False)
    updated_timestamp = models.DateTimeField(auto_now=True, blank=False)
    sent_timestamp = models.DateTimeField(blank=True, null=True)
    #
    # Events that define this case
    #
    events = models.ManyToManyField(Event, blank=False, related_name='case')
    followup_events = models.ManyToManyField(Event, blank=False, related_name='followup')

    timespans = models.ManyToManyField(Timespan, blank=False)
    followup_sent = models.BooleanField('Followup event sent?', default=False)

    isactive = models.BooleanField('Case is active?', default=True)
    inactive_date = models.DateField(blank=True, null=True)

    class Meta:
        permissions = [('view_phi', 'Can view protected health information'), ]
        unique_together = ['patient', 'condition', 'date', 'source']
        ordering = ['id']

    def __init__(self, *args, **kwargs):
        super(Case, self).__init__(*args, **kwargs)
        if self.source:
            self._set_disease_algorithm()
        else:
            self.disease_algorithm = None

    def _set_disease_algorithm(self):
        from ESP.nodis.base import DiseaseDefinition
        try:
            self.disease_algorithm = DiseaseDefinition.get_by_uri(self.source)
        except UnknownDiseaseException:
            log.warn("Could not find a plugin for uri {}".format(self.source))
            self.disease_algorithm = None

    def get_disease_algorithm(self):
        if self.disease_algorithm is None:
            self._set_disease_algorithm()

        return self.disease_algorithm

    @property
    def plugin_installed(self):
        return self.get_disease_algorithm() is not None

    @property
    def condition_config(self):
        '''
        Return the ConditionConfig object for this case's condition
        '''
        return ConditionConfig.objects.filter(name=self.condition).first()

    @property
    def first_provider(self):
        '''
        Provider for chronologically first event
        '''
        return self.events.order_by('date').first().tag_set.first().content_object.provider

    #
    # Events by class
    #
    @property
    def all_events(self):
        # This method was formerly necessary because there were events before, 
        # after etc.  It is no longer required but retained for compatibility.
        return self.events.all()

    @property
    def lab_results(self):
        return LabResult.objects.filter(events__in=self.all_events).order_by('date')

    @property
    def reportable_lab_results(self):
        return self.lab_results.filter(
            native_code__in=LabTestMap.objects.filter(reportable=True).values_list('native_code', flat=True))

    @property
    def encounters(self):
        return Encounter.objects.filter(events__in=self.all_events).order_by('date')

    @property
    def prescriptions(self):
        return Prescription.objects.filter(events__in=self.all_events).order_by('date')

    @property
    def first_lab(self):
        reportable_case_labs = self.reportable_lab_results
        if not len(reportable_case_labs) > 0:
            lr = self.makeDummyLabForCase()
            if lr:
                count, events = Event.create(
                    name="Dummy Lab",
                    source="dummy_lab",
                    date=lr.date,
                    patient=lr.patient,
                    provider=lr.provider,
                    emr_record=lr
                )
                self.events.add(events[0])

        return self.reportable_lab_results.filter(
            events__in=self.events.filter(
                ~Q(name__icontains='indeterminate')
            )
        ).first()

    @property
    def heuristics_labs(self):
        from ESP.nodis.base import DiseaseDefinition
        reportable_codes = set()
        try:
            disease_definition = DiseaseDefinition.get_by_short_name(self.condition)
        except UnknownDiseaseException:
            return LabResult.objects.none()

        # get native codes from lab heuristics
        for heuristic in disease_definition.event_heuristics:
            if isinstance(heuristic, BaseLabResultHeuristic):
                reportable_codes |= set(
                    LabTestMap.objects.filter(test_name=heuristic.test_name, reportable=True).values_list('native_code',
                                                                                                          flat=True))
        q_obj = Q(patient=self.patient)
        q_obj &= Q(native_code__in=reportable_codes)
        # if no recurrence interval but there is a days after or before in the condition config
        # use that
        # if there is no recurrence interval or condition config use ref_date as end_date.
        if disease_definition.recurrence_interval:
            start = self.date - datetime.timedelta(days=disease_definition.recurrence_interval)
            end = self.date + datetime.timedelta(days=disease_definition.recurrence_interval)
            q_obj &= Q(date__gte=start)
            q_obj &= Q(date__lte=end)
        else:
            conf = self.condition_config
            if conf:
                start = self.date - datetime.timedelta(days=conf.lab_days_before)
                end = self.date + datetime.timedelta(days=conf.lab_days_after)
                q_obj &= Q(date__gte=start)
                q_obj &= Q(date__lte=end)
            else:
                if not REQUEUE_REF_DATE or REQUEUE_REF_DATE == 'TODAY':
                    end = datetime.date.today()
                else:
                    end = date_from_str(REQUEUE_REF_DATE)
                q_obj &= Q(date__lte=end)

        labs = LabResult.objects.filter(q_obj).distinct()
        return labs

    @property
    def reportable_labs(self):
        # redmine 516 use abstract labs in reportables instead of native codes.
        conf = self.condition_config
        nk = str(self.id) + '-' + str(self.patient_id) + '-' + self.date.strftime('%Y%m%d') + '-' + self.condition
        lab_qobj = Q(natural_key=nk)
        q_obj = None
        if conf:
            reportable_labnames = set(
                ReportableLab.objects.filter(condition=self.condition).values_list('native_name', flat=True))
            if reportable_labnames:
                reportable_codes = set()
                for labname in reportable_labnames:
                    reportable_codes |= set(
                        LabTestMap.objects.filter(test_name=labname, reportable=True).values_list('native_code',
                                                                                                  flat=True))
                q_obj = Q(patient=self.patient)
                q_obj &= Q(native_code__in=reportable_codes)
                start = self.date - datetime.timedelta(days=conf.lab_days_before)
                end = self.date + datetime.timedelta(days=conf.lab_days_after)
                q_obj &= Q(date__gte=start)
                q_obj &= Q(date__lte=end)
        if q_obj is not None:
            lab_qobj = (q_obj) | lab_qobj

        labs = LabResult.objects.filter(lab_qobj).distinct()
        labs |= self.heuristics_labs
        if self.first_lab:
            labs = labs.exclude(pk=self.first_lab.pk)

        return labs.distinct()

    def makeDummyLabForCase(self):
        try:
            lr = LabResult.createDummyLab(self.patient, self,
                                          LabTestMap.objects.get(test_name=self.condition + ' not a test').output_code)
            return lr
        except ObjectDoesNotExist:
            pass

    @property
    def reportable_extended_variables(self):
        extended_variables = None
        conf = self.condition_config
        if conf:
            reportable_extended_variables = set(
                ReportableExtended_Variables.objects.filter(condition=self.condition).values_list(
                    'abstract_ext_var__native_string', flat=True))
            if reportable_extended_variables:
                q_obj = Q(patient=self.patient)
                q_obj &= Q(native_code__in=reportable_extended_variables)
                start = self.date - datetime.timedelta(days=conf.ext_var_days_before)
                end = self.date + datetime.timedelta(days=conf.ext_var_days_after)
                q_obj &= Q(date__gte=start)
                q_obj &= Q(date__lte=end)
                extended_variables = LabResult.objects.filter(q_obj)

        if extended_variables:
            return extended_variables.distinct()
        else:
            return LabResult.objects.none()

    @property
    def reportable_dx_codes(self):
        return Dx_code.objects.filter(reportabledx_code__condition=self.condition_config).distinct()

    @property
    def reportable_encounters(self):
        encs = None
        q_obj = Q(patient=self.patient)
        conf = self.condition_config
        rep_dx_codes = Dx_code.objects.none()
        heuristic_dx_codes = Dx_code.objects.none()
        if conf:
            rep_dx_codes = self.reportable_dx_codes
            if rep_dx_codes:
                q_obj &= Q(dx_codes__in=rep_dx_codes)
                start = self.date - datetime.timedelta(days=conf.dx_code_days_before)
                end = self.date + datetime.timedelta(days=conf.dx_code_days_after)
                q_obj &= Q(date__gte=start)
                q_obj &= Q(date__lte=end)
                encs = Encounter.objects.filter(q_obj)
        #########################################################################
        from ESP.nodis.base import DiseaseDefinition
        try:
            disease_definition = DiseaseDefinition.get_by_short_name(self.condition)
            for heuristic in disease_definition.event_heuristics:
                if isinstance(heuristic, DiagnosisHeuristic):
                    for dx_code_query in heuristic.dx_code_queries:
                        if not heuristic_dx_codes:
                            heuristic_dx_codes = Dx_code.objects.filter(dx_code_query.dx_code_q_obj)
                        else:
                            heuristic_dx_codes |= Dx_code.objects.filter(dx_code_query.dx_code_q_obj)
            q_obj = Q(patient=self.patient)
            heuristic_dx_codes = heuristic_dx_codes.distinct()
            q_obj &= Q(dx_codes__in=heuristic_dx_codes)

            if disease_definition.recurrence_interval:
                start = self.date - datetime.timedelta(days=disease_definition.recurrence_interval)
                end = self.date + datetime.timedelta(days=disease_definition.recurrence_interval)
                q_obj &= Q(date__gte=start)
                q_obj &= Q(date__lte=end)
            else:
                if conf:
                    start = self.date - datetime.timedelta(days=conf.dx_code_days_before)
                    end = self.date + datetime.timedelta(days=conf.dx_code_days_after)
                    q_obj &= Q(date__gte=start)
                    q_obj &= Q(date__lte=end)
                else:
                    if not REQUEUE_REF_DATE or REQUEUE_REF_DATE == 'TODAY':
                        end = datetime.date.today()
                    else:
                        end = date_from_str(REQUEUE_REF_DATE)
                    q_obj &= Q(date__lte=end)

            if not encs:
                encs = Encounter.objects.filter(q_obj)
            else:
                encs |= Encounter.objects.filter(q_obj)
            # log_query('Encounters for %s' % self, encs)
        except UnknownDiseaseException:
            pass
        rep_codes = rep_dx_codes | heuristic_dx_codes
        if encs is None:
            encs = Encounter.objects.none()
        return encs.distinct(), rep_codes.distinct()

    @property
    def reportable_prescriptions(self):
        conf = self.condition_config
        prescriptions = Prescription.objects.none()
        if conf:
            med_names = set(ReportableMedication.objects.filter(condition=conf).values_list('drug_name', flat=True))
            if med_names:
                med_names = list(med_names)
                # redmine 515
                all_drugs = DrugSynonym.generics_plus_synonyms(med_names)
                q_obj = Q(name__icontains=all_drugs[0])
                for med in all_drugs:
                    q_obj |= Q(name__icontains=med)
                q_obj &= Q(patient=self.patient)
                start = self.date - datetime.timedelta(days=conf.med_days_before)
                end = self.date + datetime.timedelta(days=conf.med_days_after)
                q_obj &= Q(date__gte=start)
                q_obj &= Q(date__lte=end)
                prescriptions = Prescription.objects.filter(q_obj).distinct()
                ######################################################
        from ESP.nodis.base import DiseaseDefinition
        try:
            disease_definition = DiseaseDefinition.get_by_short_name(self.condition)
            med_names = disease_definition.medications
            if not med_names:
                return prescriptions
            else:
                med_names = list(med_names)
                # redmine 515
                all_drugs = DrugSynonym.generics_plus_synonyms(med_names)
                q_obj = Q(name__icontains=all_drugs[0])
                for med in all_drugs:
                    q_obj |= Q(name__icontains=med)
                q_obj &= Q(patient=self.patient)
                if disease_definition.recurrence_interval:
                    start = self.date - datetime.timedelta(days=disease_definition.recurrence_interval)
                    end = self.date + datetime.timedelta(days=disease_definition.recurrence_interval)
                    q_obj &= Q(date__gte=start)
                    q_obj &= Q(date__lte=end)
                else:
                    if conf:
                        start = self.date - datetime.timedelta(days=conf.med_days_before)
                        end = self.date + datetime.timedelta(days=conf.med_days_after)
                        q_obj &= Q(date__gte=start)
                        q_obj &= Q(date__lte=end)
                    else:
                        if not REQUEUE_REF_DATE or REQUEUE_REF_DATE == 'TODAY':
                            end = datetime.date.today()
                        else:
                            end = date_from_str(REQUEUE_REF_DATE)
                        q_obj &= Q(date__lte=end)

                prescriptions |= Prescription.objects.filter(q_obj).distinct()
        except UnknownDiseaseException:
            pass
        return prescriptions.distinct()

    def unreported(self, candidates=None, reported=None):
        if not candidates:
            return Case.objects.none()
        if reported is None:
            reported = Reported.from_case(self.pk)

        reported_type = ContentType.objects.get_for_model(candidates[0].__class__)
        reported = reported.filter(content_type=reported_type).values_list('object_id')
        return candidates.exclude(pk__in=reported)

    def dx_from_encounters(self, encounters):
        dx = Dx_code.objects.none()
        for encounter in encounters:
            dx |= encounter.dx_codes.filter(combotypecode__in=self.reportable_dx_codes.values_list('combotypecode',
                                                                                                   flat=True))

        return dx

    def get_unreported(self):
        from collections import namedtuple
        reported = Reported.from_case(self.pk)
        Unreported = namedtuple('Unreported',
                                'encounters, dx_codes, prescriptions, labs, extended_variables, followups')
        encounters = self.reportable_encounters
        unreported_encounters = self.unreported(encounters[0], reported)
        dx_codes = self.dx_from_encounters(unreported_encounters)

        unreported = Unreported(encounters=unreported_encounters,
                                dx_codes=dx_codes,
                                prescriptions=self.unreported(self.reportable_prescriptions, reported),
                                labs=self.unreported(self.reportable_labs, reported),
                                extended_variables=self.unreported(self.reportable_extended_variables, reported),
                                followups=self.unreported(self.followup_events.all(), reported))

        return unreported

    def __str__(self):
        return '%s # %s' % (self.condition, self.pk)

    def str_line(self):
        '''
        Returns a single-line string representation of the Case instance
        '''
        values = self.__dict__
        return '%(date)-10s    %(id)-8s    %(condition)-30s' % values

    @classmethod
    def str_line_header(cls):
        '''
        Returns a header describing the fields returned by str_line()
        '''
        values = {'date': 'DATE', 'id': 'CASE #', 'condition': 'CONDITION'}
        return '%(date)-10s    %(id)-8s    %(condition)-30s' % values

    def __get_collection_date(self):
        '''
        Returns the most recent specimen collection date across all case event labs
        '''
        if self.lab_results:
            return self.lab_results.aggregate(maxdate=Max('collection_date'))['maxdate']
        else:
            return None

    collection_date = property(__get_collection_date)

    def __get_result_date(self):
        '''
        Returns the most recent result date across all case event labs
        '''
        if self.lab_results:
            return self.lab_results.aggregate(maxdate=Max('result_date'))['maxdate']
        else:
            return None

    result_date = property(__get_result_date)

    def get_disease_report_value(self, report_field):
        try:
            case_report_field = self.get_disease_algorithm().report_field(report_field, self)
            if case_report_field is None:
                raise ValueError
            return case_report_field
        except (AttributeError, NotImplementedError, ValueError):
            return report_field


ACTIVE_STATUS_CHOICES = [
    ('I', 'I - Initial detection and activation'),
    ('R', 'R - Reactivated'),
    ('D', 'D - Deactivated'),
]

ACTIVE_STATUS_CHANGE_REASONS = [
    ('Q', 'Q - Qualifying events'),
    ('D', 'D - Disqualifying events'),
    ('E', 'E - Elapsed time')
]


class CaseActiveHistory(models.Model):
    '''
    The history of case active/inactive status and related records
    '''
    case = models.ForeignKey(Case, db_index=True, on_delete=models.CASCADE)
    status = models.CharField(max_length=8, choices=ACTIVE_STATUS_CHOICES)
    date = models.DateField('Case (re)activation or de-activation date', db_index=True)
    change_reason = models.CharField(max_length=8, choices=ACTIVE_STATUS_CHANGE_REASONS, blank=False)
    latest_event_date = models.DateField(blank=False, db_index=True)
    content_type = models.ForeignKey(ContentType, db_index=True, blank=True, null=True, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(db_index=True, blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        unique_together = [('case', 'date')]  # assumes status change smallest unit is 1 day..

    def __str__(self):
        return 'Case status %s at %s' % (self.status, self.date)

    def verbose_str(self):
        return 'Case # %s, Status %s at %s' % (self.case, self.status, self.date)

    @classmethod
    def create(cls,
               case,
               status,
               date,
               change_reason,
               event_rec,
               ):
        '''
        Creates a status history linked to case and an event.
        @return: 1
        @rtype: (int)
        '''
        if event_rec is None:
            raise ValueError("event_rec can not be None")
        content_type = ContentType.objects.get_for_model(event_rec)
        h = CaseActiveHistory(
            case=case,
            status=status,
            date=date,
            change_reason=change_reason,
            latest_event_date=event_rec.date,
            content_type=content_type,
            object_id=event_rec.pk,
        )
        h.save()
        log.debug('Created new case active history: %s' % h)
        return 1

    @classmethod
    def update_or_create(cls,
               case,
               status,
               date,
               change_reason,
               event_rec,
               ):
        '''
        Creates or updates a status history linked to case and an event.
        @return: 1
        @rtype: (int)
        '''
        if event_rec is None:
            raise ValueError("event_rec can not be None")
        content_type = ContentType.objects.get_for_model(event_rec)

        h, created = CaseActiveHistory.objects.update_or_create(
            case=case,
            date=date,
            defaults={'status': status,
                      'change_reason': change_reason,
                      'latest_event_date': event_rec.date,
                      'content_type': content_type,
                      'object_id': event_rec.pk},
        )
        log.debug('Created/Updated new case active history: %s' % h)
        return 1

class CaseStatusHistory(models.Model):
    '''
    The current review status of a given Case
    '''
    case = models.ForeignKey(Case, blank=False, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now=True, blank=False, db_index=True)
    old_status = models.CharField(max_length=10, choices=STATUS_CHOICES, blank=False)
    new_status = models.CharField(max_length=10, choices=STATUS_CHOICES, blank=False)
    changed_by = models.CharField(max_length=30, blank=False)
    comment = models.TextField(blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.case, self.timestamp)

    class Meta:
        verbose_name = 'Case Status History'
        verbose_name_plural = 'Case Status Histories'


class ReportRun(models.Model):
    '''
    A run of the case_report command
    '''
    timestamp = models.DateTimeField(auto_now=True)
    hostname = models.CharField('Host on which data was loaded', max_length=255, blank=False)


class Report(models.Model):
    '''
    A reporting message generated from one or more Case objects
    '''
    timestamp = models.DateTimeField(auto_now=True, blank=False)
    run = models.ForeignKey(ReportRun, blank=False, on_delete=models.CASCADE)
    cases = models.ManyToManyField(Case, blank=False)
    filename = models.CharField(max_length=512, blank=False)
    sent = models.BooleanField('Case status was set to sent?', default=False)
    message = models.TextField('Case report message', blank=False)


class CaseReport(models.Model):
    case = models.OneToOneField(Case, on_delete=models.CASCADE)
    initial_lab = models.ForeignKey(LabResult, blank=True, null=True, on_delete=models.CASCADE)
    initial_lab_report = models.TextField(blank=True, null=True)
    na_trmt_true_sent = models.BooleanField(default=False)
    na_5_true_sent = models.BooleanField(default=False)


class CaseReportReported(models.Model):
    case_report = models.ForeignKey(CaseReport, on_delete=models.CASCADE)
    time_sent = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['time_sent']


class Reported(models.Model):
    case_reported = models.ForeignKey(CaseReportReported, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, db_index=True, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(db_index=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        unique_together = [('case_reported', 'content_type', 'object_id')]

    @classmethod
    def from_case(cls, case_pk):
        return cls.objects.filter(case_reported__case_report__case=case_pk)

    @classmethod
    def set_reported(cls, case_report, obj):
        reported = cls()
        reported.case_reported = case_report
        reported.content_type = ContentType.objects.get_for_model(obj)
        reported.object_id = obj.pk
        reported.save()


# ===============================================================================
#
# Case Validator Models
#
# -------------------------------------------------------------------------------

class ReferenceCaseList(models.Model):
    '''
    A group of reference cases loaded from the same source.
    '''
    source = models.CharField(max_length=255, blank=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return 'List # %s' % self.pk

    class Meta:
        verbose_name = 'Reference Case List'


class ReferenceCase(models.Model):
    '''
    A reference case -- provided from an external source (such as manual Health 
    Department reporting systems), this is presumed to be a known valid case.
    '''
    list = models.ForeignKey(ReferenceCaseList, blank=False, on_delete=models.CASCADE)
    #
    # Data provided by Health Dept etc
    #
    patient = models.ForeignKey(Patient, blank=True, null=True, on_delete=models.CASCADE)
    condition = models.CharField(max_length=100, blank=False, db_index=True)
    date = models.DateField(blank=False, db_index=True)
    #
    ignore = models.BooleanField(default=False, db_index=True)
    notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return '%s - %s - %s' % (self.condition, self.date, self.patient.mrn)

    class Meta:
        verbose_name = 'Reference Case'


class ValidatorRun(models.Model):
    '''
    One run of the validator tool.
    '''
    timestamp = models.DateTimeField(blank=False, auto_now_add=True)
    list = models.ForeignKey(ReferenceCaseList, blank=False, on_delete=models.CASCADE)
    complete = models.BooleanField(blank=False, default=False)  # Run is complete?
    related_margin = models.IntegerField(blank=False)
    #
    # Notes
    #
    notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return 'Run # %s' % self.pk

    class Meta:
        verbose_name = 'Validator Run'

    def __get_results(self):
        q_obj = Q(run=self) & ~Q(ref_case__ignore=True)
        qs = ValidatorResult.objects.filter(q_obj)
        # log_query('Validator results', qs)
        return qs

    results = property(__get_results)

    def __get_missing(self):
        return self.results.filter(disposition='missing')

    missing = property(__get_missing)

    def __get_exact(self):
        return self.results.filter(disposition='exact')

    exact = property(__get_exact)

    def __get_similar(self):
        return self.results.filter(disposition='similar')

    similar = property(__get_similar)

    def __get_new(self):
        return self.results.filter(disposition='new')

    new = property(__get_new)

    def __get_ignored(self):
        return ValidatorResult.objects.filter(run=self, ref_case__ignore=True)

    ignored = property(__get_ignored)

    def percent_ignored(self):
        return 100.0 * float(self.ignored.count()) / self.results.count()

    def percent_exact(self):
        return 100.0 * float(self.exact.count()) / self.results.count()

    def percent_similar(self):
        return 100.0 * float(self.similar.count()) / self.results.count()

    def percent_missing(self):
        return 100.0 * float(self.missing.count()) / self.results.count()

    def percent_new(self):
        return 100.0 * float(self.new.count()) / self.results.count()

    @classmethod
    def latest(cls):
        '''
        Return the most recent complete ValidatorRun
        '''
        return cls.objects.filter(complete=True).order_by('-timestamp').first()


class ValidatorResult(models.Model):
    run = models.ForeignKey(ValidatorRun, blank=False, on_delete=models.CASCADE)
    ref_case = models.ForeignKey(ReferenceCase, blank=True, null=True, on_delete=models.CASCADE)
    condition = models.CharField(max_length=100, blank=False, db_index=True)
    date = models.DateField(blank=False, db_index=True)
    disposition = models.CharField(max_length=30, blank=False, choices=DISPOSITIONS)
    #
    # ManyToManyFields populated only for missing cases
    #
    events = models.ManyToManyField(Event, blank=True)
    cases = models.ManyToManyField(Case, blank=True)
    lab_results = models.ManyToManyField(LabResult, blank=True)
    encounters = models.ManyToManyField(Encounter, blank=True)
    prescriptions = models.ManyToManyField(Prescription, blank=True)

    def patient(self):
        return self.ref_case.patient

    class Meta:
        verbose_name = 'Validator Result'

    def __str__(self):
        return 'Result # %s' % self.pk

    def date_diff(self):
        '''
        For 'similar' cases, date_diff() returns the difference between the 
        reference case date and the ESP case date.
        '''
        if not self.disposition == 'similar':
            raise RuntimeError('date_diff() makes sense only for "similar" cases')
        return self.ref_case.date - self.cases.all()[0].date
