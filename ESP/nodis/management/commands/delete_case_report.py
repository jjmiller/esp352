from django.core.management import BaseCommand, CommandError
from django.db import transaction

from ESP.nodis.models import Case, CaseReport


class Command(BaseCommand):
    help = 'Delete Case Report'

    def add_arguments(self, parser):
        parser.add_argument('case', nargs='*', type=str, help='ID of case to delete report for')


    @transaction.atomic
    def handle(self, *args, **options):
        if len(options['case']) != 1:
            raise CommandError("Please provide 1 case id")

        cid = options['case'][0]
        try:
            case = Case.objects.get(pk=cid)
        except Case.DoesNotExist:
            raise CommandError('No Case with id {}'.format(cid))

        try:
            CaseReport.objects.get(case=case).delete()
        except CaseReport.DoesNotExist:
            raise CommandError('This case does not have an existing Case Report')

