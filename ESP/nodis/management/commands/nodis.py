#!/usr/bin/env python
'''
                                  ESP Health
                         Notifiable Diseases Framework
                              Command Line Runner


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@copyright: (c) 2009 Channing Laboratory
@license: LGPL
'''
from django.core.management.base import BaseCommand

from ESP.nodis.base import DiseaseDefinition, UnknownDiseaseException
from ESP.utils.utils import log


class Command(BaseCommand):
    
    help = 'Generate Nodis cases'
    
    def add_arguments(self, parser):
        parser.add_argument('conditions', nargs='*', type=str)
        parser.add_argument('--list', action='store_true', dest='list', help='List available diseases')
        parser.add_argument('--dependencies', action='store_true', dest='dependencies', default=False,
            help='Generate dependencies before generating diseases')

    def handle(self, *args, **options):
        if options['list']:
            for disease in DiseaseDefinition.get_all():
                print(disease.short_name)
            return 
        disease_list = set()
        if not options['conditions']:
            disease_list = DiseaseDefinition.get_all()
        else:
            for short_name in options['conditions']:
                try:
                    disease_list.add(DiseaseDefinition.get_by_short_name(short_name))
                except UnknownDiseaseException as e:
                    log.warning(e)
                    continue

        DiseaseDefinition.generate_all(
            disease_list = disease_list, 
            dependencies = options['dependencies'], 
            )