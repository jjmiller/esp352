# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('nodis', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CaseActiveHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=2, choices=[('I', 'I - Initial detection and activation'), ('R', 'R - Reactivated'), ('D', 'D - Deactivated')])),
                ('date', models.DateField(verbose_name='Case (re)activation or de-activation date', db_index=True)),
                ('change_reason', models.CharField(max_length=2, choices=[('Q', 'Q - Qualifying events'), ('D', 'D - Disqualifying events')])),
                ('latest_event_date', models.DateField(db_index=True)),
                ('object_id', models.PositiveIntegerField(db_index=True)),
            ],
        ),
        migrations.AddField(
            model_name='case',
            name='inactive_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='case',
            name='isactive',
            field=models.BooleanField(default=True, verbose_name='Case is active?'),
        ),
        migrations.AlterField(
            model_name='validatorresult',
            name='cases',
            field=models.ManyToManyField(to='nodis.Case', blank=True),
        ),
        migrations.AlterField(
            model_name='validatorresult',
            name='encounters',
            field=models.ManyToManyField(to='emr.Encounter', blank=True),
        ),
        migrations.AlterField(
            model_name='validatorresult',
            name='events',
            field=models.ManyToManyField(to='hef.Event', blank=True),
        ),
        migrations.AlterField(
            model_name='validatorresult',
            name='lab_results',
            field=models.ManyToManyField(to='emr.LabResult', blank=True),
        ),
        migrations.AlterField(
            model_name='validatorresult',
            name='prescriptions',
            field=models.ManyToManyField(to='emr.Prescription', blank=True),
        ),
        migrations.AddField(
            model_name='caseactivehistory',
            name='case',
            field=models.ForeignKey(to='nodis.Case', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='caseactivehistory',
            name='content_type',
            field=models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name='caseactivehistory',
            unique_together=set([('case', 'date')]),
        ),
    ]
