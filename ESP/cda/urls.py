'''
                              ESP Health Project
User Interface Module
                               URL Configuration

@authors: Jay Boyer <jboyer@commoninf.com>
'''


from django.conf.urls import url
from ESP.cda import views

urlpatterns = [
    url(r'^$', views.post),   
]
