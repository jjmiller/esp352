from django.apps import AppConfig


class CdaInputConfig(AppConfig):
    name = 'cda_input'
