logrotate for CDA ESP consumption instructions

cd /srv/esp/prod/ESP/scripts
cp cda.logrotate.sample /srv/esp/scripts/cda.logrotate
cp cda_compress_clean.sh.sample /srv/esp/scripts/cda_compress_clean.sh
cd /srv/esp/scripts
chmod +x cda_compress_clean.sh

# logrotate requires the config file to be owned by root
sudo chown root:root cda.logrotate

# add a cron job to run logrotate daily as root and delete files older than 30 days
sudo crontab -e

# add a line like the following to run the shell script daily
0  1  *  *  * /srv/esp/scripts/cda_compress_clean.sh
