'''
                                  ESP Health
                         Notifiable Diseases Framework
                         Clinician Inbasket HL7 Message

@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth Informatics Inc. http://www.commoninf.com
@copyright: (c) 2012 Commonwealth Informatics Inc.
@license: LGPL - http://www.gnu.org/licenses/lgpl-3.0.txt
'''


from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from ESP.static.models import Vaccine, ImmunizationManufacturer, Dx_code
from ESP.vaers.models import Case, Questionnaire, Sender
from ESP.conf.models import SiteHL7

from ESP.utils.hl7_builder.segments import MSH, EVN, PID, OBX, PV1, TXA
from ESP.utils.utils import log

from ESP.settings import UPLOAD_SERVER
from ESP.settings import UPLOAD_USER
from ESP.settings import UPLOAD_PASSWORD
from ESP.settings import UPLOAD_PATH
from ESP.settings import SITE_NAME
from ESP.settings import VAERS_OVERRIDE_CLINICIAN_REVIEWER
from ESP.emr.models import Provider
from ESP.settings import CASE_REPORT_OUTPUT_FOLDER

import ftplib
import io
import datetime



UNKNOWN_VACCINE = Vaccine.objects.get(short_name='unknown')
UNKNOWN_MANUFACTURER = ImmunizationManufacturer.objects.get(code='UNK')
now = datetime.datetime.now()

class HL7_clinbasket(object):
    '''
    Format and generate the HL7 message for delivery to the Clinician Inbasket.
    Content is per ESP-VAERS algorithm specification.
    '''
    #initialize with a questionnaire record
    def __init__(self, questionnaire):
        self.ques = questionnaire
        self.case = Case.objects.get(id=self.ques.case_id)
        self.immunizations = self.case.immunizations.all()
        self.site_hl7 = SiteHL7.objects.all()
        
    def makeMSH(self):
        msh = MSH()
        # the following values for MetroHealth systems
        #todo: these values should be provided via a plugin for metrohealth
        msh.sending_application = self.site_hl7.get(location='sending_application').value
        msh.sending_facility = self.site_hl7.get(location='sending_application').value
        msh.receiving_application = 'EPIC'
        msh.receiving_facility = 'EPICCARE'
        msh.time = now.strftime("%Y%m%d%H%M%S")
        msh.message_type = 'MDM^T02'
        msh.message_control_id='ESP'+str(self.ques.id).zfill(7)
        msh.processing_id = 'P'
        msh.version='2.3.1'
        return msh

    def makeEVN(self):
        #minimal values required for clinicians inbasket
        evn = EVN()
        # probably  need more here
        evn.event_type_code = 'T02'
        evn.recorded_datetime = self.case.date.strftime("%Y%m%d%H%M%S")
        
        return evn
        
    def makePID(self):
        patient = self.case.patient
        pid = PID()
        pid.patient_internal_id = patient.mrn + '^^^^' + self.site_hl7.get(location='patient_internal_id').value
        #We currently don't need the following but could provide at some point if desired.
        #pid.patient_name = patient._get_name
        #pid.date_of_birth = utils.str_from_date(patient.date_of_birth)
        #pid.sex = patient.gender
        #pid.race = patient.race
        #pid.patient_address = patient._get_address
        #pid.home_phone = u'(%s) %s' % (patient.areacode, patient.phone_number)
        #pid.primary_language = patient.home_language
        #pid.marital_status = patient.marital_stat
        #pid.country_code = patient.country
        return pid

    def makePV1(self):
        pv1 = PV1()
        #we don't currently do anything with this record.
        return pv1

    def makeTXA(self,messaged):
        #again, the DI and UN values seem to be MetroHealth specific.  These should be plugin specified
        txa = TXA()
        enc = self.case
        ques = self.ques
        txa.report_type = self.site_hl7.get(location='report_type').value
        txa.activity_date=enc.date.strftime("%Y%m%d%H%M%S")
        txa.primary_activity_provider=self.site_hl7.get(location='primary_activity_provider').value
        txa.originator_codename=self.site_hl7.get(location='originator_codename').value
        txa.unique_document_number='^^ESPVAERS_' + str(ques.id)
        txa.document_completion_status='AU'
        txa.document_availability_status='AV'
        txa.distributed_copies=messaged
        return txa
    
    def makeOBX(self, rowcode, override):
        #This does all the heavy lifting
        obx = OBX()
        patient = self.case.patient
        if rowcode!='RP':
            AEs = self.case.adverse_events.distinct().order_by('date')
            j=1
            obx.set_id=str(j).zfill(3)
            obx.value_type='TX'
            obx.identifier='REP^Report Text'
            if not override:
                salutation='Dear ' + str(self.ques.provider.first_name) + ', '
                if self.ques.provider.title in ['MD','DO']:
                    salutation='Dear Dr. ' + str(self.ques.provider.last_name) + ', '
                obx.value=salutation 
            else:
                obx.value='This message was generated as part of an initial pilot of the VAERS automatic detection system.'
            obxstr=str(obx)+'\r'
            j += 1
            obx.set_id=str(j).zfill(3)
            obx.value_type='TX'
            obx.identifier='REP^Report Text'
            if self.case.adverse_events.filter(category='1_rare').exists():
                #Rare, severe AE
                caseDescription = ('Your patient, ' + patient.first_name + ' ' + patient.last_name +  ', may have experienced a serious adverse event following a recent vaccination. ' + 
                        'PLEASE NOTE: DUE TO THE SEVERITY OF THE ADVERSE EVENT, A VAERS REPORT WILL AUTOMATICALLY BE GENERATED AND SUBMITTED TO CDC-VAERS IF YOU DO NOT RESPOND TO THIS MESSAGE. ' +
                        patient.first_name + ' ' + patient.last_name + ', was recently noted to have: ')
            elif self.case.adverse_events.filter(category='3_reportable').exists():
                #Possible AE
                caseDescription = ('Your patient, ' + patient.first_name + ' ' + patient.last_name +  ', was recently diagnosed with a reportable vaccination adverse reaction.  ' +
                        'PLEASE NOTE: DUE TO THE REPORTABLE NATURE OF THE DIAGNOSIS, A VAERS REPORT WILL AUTOMATICALLY BE GENERATED AND SUBMITTED TO CDC-VAERS IF YOU DO NOT RESPOND TO THIS MESSAGE. ' +
                         patient.first_name + ' ' + patient.last_name + ', was recently noted to have: ')
            elif self.case.adverse_events.filter(category='2_possible').exists():
                #Possible AE
                caseDescription = ('Your patient, ' + patient.first_name + ' ' + patient.last_name +  ', may have experienced an adverse reaction to a recent vaccination. ' +
                                   'Please note this is a preliminary assessment and must be reviewed, otherwise it will be presumed to be a false positive detection. ' +
                                   patient.first_name + ' ' + patient.last_name + ', was recently noted to have: ')
            obx.value=caseDescription
            obxstr=obxstr+str(obx)+'\r'
            i=1
            for AE in AEs:
                if ContentType.objects.get_for_id(AE.content_type_id).model.startswith('encounter'):
                    dx_codes = AE.matching_rule_explain.split()
                    for dx_code in dx_codes:
                        if dx_code.startswith('icd'):
                            dx_code = dx_code[:-1] if dx_code.endswith(':') else dx_code
                            obx.value = '(' + str(i) + ') a diagnosis of ' + Dx_code.objects.get(combotypecode=dx_code).longname + ' on ' + str(AE.encounterevent.date)
                            j += 1
                            obx.set_id=str(j).zfill(3)
                            i += 1
                            obxstr=obxstr+str(obx)+'\r'
                elif ContentType.objects.get_for_id(AE.content_type_id).model.startswith('prescription'):
                    obx.value = '(' + str(i) + ') a prescription for ' + AE.prescriptionevent.content_object.name + ' on ' + str(AE.prescriptionevent.content_object.date)
                    j += 1
                    obx.set_id=str(j).zfill(3)
                    i += 1
                    obxstr=obxstr+str(obx)+'\r'
                elif ContentType.objects.get_for_id(AE.content_type_id).model.startswith('labresult'):
                    obx.value = ('(' + str(i) + ') a lab test for ' + AE.labresultevent.content_object.native_name 
                            + ' with a result of ' + AE.labresultevent.content_object.result_string + ' on ' + str(AE.labresultevent.content_object.result_date) )
                    j += 1
                    obx.set_id=str(j).zfill(3)
                    i += 1
                    obxstr=obxstr+str(obx)+'\r'
                elif ContentType.objects.get_for_id(AE.content_type_id).model.startswith('allergy'):
                        #adding the term 'allergy' to the name, as it is otherwise confusing with the test data.
                        #this may not be the case with real allergen names and if so the code will need to be revised.
                    obx.value = '(' + str(i) + ') an allergic reaction to ' + AE.allergyevent.content_object.name + ' on ' + str(AE.allergyevent.content_object.date)
                    j += 1
                    obx.set_id=str(j).zfill(3)
                    i += 1
                    obxstr=obxstr+str(obx)+'\r'
            obx.value = patient.first_name + ' ' + patient.last_name + ' was vaccinated with: '
            j += 1
            obx.set_id=str(j).zfill(3)
            obxstr=obxstr+str(obx)+'\r'
            i=1
            for imm in self.case.immunizations.values('name','date').distinct():
                obx.value = '(' + str(i) + ') ' + imm.get('name') + ' on ' + str(imm.get('date'))
                j += 1
                obx.set_id=str(j).zfill(3)
                i += 1
                obxstr=obxstr+str(obx)+'\r' 
            if self.case.adverse_events.filter(category='3_reportable').exists():
                obx.value =  'Can you confirm your patient experienced an adverse event (a possible side effect) of a vaccination? '
            else:
                obx.value =  'Is it possible your patient experienced an adverse event (a possible side effect) of a vaccination? '
            j += 1
            obx.set_id=str(j).zfill(3)
            obxstr=obxstr+str(obx)+'\r'
            obx.value='If you wish, we can submit an electronic report to the CDC/FDA\'s Vaccine Adverse Event Reporting System (VAERS) on your behalf.'
            j += 1
            obx.set_id=str(j).zfill(3)
            obxstr=obxstr+str(obx)+'\r'
            obx.value=u'This note was automatically generated by the Electronic medical record Support for Public Health (ESPnet), a joint venture of ' + self.site_hl7.get(location='site_name').value  + ', the Centers for Disease Control and Prevention, and Harvard Pilgrim Health Care Institute. The project is funded by the Centers for Disease Control and Prevention. If you have clinical questions about an adverse event, please contact the CDC/FDA\'s Vaccine Adverse Event Reporting System helpline at 1-800-822-7967 email: info@vaers.org. If you have questions about this project please contact the ' + self.site_hl7.get(location='site_name').value  + ' Physician liaison ' + self.site_hl7.get(location='site_liaison').value
            j += 1
            obx.set_id=str(j).zfill(3)
            obxstr=obxstr+str(obx)+'\r'
            obx.value='Please click the link below to review and comment on this issue.'
            j += 1
            obx.set_id=str(j).zfill(3)
            obxstr=obxstr+str(obx)+'\r'
            return obxstr
        elif rowcode=='RP':
        #last obx record is not supposed to have a set_id value for some reason 
            obx.set_id=''
            obx.value_type='RP'
            obx.identifier='Review and comment on this issue at:'
            obx.value='http://' + SITE_NAME + '/vaers/digest/' + self.ques.digest + '^EPIC^LINK^WEBURL^OTHER' 
            return obx

    def make_msgs(self):
        '''
        Assembles an hl7 message file of VAERs case for Clinician's In Basket
        '''
        patient = self.case.patient
        if patient.mrn:
            senderset=Provider.objects.filter(id__in=Sender.objects.all().values_list('provider_id'))
            senderset_count = senderset.count()
            if VAERS_OVERRIDE_CLINICIAN_REVIEWER=='' and senderset_count==0:
                messaged=self.ques.provider.natural_key
                override=False
            elif VAERS_OVERRIDE_CLINICIAN_REVIEWER!='' and senderset_count==0:
                messaged = VAERS_OVERRIDE_CLINICIAN_REVIEWER
                override=True
            elif VAERS_OVERRIDE_CLINICIAN_REVIEWER!='' and senderset_count>0:
                try:
                    senderset.get(natural_key=self.ques.provider.natural_key)
                    messaged = (VAERS_OVERRIDE_CLINICIAN_REVIEWER + '~' + 
                            self.ques.provider.natural_key + '^' + self.ques.provider.first_name + ' ' + self.ques.provider.last_name)
                    override=False
                except ObjectDoesNotExist:
                    messaged = VAERS_OVERRIDE_CLINICIAN_REVIEWER
                    override=True
            all_text = (str(self.makeMSH()) + '\r' +
                    str(self.makeEVN()) + '\r' + 
                    str(self.makePID()) + '\r' + 
                    str(self.makePV1()) + '\r' + 
                    str(self.makeTXA(messaged)) + '\r' + 
                    self.makeOBX('main',override) +
                    str(self.makeOBX('RP',override)) )
            #the cStringIO.StringIO object is built in memory, not saved to disk.
            #hl7file = cStringIO.StringIO()
            hl7file = open(CASE_REPORT_OUTPUT_FOLDER + "/clinbox_msg_ID" + '_' + str(self.ques.id) + '.txt',"w+")
            hl7file.write(all_text)
            hl7file.seek(0)
            #if transmit_ftp(hl7file, 'clinbox_msg_ID' + '_' + str(self.ques.id) + '.txt'):
            Questionnaire.objects.filter(id=self.ques.id).update(inbox_message=all_text)
            hl7file.close()
        else:
            msg='Patient has no MRN.  ID: ' + str(patient.id)
            log.warning(msg)

def transmit_ftp(fileObj, filename):
        '''
        Upload a file using cleartext FTP.  Adapted from ESP.nodis.management.commands.case_report.py
        '''
        log.info('Transmitting case report via FTP')
        log.debug('FTP server: %s' % UPLOAD_SERVER)
        log.debug('FTP user: %s' % UPLOAD_USER)
        log.debug('Attempting to connect...')
        conn = ftplib.FTP(UPLOAD_SERVER, UPLOAD_USER, UPLOAD_PASSWORD)
        log.debug('Connected to %s' % UPLOAD_SERVER)
        log.debug('CWD to %s' % UPLOAD_PATH)
        conn.cwd(UPLOAD_PATH)
        command = 'STOR ' + filename
        try:
            conn.storbinary(command, fileObj)
            log.info('Successfully uploaded Clin Inbasket HL7 message')
        except BaseException as e:
            log.error('FTP ERROR: %s' % e)
            return False
        return True
