from django import forms

# TODO: I think this should be ModelForm so that the form values reflect the data model
# See Creating forms from models
from ESP.vaers.models import Questionnaire, CASE_YESNO_CHOICES, CASE_TYPE_CHOICES

CASE_RESPONSE_CHOICES = (
    ('Q', 'Yes, submit the Adverse Event Report to CDC/FDA VAERS Reporting System'),
    ('FP', 'No'),
)


class DateInput(forms.DateInput):
    input_type = 'date'


class QuestionnaireForm(forms.ModelForm):
    state = forms.ChoiceField(label='Possible Adverse Event?', choices=CASE_RESPONSE_CHOICES,
                              widget=forms.RadioSelect(attrs={'onchange': 'togglecomment();'}))
    patient_recovered = forms.ChoiceField(label='Has the patient recovered from the adverse event(s)?',
                                          choices=(('Y', 'Yes'), ('N', 'No'), ('U', 'Unknown')),
                                          widget=forms.RadioSelect,
                                          required=False)
    message_ishelpful = forms.ChoiceField(label='Was this message helpful?',
                                          choices=CASE_YESNO_CHOICES,
                                          widget=forms.RadioSelect,
                                          required=False)
    satisfaction_num_msg = forms.ChoiceField(label='Has the number of messages recently been',
                                          choices=CASE_TYPE_CHOICES,
                                          widget=forms.RadioSelect,
                                          required=False)

    class Meta:
        model = Questionnaire
        fields = ['state',
                  'event_comment',
                  'patient_recovered',
                  'result_visit',
                  'result_emergency',
                  'result_hosp',
                  'result_hosp_days',
                  'result_hosp_name',
                  'result_hosp_city',
                  'result_hosp_state',
                  'result_prolong',
                  'result_threat',
                  'result_disability',
                  'result_death',
                  'result_death_date',
                  'result_congenital',
                  'result_unknown',
                  'result_none',
                  'message_ishelpful',
                  'satisfaction_num_msg',
                  'assess_reporting'
                  ]
        widgets = {
            'event_comment': forms.Textarea(attrs={'cols': '55', 'rows': '5'}),
            'assess_reporting': forms.Textarea(attrs={'cols': '55', 'rows': '5'}),
            'result_death_date': DateInput,
        }
