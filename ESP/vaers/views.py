#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from wsgiref.util import FileWrapper
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden, Http404
from django.shortcuts import render

from ESP.emr.models import Encounter, Prescription, LabResult, Allergy
from ESP.settings import VAERS_LINELIST_PATH
from ESP.utils.utils import log, Flexigrid
from ESP.vaers.forms import QuestionnaireForm
from ESP.vaers.models import AdverseEvent, Case, Questionnaire, ADVERSE_EVENT_CATEGORY_ACTION

PAGE_TEMPLATE_DIR = 'pages/vaers/'
WIDGET_TEMPLATE_DIR = 'widgets/vaers/'


@login_required
def index(request):
    # Complete query and present results
    cases = Case.paginate()
    return render(request, PAGE_TEMPLATE_DIR + 'home.html',
                  {'cases': cases,
                   'page': 1
                   })


@login_required
def list_cases(request):
    # This is a ajax call. Our response contains only the result page.

    # Page may be passed in the query string and must be a positive number

    grid = Flexigrid(request)
    page = grid.page

    # Complete query and present results
    cases = Case.paginate(page=int(page))
    total = Case.objects.all().count()
    return render(request, WIDGET_TEMPLATE_DIR + 'case_grid.json',
                  {'cases': cases,
                   'total': total,
                   'page': page},
                  content_type='application/json'
                  )


@login_required
def notify(request, id):
    case = AdverseEvent.by_id(id)
    if request.method == 'POST' and case:
        try:
            if not case.is_fake():
                raise TypeError("{} for id {} is not fake".format(case, id))
            email = request.POST.get('email', None)
            case.mail_notification(email_address=email)
            result = 'Successfully sent report on case %s to %s' % (
                case.id, email)
            return HttpResponse(result)
        except Exception as why:
            log.warn('Exception during email notification: %s' % why)
    else:
        return render(request, PAGE_TEMPLATE_DIR + 'notify.html')


@login_required
def report(request):
    cases = AdverseEvent.objects.all()
    return render(request, PAGE_TEMPLATE_DIR + 'report.html',
                  {'cases': cases})


# @param id : is the primary key of the questionnaire or the digest value, depending on ptype
def case_details(request, ptype, id):
    '''
    This view presents a user with case details and a form to update a specific questionnaire 
    It can accept either a digest value or a questionnaire id, and uses ptype to determine which is being provided.
    If the details are accessed via digest, user may be anonymous.  Otherwise, user must be logged in and have the 
    vaers.view_phi permission
    '''
    category = None
    if ptype == 'case':
        if request.user.has_perm('vaers.view_phi'):
            questionnaire = Questionnaire.objects.get(id=id)
        else:
            return HttpResponseForbidden(
                'You cannot access this page unless you are logged in and have permission to view PHI.')
    elif ptype == 'digest':
        questionnaire = Questionnaire.by_digest(id)
        if not any(x for x in ['AR', 'AS'] if x == questionnaire.state):
            return HttpResponse('You have already processed this case.  Thank you for your attention.')
    else:
        # should never get here due to regex in vaers/urls.py for this view, but just in case...
        return HttpResponse('<h2>Vaers page type "' + ptype + '" not found.</h2>')

    case = questionnaire.case
    # another just in case catch...
    if not case:
        raise Http404

    provider = questionnaire.provider
    if not provider:
        # likewise, not likely to be a problem since provider has not-null foreign key attributes in db.
        return HttpResponseForbidden('No provider in questionnaire')

    if request.method == 'POST':
        form = QuestionnaireForm(request.POST, instance=questionnaire)
    else:
        form = QuestionnaireForm()

    if request.method == 'POST' and form.is_valid():
        questionnaire_form = form.save()
        return HttpResponse(
                'Thank you for providing instruction and feedback for this case.  You may close the browser window')

    else:
        # mail_admins('ESP:VAERS - User viewed case report',
        #            'Case %s.\nProvider %s \n.' % (case, provider))
        print(form.errors)
        return render(request, PAGE_TEMPLATE_DIR + 'present.html', {
            'case': case,
            'questionnaire': questionnaire,
            'form': form,
            'content_type_enc': ContentType.objects.get_for_model(Encounter),
            'content_type_lx': ContentType.objects.get_for_model(LabResult),
            'content_type_rx': ContentType.objects.get_for_model(Prescription),
            'content_type_all': ContentType.objects.get_for_model(Allergy),
            'ptype': ptype,
            'formid': id
        })


@login_required
def download_vae_listing(request):
    if request.user.has_perm('vaers.view_phi'):
        filename = VAERS_LINELIST_PATH + "vaers_linelist_phi.csv"
        vfile = open(filename, 'r')
    else:
        filename = VAERS_LINELIST_PATH + "vaers_linelist_nophi.csv"
        vfile = open(filename, 'r')
    response = HttpResponse(FileWrapper(vfile), content_type='application/csv')
    response['Content-Disposition'] = 'attachment; filename=vae_linelist.csv'
    return response
