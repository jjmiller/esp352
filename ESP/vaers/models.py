# -*- coding:utf-8 -*-
import datetime
import hashlib
import os
import random
import time

from django.contrib.contenttypes.fields import GenericForeignKey
from django.core.mail import EmailMessage
from django.urls import reverse
from django.db import models
from django.db.models import Q
from django.template.loader import get_template

from django.contrib.contenttypes.models import ContentType

from ESP.conf.common import DEIDENTIFICATION_TIMEDELTA, EPOCH
from ESP.emr.choices import WORKFLOW_STATES  # FIXME: 'esp' module is deprecated
from ESP.emr.models import Encounter, LabResult, Provider, PRIORITY_TYPES
from ESP.emr.models import Patient, Immunization
from ESP.static.models import Dx_code, Vaccine
from ESP.utils.utils import log, make_date_folders
from ESP.settings import DATA_DIR
from . import settings

CLUSTERING_REPORTS_DIR = os.path.join(DATA_DIR, 'vaers', 'clustering_reports')
HL7_MESSAGES_DIR = os.path.join(DATA_DIR, 'vaers', 'hl7_messages')

# types of action types
# 1_rare: (send) Rare, severe adverse event on VSD list
# 2_possible: (confirm) Possible novel adverse event not previously associated with vaccine
# 3_reportable: (send) dx_code describes vaccine reaction.

# TODO use this later for header in report for suggested action
ADVERSE_EVENT_CATEGORY_ACTION = [
    ('1_rare', 'Automatically confirmed, rare, severe adverse event associated with vaccine.'),
    ('2_possible', 'Confirm, Possible novel adverse event not previously associated with vaccine'),
    ('3_reportable', 'Automatically confirmed, dx code describes vaccine reaction')
]

ADVERSE_EVENT_CATEGORIES = [
    ('1_rare', '1_rare'),
    ('2_possible', '2_possible'),
    ('3_reportable', '3_reportable')
]

CASE_TYPE_CHOICES = (
    ('approp', 'Appropriate'),
    ('frequent', 'Too Frequent')
)

CASE_YESNO_CHOICES = (
    ('True', 'Yes'),
    ('False', 'No'),

)

CASE_STATUS_STATE = {
    'Q': 'Yes, submit the adverse event report to CDC/FDA VAERS Reporting System',
    'FP': 'No'
}

VAERS_VERSION = '0.6'


class AdverseEventManager(models.Manager):
    def cases_to_report(self):
        now = datetime.datetime.now()
        week_ago = now - datetime.timedelta(days=7)
        # TODO use the state instead of this
        confirmed = Q(state='Q')
        rare = Q(category='1_rare', state='AR', created_on__lte=week_ago)
        reportable = Q(category='3_reportable', state='AR', created_on__lte=week_ago)

        return self.filter(confirmed | rare | reportable)


class AdverseEvent(models.Model):
    objects = AdverseEventManager()
    # Model Fields
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    date = models.DateField()
    gap = models.IntegerField(null=True)
    matching_rule_explain = models.CharField(max_length=200)
    category = models.CharField(max_length=20, choices=ADVERSE_EVENT_CATEGORIES)
    digest = models.CharField(max_length=200, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    version = models.CharField(max_length=20, null=True, default=VAERS_VERSION)

    # Generic foreign key - any kind of EMR record can be tagged
    #    http://docs.djangoproject.com/en/dev/ref/contrib/contenttypes/
    content_type = models.ForeignKey(ContentType, db_index=True, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(db_index=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    FIXTURE_DIR = os.path.join(os.path.dirname(__file__), 'fixtures', 'events')
    last_known_value = models.FloatField('Last known Numeric Test Result', blank=True, null=True)
    last_known_date = models.DateField(blank=True, null=True)
    priority = models.IntegerField(blank=False, choices=PRIORITY_TYPES, default=3, db_index=True)

    @staticmethod
    def fakes():
        return AdverseEvent.objects.filter(AdverseEvent.fake_q)

    @staticmethod
    def delete_fakes():
        AdverseEvent.fakes().delete()

    @staticmethod
    def by_id(id):
        try:
            klass = AdverseEvent.objects.get(id=id).content_type
            return klass.model_class().objects.get(id=id)
        except:
            return None

    @staticmethod
    def paginated(page=1, results_per_page=100):
        # Set limits for query
        floor = max(page - 1, 0) * results_per_page
        ceiling = floor + results_per_page

        # We are only getting the fake ones. There is no possible case
        # in our application where we need paginated results for real
        # cases.
        return AdverseEvent.fakes()[floor:ceiling]

    @staticmethod
    def make_deidentified_fixtures():
        for ev in AdverseEvent.objects.all():
            event = AdverseEvent.by_id(ev.id)
            event.make_deidentified_fixture()

    @staticmethod
    def build_from_fixture():
        import json

        event_dir = AdverseEvent.FIXTURE_DIR

        for f in [os.path.join(event_dir, f) for f in os.listdir(event_dir)
                  if os.path.basename(f).startswith('vaers')]:
            fixture_file = open(f, 'rb')
            data = json.loads(fixture_file.read().strip())
            event_data, patient_data = data['event'], data['patient']
            immunizations = data['immunizations']
            encounter = data.get('encounter')
            lab_result = data.get('lab_result')

            if not (encounter or lab_result):
                raise ValueError('Not an Encounter Event nor LabResultEvent')

            patient = Patient(
                natural_key=patient_data['identifier'],
                first_name=patient_data['first_name'],
                last_name=patient_data['last_name'],
                date_of_birth=patient_data['date_of_birth']
            )

            patient.save()

            if encounter:
                raw_encounter_type = ContentType.objects.get_for_model(EncounterEvent)
                event = EncounterEvent(content_type=raw_encounter_type)
                event_encounter = Encounter(
                    patient=patient, date=encounter['date'],
                    temperature=encounter['temperature'])
                event_encounter.save()
                event.encounter = event_encounter
                # need to parse the type 
                for code in encounter['dx_codes']:
                    dx_code = Dx_code.objects.get(code=code['code'], type=code['type'])
                    event.encounter.dx_codes.add(dx_code)

            if lab_result:
                lx_type = ContentType.objects.get_for_model(LabResultEvent)
                event = LabResultEvent(content_type=lx_type)
                event_lx = LabResult(patient=patient, date=lab_result['date'],
                                     result_float=lab_result['result_float'],
                                     result_string=lab_result['result_string'],
                                     ref_unit=lab_result['unit'])
                event_lx.loinc = lab_result['loinc']
                event_lx.save()
                event.lab_result = event_lx

            event.date = event_data['date']
            event.category = event_data['category']
            event.matching_rule_explain = event_data['matching_rule_explain']
            event.save()

            for immunization in [Immunization.objects.create(
                    patient=patient, date=imm['date']) for imm in immunizations]:
                event.immunizations.add(immunization)

            fixture_file.close()

    @staticmethod
    def send_notifications():
        now = datetime.datetime.now()
        three_days_ago = now - datetime.timedelta(days=3)

        # Category 3 (cases that need clinicians confirmation for report)
        must_confirm = Q(category='2_possible')

        # Category 2 (cases that are reported by default, i.e, no comment
        # from the clinician after 72 hours since the detection.
        may_receive_comments = Q(category='1_rare', created_on__gte=three_days_ago)
        # TODO work based on cases not events .. 
        cases_to_notify = AdverseEvent.objects.filter(must_confirm | may_receive_comments)

        for case in cases_to_notify:
            try:
                case.mail_notification()
            except Exception as why:
                print('Failed to send in case %s.\nReason: %s' % (case.id, why))

    def make_deidentified_fixture(self):
        outfile = open(os.path.join(AdverseEvent.FIXTURE_DIR, 'vaers_event.%s.json' % self.id), 'w')
        outfile.write(self.render_json_fixture())
        outfile.close()

    fake_q = Q(immunizations__name='FAKE')

    def temporal_report(self):
        results = []
        for imm in Case.objects.get(adverse_events=self).immunizations.all():
            patient = imm.patient
            results.append({
                'id': imm.natural_key,
                'vaccine_date': imm.date,
                'event_date': self.date,
                'days_to_event': (self.date - imm.date).days,
                'immunization_name': imm.name,
                'event_description': self.matching_rule_explain,
                'patient_age': patient._get_age_str(),
                'patient_gender': patient.gender
            })

        return results

    def render_temporal_report(self):
        buf = []
        for result in self.temporal_report():
            buf.append(
                '\t'.join([str(x) for x in [
                    result['id'], result['vaccine_date'],
                    result['event_date'], result['days_to_event'],
                    result['immunization_name'],
                    result['event_description'],
                    result['patient_age'], result['patient_gender']
                ]]))

        return '\n'.join(buf)

    def is_fake(self):
        return self.patient.is_fake()

    def provider(self):
        return self.patient.pcp

    def verification_url(self):
        return reverse('verify_case', kwargs={'key': self.digest})

    def mail_notification(self, email_address=None):
        from django.contrib.sites.models import Site
        from ESP.settings import ADMINS, DEBUG

        current_site = Site.objects.get_current()

        recipient = email_address or settings.EMAIL_RECIPIENT

        if DEBUG:
            admins_address = [x[1] for x in ADMINS]
            who_to_send = admins_address + [recipient]
        else:
            who_to_send = [recipient]

        params = {
            'case': self,
            'provider': self.provider(),
            'patient': self.patient,
            'url': 'http://%s%s' % (current_site, self.verification_url()),
            'misdirected_email_contact': settings.EMAIL_SENDER
        }

        templates = {
            '1_rare': 'email_messages/notify_category_two',
            '2_possible': 'email_messages/notify_category_three'
        }

        html_template = templates[self.category] + '.html'
        # text_template = templates[self.category] + '.txt'

        html_msg = get_template(html_template).render(params)
        # text_msg = get_template(text_template).render(params)

        msg = EmailMessage(settings.EMAIL_SUBJECT, html_msg, settings.EMAIL_SENDER, who_to_send)
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()

    def _deidentify_patient(self, days_to_shift):
        # From patient personal data, we can get rid of
        # everything, except for date of birth. Vaers Reports are
        # dependant on the patient's age.

        fake_patient = Patient.make_mock(save_on_db=False)
        new_dob = self.patient.date_of_birth - datetime.timedelta(days=days_to_shift)
        fake_patient.date_of_birth = new_dob
        return fake_patient

    def _deidentified_immunizations(self, patient, days_to_shift):
        if days_to_shift < 1:
            raise ValueError("days_to_shift must be greater than 1")
        if not patient.is_fake():
            raise TypeError('{} is not a fake patient'.format(patient))

        deidentified = []
        for imm in Case.objects.get(adverse_events=self).immunizations.all():
            new_date = imm.date - datetime.timedelta(days=days_to_shift)
            fake_imm = Immunization.make_mock(imm.vaccine, patient, new_date)
            deidentified.append(fake_imm)

        return deidentified

    def deidentified(self):
        '''
        Returns an event that is derived from "real" data, but
        containing no identifiable information about patients. 
        '''
        # A patient's date of birth is crucial in identification, but
        # is necessary for VAERS reports (age). To keep information
        # accurate, we shift the patient date of birth by a random
        # delta (DEIDENTIFICATION_TIMEDELTA), and do the same to all
        # date-related information in the event.

        days_to_shift = random.randrange(1, DEIDENTIFICATION_TIMEDELTA)
        deidentified_patient = self._deidentify_patient(days_to_shift)

        my_content_type = ContentType.objects.get_for_model(self.__class__)
        date = self.date - datetime.timedelta(days=days_to_shift)
        immunizations = self._deidentified_immunizations(
            deidentified_patient, days_to_shift)

        data = {
            'event': {
                'type': str(my_content_type),
                'category': self.category,
                'date': str(date),
                'matching_rule_explain': self.matching_rule_explain
            },
            'patient': {
                'first_name': deidentified_patient.first_name,
                'last_name': deidentified_patient.last_name,
                'date_of_birth': str(deidentified_patient.date_of_birth),
                'identifier': deidentified_patient.natural_key
            },
            'provider': {
                'first_name': deidentified_patient.pcp.first_name,
                'last_name': deidentified_patient.pcp.last_name,
                'identifier': deidentified_patient.pcp.natural_key
            },
            'immunizations': [{
                'date': str(i.date)
            } for i in immunizations]
        }

        return self.complete_deidentification(data,
                                              days_to_shift=days_to_shift)

    def render_json_fixture(self):
        import json

        return json.dumps(self.deidentified())

    def render_hl7_message(self):
        from .hl7_report import AdverseReactionReport

        return AdverseReactionReport(self).render()

    def save_hl7_message_file(self, folder=None):
        # TODO maybe gen the notification message here.
        folder = folder or HL7_MESSAGES_DIR
        outfile = open(os.path.join(folder, 'report.%07d.hl7' % self.id), 'w')
        outfile.write(self.render_hl7_message())
        outfile.close()

    class Meta:
        permissions = [('view_phi', 'Can view protected health information'), ]
        ordering = ['id']
        # from hef unique_together = [('content_type', 'object_id')]


# TODO clean up by moving all methods to adverse event and rename the ones that share the name
# later consolidate them 

class EncounterEvent(AdverseEvent):
    @staticmethod
    def write_diagnostics_clustering_report(**kw):
        from heuristics import MAX_TIME_WINDOW_POST_EVENT
        begin_date = kw.pop('begin_date', None) or EPOCH
        end_date = kw.pop('end_date', None) or datetime.datetime.today()

        root_folder = kw.pop('folder', CLUSTERING_REPORTS_DIR)
        folder = make_date_folders(begin_date, end_date, root=root_folder)
        gap = kw.pop('max_interval', MAX_TIME_WINDOW_POST_EVENT)

        dx_code_events = EncounterEvent.objects.filter(date__gte=begin_date,
                                                       date__lte=end_date, gap__lte=MAX_TIME_WINDOW_POST_EVENT)

        within_interval = [e for e in dx_code_events
                           if (
                                   e.date - max(
                               [i.date for i in Case.objects.get(adverse_events=e).immunizations.all()])).days <= gap]

        log.info('Writing report for %d dx_code events between %s and %s' % (
            len(within_interval), begin_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')))

        make_clustering_event_report_file(os.path.join(folder, 'dx_code_events.txt'), within_interval)

    def _deidentified_encounter(self, days_to_shift):
        codes = [{'code': x.combotypecode} for x in self.content_object.dx_codes.all()]
        date = self.content_object.date - datetime.timedelta(days=days_to_shift)
        return {
            'date': str(date),
            'temperature': self.content_object.temperature,
            'dx_codes': codes
        }

    def complete_deidentification(self, data, **kw):
        days_to_shift = kw.pop('days_to_shift', None)
        if not days_to_shift:
            raise ValueError('Must indicate days_to_shift')

        data['encounter'] = self._deidentified_encounter(days_to_shift)

        return data

    def __str__(self):
        return "Encounter Event %s: Patient %s, %s on %s" % (
            self.id, self.patient.full_name,
            self.matching_rule_explain, self.date)


class PrescriptionEvent(AdverseEvent):
    @staticmethod
    def write_clustering_report(**kw):
        from heuristics import MAX_TIME_WINDOW_POST_RX
        begin_date = kw.pop('begin_date', None) or EPOCH
        end_date = kw.pop('end_date', None) or datetime.datetime.today()

        root_folder = kw.pop('folder', CLUSTERING_REPORTS_DIR)
        folder = make_date_folders(begin_date, end_date, root=root_folder)
        gap = kw.pop('max_interval', MAX_TIME_WINDOW_POST_RX)

        rx_events = PrescriptionEvent.objects.filter(date__gte=begin_date, date__lte=end_date)
        within_interval = [e for e in rx_events
                           if (
                                   e.date - max(
                               [i.date for i in Case.objects.get(adverse_events=e).immunizations.all()])).days <= gap]

        log.info('Writing report for %d Prescription events between %s and %s' % (
            len(within_interval), begin_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')))

        make_clustering_event_report_file(os.path.join(folder, 'rx_events.txt'), within_interval)

    def _deidentified_rx(self, days_to_shift):
        date = self.content_object.date - datetime.timedelta(days=days_to_shift)
        return {
            'med': self.content_object.name,
            'date': str(date),
            'dose': self.content_object.dose,
            'quantity': self.content_object.quantity
        }

    def complete_deidentification(self, data, **kw):
        days_to_shift = kw.pop('days_to_shift', None)
        if not days_to_shift:
            raise ValueError('Must indicate days_to_shift')

        data['prescription'] = self._deidentified_rx(days_to_shift)
        return data

    def __str__(self):
        return 'Prescription Event %s: Patient %s, %s on %s' % (
            self.id, self.content_object.patient.full_name,
            self.matching_rule_explain, self.date)


class ProblemEvent(AdverseEvent):
    @staticmethod
    def write_clustering_report(**kw):
        from heuristics import MAX_TIME_WINDOW_POST_RX
        begin_date = kw.pop('begin_date', None) or EPOCH
        end_date = kw.pop('end_date', None) or datetime.datetime.today()

        root_folder = kw.pop('folder', CLUSTERING_REPORTS_DIR)
        folder = make_date_folders(begin_date, end_date, root=root_folder)
        gap = kw.pop('max_interval', MAX_TIME_WINDOW_POST_RX)

        prob_events = ProblemEvent.objects.filter(date__gte=begin_date, date__lte=end_date)
        within_interval = [e for e in prob_events
                           if (
                                   e.date - max(
                               [i.date for i in Case.objects.get(adverse_events=e).immunizations.all()])).days <= gap]

        log.info('Writing report for %d Problem events between %s and %s' % (
            len(within_interval), begin_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')))

        make_clustering_event_report_file(os.path.join(folder, 'prob_events.txt'), within_interval)

    def _deidentified_prob(self, days_to_shift):
        code = self.content_object.dx_code
        date = self.content_object.date - datetime.timedelta(days=days_to_shift)
        return {
            'date': str(date),
            'dx_code': code
        }

    def complete_deidentification(self, data, **kw):
        days_to_shift = kw.pop('days_to_shift', None)
        if not days_to_shift:
            raise ValueError('Must indicate days_to_shift')

        data['problem'] = self._deidentified_prob(days_to_shift)
        return data

    def __str__(self):
        return 'Problem Event %s: Patient %s, %s on %s' % (
            self.id, self.content_object.patient.full_name,
            self.matching_rule_explain, self.date)


class HospProblemEvent(AdverseEvent):
    @staticmethod
    def write_clustering_report(**kw):
        from heuristics import MAX_TIME_WINDOW_POST_RX
        begin_date = kw.pop('begin_date', None) or EPOCH
        end_date = kw.pop('end_date', None) or datetime.datetime.today()

        root_folder = kw.pop('folder', CLUSTERING_REPORTS_DIR)
        folder = make_date_folders(begin_date, end_date, root=root_folder)
        gap = kw.pop('max_interval', MAX_TIME_WINDOW_POST_RX)

        hprob_events = HospProblemEvent.objects.filter(date__gte=begin_date, date__lte=end_date)
        within_interval = [e for e in hprob_events
                           if (
                                   e.date - max(
                               [i.date for i in Case.objects.get(adverse_events=e).immunizations.all()])).days <= gap]

        log.info('Writing report for %d Hospital Problem events between %s and %s' % (
            len(within_interval), begin_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')))

        make_clustering_event_report_file(os.path.join(folder, 'hprob_events.txt'), within_interval)

    def _deidentified_hprob(self, days_to_shift):
        code = self.content_object.dx_code
        date = self.content_object.date - datetime.timedelta(days=days_to_shift)
        return {
            'date': str(date),
            'dx_code': code
        }

    def complete_deidentification(self, data, **kw):
        days_to_shift = kw.pop('days_to_shift', None)
        if not days_to_shift:
            raise ValueError('Must indicate days_to_shift')

        data['hproblem'] = self._deidentified_hprob(days_to_shift)
        return data

    def __str__(self):
        return 'Hospital Problem Event %s: Patient %s, %s on %s' % (
            self.id, self.content_object.patient.full_name,
            self.matching_rule_explain, self.date)


class AllergyEvent(AdverseEvent):
    @staticmethod
    def write_clustering_report(**kw):
        from heuristics import MAX_TIME_WINDOW_POST_EVENT
        begin_date = kw.pop('begin_date', None) or EPOCH
        end_date = kw.pop('end_date', None) or datetime.datetime.today()

        root_folder = kw.pop('folder', CLUSTERING_REPORTS_DIR)
        folder = make_date_folders(begin_date, end_date, root=root_folder)
        gap = kw.pop('max_interval', MAX_TIME_WINDOW_POST_EVENT)

        allergy_events = AllergyEvent.objects.filter(date__gte=begin_date, date__lte=end_date)
        within_interval = [e for e in allergy_events
                           if (
                                   e.date - max(
                               [i.date for i in Case.objects.get(adverse_events=e).immunizations.all()])).days <= gap]

        log.info('Writing report for %d Allergy events between %s and %s' % (
            len(within_interval), begin_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')))

        make_clustering_event_report_file(os.path.join(folder, 'allergy_events.txt'), within_interval)

    def _deidentified_allergy(self, days_to_shift):
        date = self.content_object.date - datetime.timedelta(days=days_to_shift)
        return {
            'med': self.content_object.name,
            'date': str(date)
        }

    def complete_deidentification(self, data, **kw):
        days_to_shift = kw.pop('days_to_shift', None)
        if not days_to_shift:
            raise ValueError('Must indicate days_to_shift')

        data['allergy'] = self._deidentified_allergy(days_to_shift)
        return data

    def __str__(self):
        return 'Allergy Event %s: Patient %s, %s on %s' % (
            self.id, self.patient.full_name,
            self.matching_rule_explain, self.date)


class LabResultEvent(AdverseEvent):
    @staticmethod
    def write_clustering_report(**kw):
        from heuristics import MAX_TIME_WINDOW_POST_LX
        begin_date = kw.pop('begin_date', None) or EPOCH
        end_date = kw.pop('end_date', None) or datetime.datetime.today()

        root_folder = kw.pop('folder', CLUSTERING_REPORTS_DIR)
        folder = make_date_folders(begin_date, end_date, root=root_folder)
        gap = kw.pop('max_interval', MAX_TIME_WINDOW_POST_LX)

        lx_events = LabResultEvent.objects.filter(date__gte=begin_date, date__lte=end_date)
        within_interval = [e for e in lx_events
                           if (
                                   e.date - max(
                               [i.date for i in Case.objects.get(adverse_events=e).immunizations.all()])).days <= gap]

        log.info('Writing report for %d Lab Result events between %s and %s' % (
            len(within_interval), begin_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')))

        make_clustering_event_report_file(os.path.join(folder, 'lx_events.txt'), within_interval)

    def _deidentified_lx(self, days_to_shift):
        date = self.content_object.date - datetime.timedelta(days=days_to_shift)
        return {
            'loinc': self.content_object.loinc.loinc_num,
            'date': str(date),
            'result_float': self.content_object.result_float,
            'result_string': self.content_object.result_string,
            'unit': self.content_object.ref_unit
        }

    def complete_deidentification(self, data, **kw):
        days_to_shift = kw.pop('days_to_shift', None)
        if not days_to_shift:
            raise ValueError('Must indicate days_to_shift')

        data['lab_result'] = self._deidentified_lx(days_to_shift)
        return data

    def __str__(self):
        if self.content_object:
            return 'LabResult Event %s: Patient %s, %s on %s' % (
                self.id, self.patient.full_name,
                self.matching_rule_explain, self.date)
        else:
            return 'LabResult Event %s:  %s on %s' % (
                self.id, self.matching_rule_explain, self.date)


class Sender(models.Model):
    provider = models.ForeignKey(Provider, verbose_name='Physician', blank=True, null=True, on_delete=models.CASCADE)
    date_added = models.DateField(blank=True, null=True, db_index=True)

    def _get_name(self):
        return '%s, %s ' % (self.provider.last_name, self.provider.first_name)

    name = property(_get_name)

    def __str__(self):
        return '%s %s' % (self.provider_id, self.name)


class Case(models.Model):
    date = models.DateTimeField(db_index=True)

    adverse_events = models.ManyToManyField(AdverseEvent, db_index=True)
    immunizations = models.ManyToManyField(Immunization, related_name='all_immunizations', db_index=True)
    patient = models.ForeignKey(Patient, related_name='vaers_cases', blank=False, db_index=True, on_delete=models.CASCADE)
    prior_immunizations = models.ManyToManyField(Immunization, related_name='prior_immunizations', )
    last_update = models.DateTimeField(auto_now=True, db_index=True)

    @staticmethod
    def immunization_by_id(id):
        try:
            klass = ContentType.objects.get_for_model(Immunization)
            return klass.model_class().objects.get(id=id)
        except:
            return None

    @staticmethod
    def paginate(page=1, results_per_page=100):
        # Set limits for query
        floor = max(page - 1, 0) * results_per_page
        ceiling = floor + results_per_page

        # We are only getting the fake ones. There is no possible case
        # in our application where we need paginated results for real
        # cases.
        return Case.fake()[floor:ceiling]

    @staticmethod
    def fake():
        return Case.objects.filter(immunizations__name='FAKE')

    def highest_event_category(self):

        highest_adverse_event = []
        prior_name = None
        prior_priority = None
        # the first encounter will already be added as priority 1
        for event in self.adverse_events.order_by('name', 'priority', 'date'):
            if prior_name == event.name and event.priority == prior_priority and event.content_type.name == ContentType.objects.get_for_model(
                    Encounter):
                prior_name = event.name
                prior_priority = event.priority
                continue
            else:
                prior_name = event.name
                prior_priority = event.priority
                highest_adverse_event.append(event)

        return highest_adverse_event

    def __str__(self):
        return 'AE Case %s: Patient %s, on %s' % (
            self.id, self.patient.full_name, self.date)


class Questionnaire(models.Model):
    event_comment = models.TextField("Please provide details on the likelihood and severity of this possible event",
                                     blank=True, null=True)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

    message_ishelpful = models.NullBooleanField('Was this message helpful', null=True, blank=False, default=None)
    satisfaction_num_msg = models.CharField('Has the number of messages recently been', max_length=10,
                                            choices=CASE_TYPE_CHOICES, null=True, blank=False, db_index=True,
                                            default=None)
    assess_reporting = models.TextField(
        "Please provide comments so that we can refine our adverse event detection algorithms", blank=True, null=True)
    case = models.ForeignKey(Case, on_delete=models.CASCADE)
    patient_recovered = models.CharField('Has the patient recovered from the adverse event(s)?', max_length=1,
                                         blank=True, null=True, default=None)
    result_visit = models.BooleanField("Doctor or other healthcare professional office/clinic visit", default=False)
    result_emergency = models.BooleanField("Emergency room/department or urgent care", default=False)
    result_hosp = models.BooleanField("Hospitalization", default=False)
    result_hosp_days = models.IntegerField("Number of days (if known)", blank=True, null=True)
    result_hosp_name = models.CharField("Hospital Name", max_length=100, blank=True, null=True)
    result_hosp_city = models.CharField("City", max_length=100, blank=True, null=True)
    result_hosp_state = models.CharField("State", max_length=50, blank=True, null=True)
    result_prolong = models.BooleanField(
        "Prolongation of existing hospitalization (vaccine received during existing hospitalization)", default=False)
    result_threat = models.BooleanField("Life threatening illness (immediate risk of death from the event)",
                                        default=False)
    result_disability = models.BooleanField("Disability or permanent damage", default=False)
    result_death = models.BooleanField("Patient Died", default=False)
    result_death_date = models.DateField('Date of death (mm/dd/yyyy)', blank=True, null=True)
    result_congenital = models.BooleanField("Congenital anomaly or birth defect", default=False)
    result_unknown = models.BooleanField("Unknown", default=False)
    result_none = models.BooleanField("None of the above", default=False)
    # HL7 ‘transcription interface’ email for logging/debugging
    inbox_message = models.TextField()
    digest = models.CharField(max_length=200, null=True)
    state = models.SlugField('Possible Adverse Event?', max_length=2, choices=WORKFLOW_STATES, default='AR')

    def create_digest(self):

        if self.digest:
            return
        clear_msg = '%s%s%s' % (self.id, self.case.date, int(time.time()))
        self.digest = hashlib.sha224(clear_msg).hexdigest()
        self.save()

    @staticmethod
    def by_digest(key):
        try:
            klass = Questionnaire.objects.get(digest=key)
            return klass
        except:
            return None

    def clean(self):
        if any([self.result_hosp_city, self.result_hosp_state, self.result_hosp_days, self.result_hosp_name]):
            print("set true")
            self.result_hosp = True
        if self.result_death_date is not None:
            self.result_death = True


class Report_Sent(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    case = models.ForeignKey(Case, on_delete=models.CASCADE)
    #raw hl7 vaers report as sent
    report = models.TextField()
    report_type = models.CharField(max_length=20, blank=False, db_index=True)


class ExcludedDx_Code(models.Model):
    '''
    Codes to be excluded by vaers diagnosis heuristics
    '''
    code = models.CharField(max_length=20, blank=False, unique=True, db_index=True)
    type = models.CharField('Code type', max_length=10)
    description = models.CharField(max_length=255, blank=False)

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = 'Excluded dx code'


class Rule(models.Model):
    name = models.CharField(max_length=100)
    category = models.CharField(max_length=60, choices=ADVERSE_EVENT_CATEGORIES)

    class Meta:
        abstract = True
        ordering = ('name',)

    def __str__(self):
        return "{}".format(self.name)

class DiagnosticsEventRule(Rule):
    ignore_period = models.PositiveIntegerField(null=True)
    heuristic_defining_codes = models.ManyToManyField(Dx_code, related_name='defining_dx_code_set')
    heuristic_discarding_codes = models.ManyToManyField(Dx_code, related_name='discarding_dx_code_set')
    risk_period = models.IntegerField(blank=False, null=False,
                                      help_text='MAX Risk period in days following vaccination')
    risk_period_start = models.IntegerField(default=1,
                                            help_text='Risk period start day following vaccination')
    applies_to_all = models.BooleanField(blank=False, default=True)
    dxrule_vxs = models.ManyToManyField(Vaccine)



    @staticmethod
    def random():
        return DiagnosticsEventRule.objects.order_by('?').first()

    def __str__(self):
        return str(self.name)

class LabRule(Rule):
    pediatric = models.BooleanField()
    trigger = models.CharField(max_length=10)
    unit = models.CharField(max_length=10)
    comparator = models.CharField(max_length=1, choices=(('<', '<'), ('>', '>')))
    baseline = models.CharField(max_length=10)
    risk_period = models.IntegerField(blank=True, null=True)

    class Meta:
        unique_together = ('name', 'pediatric', 'trigger')

    def __str__(self):
        return "{} (pediatric-{})".format(self.name, self.pediatric)

    def get_native_codes(self):
        from ESP.conf.models import LabTestMap
        return LabTestMap.objects.filter(test_name__icontains=self.name).values('native_code')


class PrescriptionRule(Rule):
    risk_period = models.IntegerField(blank=False, null=False,
                                      help_text='MAX Risk period in days following vaccination')
    exclude_due_to_history = models.BooleanField(default=False)


class AllergyRule(Rule):
    pass


class AllergyKeyword(models.Model):
    allergy = models.ForeignKey(AllergyRule, related_name="keywords", on_delete=models.CASCADE)
    keyword = models.CharField(max_length=20)

    def __str__(self):
        return "{} for allergy {}".format(self.keyword, self.allergy.name)


class State(models.Model):
    name = models.CharField(max_length=50, primary_key=True)
    abbreviation = models.CharField(max_length=2)

    def __str__(self):
        return self.name
