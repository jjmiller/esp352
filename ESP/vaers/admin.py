from django.contrib import admin

from ESP.vaers.models import DiagnosticsEventRule, Sender
from ESP.vaers.models import EncounterEvent, LabResultEvent, PrescriptionEvent, AllergyEvent
from ESP.vaers.models import PrescriptionRule, AllergyRule, AllergyKeyword, LabRule


class EncounterEventAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    fields = ['matching_rule_explain', 'date', 'category', ]
    list_display = ['matching_rule_explain', 'object_id', 'date', 'category', 'digest']
    list_filter = ['date', 'category']


class LabResultAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    fields = ['matching_rule_explain', 'date', 'category', ]
    list_display = ['matching_rule_explain', 'object_id', 'date', 'category', 'digest']
    list_filter = ['date', 'category']


class PrescriptionAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    fields = ['matching_rule_explain', 'date', 'category', ]
    list_display = ['matching_rule_explain', 'object_id', 'date', 'category', 'digest']
    list_filter = ['date', 'category']


class AllergyEventAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    fields = ['matching_rule_explain', 'date', 'category', ]
    list_display = ['matching_rule_explain', 'object_id', 'date', 'category', 'digest']
    list_filter = ['date', 'category']


class SenderAdmin(admin.ModelAdmin):
    raw_id_fields = ['provider']
    list_display = ['pk', 'name']


class AllergyKeywordInline(admin.TabularInline):
    model = AllergyKeyword


class AllergyRuleAdmin(admin.ModelAdmin):
    inlines = [
        AllergyKeywordInline,
    ]


admin.site.register(EncounterEvent, EncounterEventAdmin)
admin.site.register(LabResultEvent, LabResultAdmin)
admin.site.register(PrescriptionEvent, PrescriptionAdmin)
admin.site.register(AllergyEvent, AllergyEventAdmin)
admin.site.register(DiagnosticsEventRule)
admin.site.register(Sender, SenderAdmin)
admin.site.register(PrescriptionRule)
admin.site.register(AllergyRule, AllergyRuleAdmin)
admin.site.register(LabRule)
