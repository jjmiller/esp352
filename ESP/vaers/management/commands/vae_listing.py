#!/usr/bin/env python
'''
                                  ESP Health
                                VAERS Listing
                              Command Line Runner


@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth Informatics Inc. http://www.commoninf.com
@copyright: (c) 2012 Commonwealth Informatics Inc.
@license: LGPL
'''

import csv, pdb
import re
from datetime import date, datetime

from dateutil.relativedelta import relativedelta
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand
from django.db.models import Min

from ESP.emr.models import Provider
from ESP.settings import VAERS_LINELIST_PATH
from ESP.static.models import Dx_code
from ESP.utils import log
from ESP.vaers.models import Case


class Command(BaseCommand):
    help = 'Generates .csv VAERS Listing table for download'
    OBSC = 'PHI'

    def add_arguments(self, parser):
        parser.add_argument('-p', '--phi', dest='phi' , action='store_true', default=False,
            help='Sets phi level to true (false by default)')
        super(Command, self).add_arguments(parser)

    def handle(self, *args, **options):
        self.phi = options['phi']
        self.vaers_csv()

    def vaers_csv(self):
        '''
        Creates a linelist CSV file of all vaers data
        the parameter no_phi is a boolean value for keeping or OBSCuring phi
        if no_phi is true, phi is OBSCured
        '''
        fpath = VAERS_LINELIST_PATH
        if not self.phi:
            log.info('creating VAERS listing with no PHI.')
            vfile = open(fpath + "vaers_linelist_nophi.csv", 'w')
        else:
            log.info('creating VAERS listing with PHI.')
            vfile = open(fpath + "vaers_linelist_phi.csv", 'w')
            # rebuild each time
        vfile.truncate()
        # this has to be parameterized.
        writer = csv.writer(vfile, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(self._make_header())

        for case in Case.objects.all():
            writer.writerow(self._make_row(case))

        vfile.close()
        log.info('VAERS Listing written.')

    def _pad_to_size(self, iterable, size):
        iterable += [None] * size
        return iterable[:size]

    def _make_header(self):
        header = ['Case_id',
                  'First_message_date',
                  'MRN',
                  'Name',
                  'DOB',
                  'Age',
                  'Gender',
                  'Practice',
                  'Vaccine provider',
                  'Vaccination_date',
                  ]

        header.extend(self._make_numbered_list(['Vaccine_name_', 'Vaccine_Lot_no_'], 11))
        header.append('Immunization_count')
        header.extend(self._make_numbered_list(['Event_name', 'Reporting_clinician_name', 'Action_category'], 22))
        header.append('AE_Count')
        header.extend(
            self._make_numbered_list(['Lab_result_name', 'Lab_date', 'Lab_result_text', 'Lab_result_value'], 5))
        header.append('Lab_count')
        header.extend(self._make_numbered_list(['DX_CODE_result_date', 'DX_CODE_result_text'], 29))
        header.append('DX_CODE_count')
        header.extend(
            self._make_numbered_list(['Prescription_date', 'Prescription_name', 'Prescription_dose_freq_dur'], 13))
        header.append('Prescription_count')
        header.extend(
            self._make_numbered_list(['Physician_notified', 'Workflow_Status', 'Clinical_comment', 'Message_ishelpful'],
                                     16))
        header.append('Message_count')

        return header

    def _get_phi_date(self, case, pdate):
        if self.phi:
            return pdate
        else:
            if type(pdate)==date:
                interval = pdate - (case.immunizations.aggregate(Min('date'))['date__min'])
            else:
                interval = pdate.date() - (case.immunizations.aggregate(Min('date'))['date__min'])
            return interval.days

    def _clean_phi(self, elem):
        obsc = 'PHI'
        if self.phi:
            return elem
        else:
            return obsc

    def _make_numbered_list(self, elements, count):
        row = []
        for i in range(1, count + 1):
            for elem in elements:
                row.append('{}{}'.format(elem, i))
        return row

    def _make_row(self, case):
        immunization_qs = case.immunizations.all()
        patient = case.patient
        mrn = patient.mrn
        if not patient.last_name:
            fullname = None
        elif not patient.first_name:
            fullname = patient.last_name
        else:
            fullname = patient.last_name + ', ' + patient.first_name
        dob = patient.date_of_birth
        if patient.date_of_birth:
            age = patient.get_age(case.date).years 
        else:
            age = None
        provider = Provider.objects.get(id=immunization_qs.first().provider_id)
        pname = provider._get_name()
        vaccdate = immunization_qs.first().date
        if not self.phi:
            if relativedelta(date.today(), dob).years >= 90:
                dob = None
            elif patient.date_of_birth:
                dob = patient.date_of_birth.year
            if age > 90:
                age = 90
        # here is the immunization array
        immuarray = []
        for imm in immunization_qs:
            immuarray.append(imm.name)
            immuarray.append(imm.lot)

        aearray = []
        dxarray = []
        rxarray = []
        lbarray = []
        alarray = []
        vaers_qs = case.adverse_events.distinct()
        # todo: this needs to be ordered by category -- worst first.  Order by does not work, as category order is 2, 3, 1
        for AE in vaers_qs:
            aearray.append(AE.name)
            if not self.phi:
                aearray.append(self.OBSC)
            else:
                aearray.append(AE.content_object.provider)
            aearray.append(AE.category)
            types = ContentType.objects.get_for_id(AE.content_type_id).model
            if types.startswith('encounter'):
                dx_codes = AE.matching_rule_explain.split()
                for dx_code in dx_codes:
                    if re.search(r'[0-9]', dx_code):
                        dxarray.append(self._get_phi_date(case, AE.encounterevent.date))
                        dxarray.append(Dx_code.objects.filter(combotypecode=dx_code).first().name)
                        dxarray.append(dx_code)
            elif types.startswith('prescription'):
                rx = AE.prescriptionevent.content_object
                rxarray.append(self._get_phi_date(case, rx.date))
                rxarray.append(rx.name)
                rxarray.append(rx.directions)
            elif types.startswith('labresult'):
                labs = AE.labresultevent.content_object
                lbarray.append(labs.native_name)
                lbarray.append(self._get_phi_date(case, labs.date))
                lbarray.append(labs.result_string)
                lbarray.append(labs.result_float)
            elif types.startswith('allergy'):
                allergies = AE.allergyevent.allergy.allergyevent_set.all()
                for allrgy in allergies:
                    alarray.append(self._get_phi_date(case, allrgy.allergy.date))
                    alarray.append(allrgy.allergy.allergen)
                    alarray.append(allrgy.allergy.description)

        qarray = []
        ques_qs = case.questionnaire_set.all().order_by('created_on')
        firstqdt = ques_qs.first().created_on
        for ques in ques_qs:
            if not self.phi:
                qarray.append(self.OBSC)
            else:
                qarray.append(ques.provider)
            qarray.append(ques.state)
            if not self.phi:
                qarray.append(self.OBSC)
            else:
                qarray.append(ques.event_comment)
            qarray.append(ques.message_ishelpful)

        nimmu = len(immuarray) / 2
        nae = len(aearray) / 3
        nlab = len(lbarray) / 4
        ndx = len(dxarray) / 3
        nrx = len(rxarray) / 3
        nques = len(qarray) / 5

        row = [case.id,
               firstqdt.date(),
               self._clean_phi(mrn),
               self._clean_phi(fullname),
               dob,
               age,
               patient.gender,
               provider.dept,
               self._clean_phi(pname),
               self._get_phi_date(case, vaccdate)
               ]
        row.extend(self._pad_to_size(immuarray, 22))
        row.append(nimmu)
        row.extend(self._pad_to_size(aearray, 66))
        row.append(nae)
        row.extend(self._pad_to_size(lbarray, 20))
        row.append(nlab)
        row.extend(self._pad_to_size(dxarray, 87))
        row.append(ndx)
        row.extend(self._pad_to_size(rxarray, 39))
        row.append(nrx)
        row.extend(self._pad_to_size(qarray, 80))
        row.append(nques)

        return row
