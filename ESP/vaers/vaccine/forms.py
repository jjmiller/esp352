from django import forms

from ESP.conf.models import Vaccine, ImmunizationManufacturer

try:
    STANDARD_VACCINES = ((x.code, str(x)) for x in Vaccine.acceptable_mapping_values())
except:
    STANDARD_VACCINES = []
try:
    STANDARD_MANUFACTURERS = ((x.code, str(x)) for x in ImmunizationManufacturer.objects.all())
except:
    STANDARD_MANUFACTURERS = []

class StandardVaccinesForm(forms.Form):
    vaccine = forms.ChoiceField(choices=STANDARD_VACCINES)

class StandardManufacturersForm(forms.Form):
    manufacturer = forms.ChoiceField(choices=STANDARD_MANUFACTURERS)

