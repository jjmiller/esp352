from django.conf.urls import url
from . import views

app_name='vaccine'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^manufacturers$', views.manufacturers, name='manufacturers'),
    url(r'^vaccine/(?P<id>\d*)$', views.vaccine_detail, name='vaccine_detail'),
    url(r'^manufacturer/(?P<id>\d*)$', views.manufacturer_detail, name='manufacturer_detail')
    
]
