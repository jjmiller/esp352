#-*- coding:utf-8 -*-

import io
import datetime
import ftplib

from django.contrib.contenttypes.models import ContentType

from ESP.emr.models import Provider, Patient
from ESP.settings import SITE_NAME, SYSTEM_STATUS, PHINMS_SERVER, PHINMS_USER, PHINMS_PASSWORD, PHINMS_PATH, VAERS_AUTOSENDER
from ESP.settings import CASE_REPORT_OUTPUT_FOLDER
from ESP.static.models import Vaccine, ImmunizationManufacturer, Dx_code
from ESP.utils import utils
from ESP.utils.hl7_builder.core import SegmentTree
from ESP.utils.hl7_builder.nodes import VaccineDetail, PriorVaccinationDetail
from ESP.utils.hl7_builder.segments import MSH, PID, ORC, OBR, OBX, NK1
from ESP.utils.utils import log
from ESP.utils.utils import mt
from ESP.vaers.utils import race_conversion, ethnicity_conversion, state_abbreviation_lookup
from ESP.vaers.models import Case, Questionnaire, Report_Sent

UNKNOWN_VACCINE = Vaccine.objects.get(short_name='unknown')
UNKNOWN_MANUFACTURER = ImmunizationManufacturer.objects.get(code='UNK')

now = datetime.datetime.now()

class AdverseReactionReport(object):
    #initialize with a questionnaire record
    def __init__(self, questionnaire):
        self.ques = questionnaire
        self.case = Case.objects.get(id=self.ques.case_id)
        self.immunizations = self.case.immunizations.all()
        self.patient = Patient.objects.get(id=self.case.patient_id)

    def make_MSH(self):
        msh = MSH()

        # Most of these values should be parameters from the
        # function. For now they are defined here simply because we
        # have no other recipient except for Atrius Health.
        msh.receiving_application = 'VAERS HL7 Processor'
        msh.receiving_facility = 'VAERS PROCESSOR'
        msh.time = now.strftime("%Y%m%d%H%M%S")
        msh.processing_id = SYSTEM_STATUS
        msh.accept_ack_type = 'NE'
        msh.application_ack_type = 'AL'
        msh.message_type = ['ORU', 'R01']
        msh.message_control_id = self.ques.id
        msh.sending_facility = SITE_NAME

        return msh

    def make_PID(self):
        #TODO: deal with not available potential.  See NK1s
        patient = self.case.patient
        pid = PID()
        pid.patient_id_list = [patient.mrn,'','','','MR']
        pid.patient_name = [mt(patient.last_name), mt(patient.first_name),'','','','','L']
        pid.date_of_birth = utils.str_from_date(patient.date_of_birth)
        pid.sex = mt(patient.gender)
        pid.patient_address = [mt(patient.address1), mt(patient.address2), 
                               mt(patient.city), state_abbreviation_lookup(mt(patient.state)), mt(patient.zip),'','C']
        pid.ethnic_group = ethnicity_conversion(mt(patient.ethnicity))
        pid.race = race_conversion(mt(patient.race))
        if patient.date_of_death is not None:
            pid.patient_death_dt = mt(patient.date_of_death.strftime("%Y%m%d"))
        else:
            pid.patient_death_dt = mt(self.ques.result_death_date.strftime("%Y%m%d"))

        pid.home_phone = [mt(patient.phone_number),'PRN']
        # TODO: Add patient email address as part of the PID-13 repeatable field, should this data be captured
        # if mt(Patient_Addr.email) == '':
        #     pid.home_phone = [mt(patient.phone_number),'PRN']
        # else:
        #     pid.home_phone = '~'.join('^'.join([mt(patient.phone_number),'PRN']),'^'.join([mt(Patient_Addr.email),'NET']))
        return pid
    
    def make_NK1s(self):
        
        vax_provider = Provider.objects.get(id=self.immunizations.first().provider_id)
        comp_provider = self.ques.provider
        if self.ques.state=='AR':
            comp_provider = Provider.objects.get(natural_key=VAERS_AUTOSENDER)
            #VAERS AUTOSENDER must be identified for a site, and a Provider record entered for this individual
            #This is the sender of record in cases where case category is 2_serious and should be autosent.
        nk1_1 = NK1()
        if (mt(vax_provider.last_name)=='') or (vax_provider.id==1):
            nk1_1.name='Not available'
        else:
            nk1_1.name=[mt(vax_provider.last_name),mt(vax_provider.first_name),'','',
                    mt(vax_provider.title),'','L']
        nk1_1.relationship = ['POC', 'Best Healthcare Professional to Contact', 'HL70063']
        nk1_1.business_phone_number = ['-'.join([mt(vax_provider.area_code),mt(vax_provider.telephone),mt(vax_provider.tel_ext)]),'WPN']
        nk1_2 = NK1()
        if (mt(comp_provider.last_name)=='') or (comp_provider.id==1):
            nk1_2.name='Not available'
        else:
            nk1_2.name=[mt(comp_provider.last_name),mt(comp_provider.first_name),'','',
                    mt(comp_provider.title),'','L']
        if nk1_1.name==nk1_2.name:
            nk1_2.relationship = ['FVP', 'Form completed by (Name)-Vaccine provider','HL70063']
        else:
            nk1_2.relationship = ['FOT', 'Form completed by (Name)-Other','HL70063']
        nk1_2.address = [mt(comp_provider.dept_address_1), mt(comp_provider.dept_address_2), 
                         mt(comp_provider.dept_city), state_abbreviation_lookup(mt(comp_provider.dept_state)),
                         mt(comp_provider.dept_zip),'','B']
        if nk1_2.address==['', '', '', '', '', '', 'B']:
            nk1_2.address='Not available'
        if mt(comp_provider.telephone)=='':
            nk1_2.phone_number='Not available'
        else:
            nk1_2.phone_number = [mt(comp_provider.telephone),'WPN']
        nk1=[nk1_1, nk1_2]
        return SegmentTree(self.make_PID(), nk1)
    
    def make_ORC(self):
        vax_provider = Provider.objects.get(id=self.immunizations.first().provider_id)
        orc = ORC()
        orc.control='RE'
        if mt(vax_provider.last_name)=='' or vax_provider.id==1:
            orc.ordering_provider=['Not available','','','','','','','']
            orc.ordering_facility=''
            orc.ordering_facility_address = ['','','','','','','']
            orc.ordering_facility_phone = ['','']
            orc.ordering_provider_address = ['','','','','','','']
        else:
            orc.ordering_provider=[vax_provider.natural_key, mt(vax_provider.last_name),mt(vax_provider.first_name),'','',
                    mt(vax_provider.title),'','L']
            orc.ordering_facility_name=mt(vax_provider.dept)
            orc.ordering_facility_address = [mt(vax_provider.dept_address_1), mt(vax_provider.dept_address_2), 
                         mt(vax_provider.dept_city), state_abbreviation_lookup(mt(vax_provider.dept_state)),
                         mt(vax_provider.dept_zip),'','B']
            orc.ordering_facility_phone = [mt(vax_provider.telephone),'WPN']
            orc.ordering_provider_address = [mt(vax_provider.dept_address_1), mt(vax_provider.dept_address_2), 
                         mt(vax_provider.dept_city), state_abbreviation_lookup(mt(vax_provider.dept_state)),
                         mt(vax_provider.dept_zip),'','B']
        return orc


    def event_summary(self):
        obr_fda_report = OBR()
        obr_fda_report.universal_service_id = 'CDC VAERS-1 (FDA) Report'
        obr_fda_report.observation_date = utils.str_from_date(self.case.date)

        observation_results = []
        
        age_years = self.case.patient._get_age_str()
        age_months = self.case.patient._get_age_str('months', False)
        if age_years or age_months:
            int_age_years = int(age_years)
            int_age_months = int(age_months)

            if int_age_months > 11:
                int_age_months -= int_age_years * 12

            obx_patient_age_mon = OBX()
            obx_patient_age_mon.value_type = 'NM'
            obx_patient_age_mon.identifier = ['', '', '', 'MONTH AGE', 'Age at vaccination month', 'L']
            obx_patient_age_mon.value = str(int_age_months)
            obx_patient_age_mon.units = ['mo', 'month', 'ANSI']
            obx_patient_age_mon.observation_result_status = 'F'

            obx_patient_age_yr = OBX()
            obx_patient_age_yr.value_type = 'NM'
            obx_patient_age_yr.identifier = ['', '', '', 'YEAR AGE', 'Age at vaccination year', 'L']
            obx_patient_age_yr.value = age_years
            obx_patient_age_yr.units = ['yr', 'year', 'ANSI']
            obx_patient_age_yr.observation_result_status = 'F'

            observation_results.append(obx_patient_age_yr)
            observation_results.append(obx_patient_age_mon)
        
        obx_form = OBX()
        obx_form.value_type = 'TS'
        obx_form.identifier = ['30947-6', 'Date form completed', 'LN']
        obx_form.value = utils.str_from_date(self.ques.last_updated)
        obx_form.observation_result_status = 'F'
        observation_results.append(obx_form)

        obx_treatment = OBX()
        obx_treatment.value_type = 'FT'
        obx_treatment.identifier = ['30948-4', 'Vaccination adverse events and treatment, if any', 'LN']
        obx_treatment.sub_id = 1
        AEs = self.case.adverse_events.distinct().order_by('date')
        quests = self.case.questionnaire_set.filter(event_comment__isnull=False)
        caseDescription=''
        for AE in AEs:
            if ContentType.objects.get_for_id(AE.content_type_id).model.startswith('encounter'):
                dx_codes = AE.matching_rule_explain.split()
                for dx_code in dx_codes:
                    if dx_code.startswith('icd'):
                        dx_code = dx_code[:-1] if dx_code.endswith(':') else dx_code
                        caseDescription += Dx_code.objects.get(combotypecode=dx_code).name + ', '
                caseDescription = caseDescription[0:-2] + ' on ' + str(AE.encounterevent.date) + ', '  
            elif ContentType.objects.get_for_id(AE.content_type_id).model.startswith('prescription'):
                caseDescription = caseDescription + AE.prescriptionevent.content_object.name + ' on ' + str(AE.prescriptionevent.content_object.date) + ', '
            elif ContentType.objects.get_for_id(AE.content_type_id).model.startswith('labresult'):
                caseDescription = caseDescription + AE.labresultevent.content_object.native_name + ' on ' + str(AE.labresultevent.content_object.date) + ', '
            elif ContentType.objects.get_for_id(AE.content_type_id).model.startswith('allergy'):
                caseDescription = caseDescription + AE.allergyevent.content_object.name + ' allergy on ' + str(AE.allergyevent.content_object.date) + ', '
        for quest in quests:
            caseDescription += 'clinician comment: ' + quest.event_comment + ', '
        caseDescription = caseDescription[0:-2] + '. '
        obx_treatment.value = caseDescription
        obx_treatment.observation_result_status = 'F'
        observation_results.append(obx_treatment)
        
        for AE in AEs:
            if ContentType.objects.get_for_id(AE.content_type_id).model.startswith('encounter'):
                obx_ER = OBX()
                if AE.encounterevent.content_object.encounter_type=='ER':
                    obx_ER.value_type = 'CE'
                    obx_ER.identifier = ['30949-4','Vaccination adverse event outcome','LN']
                    obx_ER.sub_id=1
                    obx_ER.value=['E','Emergency room or emergency department visit','NIP005']
                    obx_ER.observation_result_status='F'
                    observation_results.append(obx_ER)
                elif AE.encounterevent.content_object.encounter_type=='VISIT':
                    obx_office = OBX()
                    obx_office.value_type = 'FT'
                    obx_ER.identifier = ['30949-2','Vaccination adverse event outcome','LN']
                    obx_ER.sub_id = 1
                    obx_ER.value = ['E','Doctor or other healthcare professional office/clinic visit','NIP005']
                    obx_ER.observation_result_status = 'F'
                    observation_results.append(obx_office)
                elif (AE.encounterevent.content_object.hosp_admit_dt and
                        AE.encounterevent.content_object.hosp_dschrg_dt and
                                  AE.encounterevent.content_object.hosp_admit_dt <= AE.encounterevent.content_object.date <= AE.encounterevent.content_object.hosp_dschrg_dt):
                    obx_hosp = OBX()
                    obx_hosp.value_type = 'CE'
                    obx_hosp.identifier = ['30949-2','Vaccination adverse event outcome','LN']
                    obx_ER.sub_id=1
                    obx_ER.value=['H','required hospitalization','NIP005']
                    obx_ER.observation_result_status='F'
                    observation_results.append(obx_hosp)
                    obx_hdur = OBX()
                    obx_hdur.value_type = 'NM'
                    obx_hdur.identifier = ['30950-0','Number of days hospitalized due to vaccination adverse event','LN']
                    obx_hdur.sub_id=1
                    obx_hdur.value=AE.encounterevent.content_object.hosp_dschrg_dt - AE.encounterevent.content_object.date
                    obx_hdur.units = ['d','day','ANSI']
                    obx_hdur.observation_result_status = 'F'
                    observation_results.append(obx_hdur)
                          
        if self.ques.result_hosp_name:
            obx_hosp_name = OBX()
            obx_hosp_name.value_type = 'FT'
            obx_hosp_name.identifier = ['', '', '', 'HOSPNAME', 'Hospital Name', 'L']
            obx_hosp_name.value = self.ques.result_hosp_name
            obx_hosp_name.observation_result_status = 'F'
            observation_results.append(obx_hosp_name)

        if self.ques.result_hosp_city:
            obx_hosp_city = OBX()
            obx_hosp_city.value_type = 'FT'
            obx_hosp_city.identifier = ['', '', '', 'HOSPCITY', 'Hospital City', 'L']
            obx_hosp_city.value = self.ques.result_hosp_city
            obx_hosp_city.observation_result_status = 'F'
            observation_results.append(obx_hosp_city)

        if self.ques.result_hosp_state:
            obx_hosp_state = OBX()
            obx_hosp_state.value_type = 'FT'
            obx_hosp_state.identifier = ['', '', '', 'HOSPSTATE', 'Hospital State', 'L']
            obx_hosp_state.value = state_abbreviation_lookup(self.ques.result_hosp_state)
            obx_hosp_state.observation_result_status = 'F'
            observation_results.append(obx_hosp_state)

        if self.ques.result_prolong:
            obx_prolong = OBX()
            obx_prolong.value_type = 'FT'
            obx_prolong.identifier = ['30949-2','Vaccination adverse event outcome','LN']
            obx_prolong.value = ['P', 'Prolongation of existing hospitalization', 'NIP005']
            obx_prolong.observation_result_status = 'F'
            observation_results.append(obx_prolong)

        if self.ques.result_threat:
            obx_threat = OBX()
            obx_threat.value_type = 'FT'
            obx_threat.identifier = ['30949-2','Vaccination adverse event outcome','LN']
            obx_threat.value = ['L', 'Life threatening illness', 'NIP005']
            obx_threat.observation_result_status = 'F'
            observation_results.append(obx_threat)

        if self.ques.result_disability:
            obx_disability = OBX()
            obx_disability.value_type = 'FT'
            obx_disability.identifier = ['30949-2','Vaccination adverse event outcome','LN']
            obx_disability.value = ['J', 'Resulted in permanent disability', 'NIP005']
            obx_disability.observation_result_status = 'F'
            observation_results.append(obx_disability)

        if self.ques.result_death:
            obx_death = OBX()
            obx_death.value_type = 'FT'
            obx_death.identifier = ['30949-2','Vaccination adverse event outcome','LN']
            obx_death.value = ['D', 'Patient died', 'NIP005']
            obx_death.observation_result_status = 'F'
            observation_results.append(obx_death)

        if self.ques.result_congenital:
            obx_congenital = OBX()
            obx_congenital.value_type = 'FT'
            obx_congenital.identifier = ['30949-2','Vaccination adverse event outcome','LN']
            obx_congenital.value = ['C', 'Congenital anomaly or birth defect', 'NIP005']
            obx_congenital.observation_result_status = 'F'
            observation_results.append(obx_congenital)

        if self.ques.result_none:
            obx_none = OBX()
            obx_none.value_type = 'FT'
            obx_none.identifier = ['30949-2','Vaccination adverse event outcome','LN']
            obx_none.value = ['O', 'None of the above', 'NIP005']
            obx_none.observation_result_status = 'F'
            observation_results.append(obx_none)

        if self.ques.patient_recovered:
            obx_recovered = OBX()
            obx_recovered.value_type = 'FT'
            obx_recovered.identifier = ['30951-8', 'Patient Recovered', 'LN']
            obx_recovered.value = ['Y', 'Yes', 'HL70136']
            obx_recovered.observation_result_status = 'F'
            observation_results.append(obx_recovered)

        obx_vaccination = OBX()
        obx_vaccination.value_type = 'TS'
        obx_vaccination.identifier = ['30952-6', 'Date of vaccination', 'LN']
        obx_vaccination.value = utils.str_from_date(
            max([x.date for x in self.immunizations]))
        obx_vaccination.observation_result_status = 'F'
        observation_results.append(obx_vaccination)

        obx_preg = OBX()
        obx_preg.value_type = 'ST'
        obx_preg.identifier = ['', '', '', 'PREGNANT', 'Pregnant at time of vaccination', 'L']
        obx_preg.value = 'No'
        for AE in AEs:
            if ContentType.objects.get_for_id(AE.content_type_id).model.startswith('encounter'):
                if AE.encounterevent.content_object.pregnant==True:
                    obx_preg.value = 'Yes'
                    break
        obx_preg.observation_result_status = 'F'
        observation_results.append(obx_preg)

        obx_date = OBX()
        obx_date.value_type = 'TS'
        obx_date.identifier = ['30953-4', 'Adverse event onset date and time', 'LN']
        obx_date.value = utils.str_from_date(min([AE.date for AE in AEs]))
        obx_date.observation_result_status = 'F'
        observation_results.append(obx_date)

        obx_labs = OBX()
        obx_labs.value_type = 'FT'
        obx_labs.identifier = ['30954-2','Relevant diagnostic tests/lab data','LN']
        obx_labs.observation_result_status = 'F'
        for AE in AEs:
            if ContentType.objects.get_for_id(AE.content_type_id).model.startswith('labresult'):
                obx_labs.value = AE.matching_rule_explain + ' Observed value: ' + AE.labresultevent.content_object.result_string
                observation_results.append(obx_labs)

        return SegmentTree(obr_fda_report, observation_results)

    def vaccine_list(self):
        obr_vaccine_list = OBR()
        obr_vaccine_list.universal_service_id = ['30955-9','All vaccines given on date listed in #10','LN']

        vaccines = []

        for immunization in self.immunizations:
            vaccine_detail = VaccineDetail(immunization)
            for seg in vaccine_detail.segments:
                vaccines.append(seg)
            
        return SegmentTree(obr_vaccine_list, vaccines)
    
    def prior_vax(self):
        obr_prior_vaccination_list = OBR()
        obr_prior_vaccination_list.universal_service_id = ['30961-7', 'Any other vaccinations within 4 weeks prior to the date listed in #10', 'LN']

        vaccines = []
        for immunization in self.case.prior_immunizations.all():
            vaccine_detail = PriorVaccinationDetail(immunization)
            for seg in vaccine_detail.segments:
                vaccines.append(seg)
        
        vax_at = OBX()
        vax_at.value_type = 'CE'
        vax_at.identifier = ['30962-5','Vaccinated at','LN']
        vax_at.value = ['PVT','Doctor''s office/hospital','NIP008']
        vax_at.observation_result_status = 'F'
        #these are not prior vax data, but VAERS message spec defines these OBX segments as subs 
        # of the prior vax OBR seg.  
        vaccines.append(vax_at)  
        
        oth_meds = []
        oth_med = OBX()
        oth_med.value_type = 'FT'
        oth_med.identifier = ['30964-1','Other medications','LN']
        oth_med.observation_result_status = 'F'
        oth_cnt = 0
        AEs = self.case.adverse_events.distinct().order_by('date')
        for AE in AEs:
            if ContentType.objects.get_for_id(AE.content_type_id).model.startswith('prescription'):
                oth_med.value = AE.prescriptionevent.content_object.name
                oth_meds.append(oth_med)
                oth_cnt += 1
        if oth_cnt==0:
            oth_med.value = 'None'
            oth_meds.append(oth_med)
        #these are not prior vax data, but VAERS message spec defines these OBX segments as subs 
        # of the prior vax OBR seg.  
        vaccines.append(oth_med)

        prev_rept = OBX()
        prev_rept.value_type = 'CE'
        prev_rept.identifier = ['30967-4','Was adverse event reported previously','LN']
        prev_rept.value = ['D','To doctor','NIP009']
        prev_rept.observation_result_status = 'F'
        #these are not prior vax data, but VAERS message spec defines these OBX segments as subs 
        # of the prior vax OBR seg.  
        vaccines.append(prev_rept)
        
                        
        return SegmentTree(obr_prior_vaccination_list, vaccines)
    
    def prior_ae(self, case):
        
        obr = OBR()
        obr.universal_service_id = ['30968-2','Adverse event following prior vaccination in patient','LN']
       
        p_aes = []
        p_ae = OBX()
        p_ae.value_type = 'CE'
        p_ae.observation_result_status = 'F'
        ae_list = []
        p_ae.identifier = ['30968-2&30971-6','Adverse event','LN']
        AEs = case.adverse_events.distinct().order_by('date')
        for AE in AEs:
            ae_list.append(AE.matching_rule_explain)
        p_ae.value = ae_list 
        p_aes.append(p_ae)
        p_ae.identifier = ['30968-2&30971-6','Onset age','LN']
        p_ae.value = self.patient.age.years
        p_ae.units = ['yr','year','ANSI']
        if p_ae.value < 2:
            p_ae.value = self.patient.age.months
            p_ae.units = ['mo','month','ANSI']
        p_aes.append(p_ae)
        p_ae.units = None
        p_ae.identifier = ['30968-2&30971-6','Vaccine Type','LN']  
        for imm in case.immunizations.all():
            CVXVax = Vaccine.objects.get(id=imm.vaccine.code)
            p_ae.value = [CVXVax.code, CVXVax.short_name, 'CVX']
            p_aes.append(p_ae)           
        return SegmentTree(obr, p_aes)
    
    def repno(self):
        obr = OBR()
        obr.universal_service_id = ['','Only for reports submitted by manufacturer/immunization project']
        obx = OBX()
        obx.value_type = 'ST'
        obx.observation_result_status = 'F'
        obx.identifier = ['30975-7','Mfr./Imm. Proj. report no.','LN']
        obx.value = ['METROHEALTHESP2012'+str(self.ques.id)]
        return SegmentTree(obr, obx)
        

    def render(self):
        observation_reports = [self.event_summary(), self.vaccine_list(), 
                               self.prior_vax()]
        pcases = Case.objects.filter(patient=self.case.patient, date__lt=self.case.date, report_sent__id__isnull=False)
        pcases = pcases.filter(id__in=Report_Sent.objects.filter)
        for pcase in pcases:
            observation_reports.append(self.prior_ae(pcase))
        observation_reports.append(self.repno())
        for idx, report in enumerate(observation_reports):
            report.parent.sequence_id = idx + 1

        patient_header = [self.make_MSH(), self.make_NK1s(), self.make_ORC()]

        all_segments = patient_header + observation_reports
        #hl7file = cStringIO.StringIO()
        hl7file = open(CASE_REPORT_OUTPUT_FOLDER + '/vaers_msg_ID' + str(self.ques.id) + '.hl7',"w+")
        hl7file.write('\r'.join([str(x) for x in all_segments]))
        hl7file.seek(0)
        #if transmit_ftp(hl7file, 'vaers_msg_ID' + str(self.ques.id) + '.hl7'):
        #    if Questionnaire.objects.filter(id=self.ques.id, state='Q'):
        #        Questionnaire.objects.filter(id=self.ques.id).update(state='S')
        #    elif Questionnaire.objects.filter(id=self.ques.id, state='AR'):
        #        Questionnaire.objects.filter(id=self.ques.id).update(state='AS')
        #    Report_Sent.objects.create(questionnaire_id=self.ques.id,
        #                               case_id=self.case.id,
        #                               date=now,
        #                               report=hl7file.getvalue(),
        #                               report_type='VAERS')
        hl7file.close()
    


def transmit_ftp(fileObj, filename):
        '''
        Upload a file using cleartext FTP.  
        '''
        log.info('Transmitting case report via FTP')
        log.debug('FTP server: %s' % PHINMS_SERVER)
        log.debug('FTP user: %s' % PHINMS_USER)
        log.debug('Attempting to connect...')
        conn = ftplib.FTP(PHINMS_SERVER, PHINMS_USER, PHINMS_PASSWORD)
        log.debug('Connected to %s' % PHINMS_SERVER)
        log.debug('CWD to %s' % PHINMS_PATH)
        conn.cwd(PHINMS_PATH)
        command = 'STOR ' + filename
        try:
            conn.storlines(command, fileObj)
            log.info('Successfully uploaded VAERS HL7 message to PHINMS Server')
        except BaseException as e:
            log.error('FTP ERROR: %s' % e)
            return False
        return True
