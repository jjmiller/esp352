# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from ESP.utils.utils import LoadFixtureData
from ESP import settings
from django.db import models, migrations
from django.core.management import call_command
from django.conf import settings

db_engine = settings.DATABASES['default']['ENGINE']

if db_engine == 'django.db.backends.postgresql_psycopg2':
  class Migration(migrations.Migration):
    dependencies = [
        ('vaers', '0004_auto_20200210_1729'),
        ('static', '0009_DXCodesforVaers'),
    ]

    operations = [
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'vaers', 'fixtures', 'vaers_fixtures') + ".json")),
        migrations.RunSQL("select setval('vaers_adverseevent_id_seq',(select max(id) from vaers_adverseevent)+1);"),
        migrations.RunSQL("select setval('vaers_allergykeyword_id_seq',(select max(id) from vaers_allergykeyword)+1);"),
        migrations.RunSQL("select setval('vaers_allergyrule_id_seq',(select max(id) from vaers_allergyrule)+1);"),
        migrations.RunSQL("select setval('vaers_case_adverse_events_id_seq',(select max(id) from vaers_case_adverse_events)+1);"),
        migrations.RunSQL("select setval('vaers_case_id_seq',(select max(id) from vaers_case)+1);"),
        migrations.RunSQL("select setval('vaers_case_immunizations_id_seq',(select max(id) from vaers_case_immunizations)+1);"),
        migrations.RunSQL("select setval('vaers_case_prior_immunizations_id_seq',(select max(id) from vaers_case_prior_immunizations)+1);"),
        migrations.RunSQL("select setval('vaers_diagnosticseventrule_dxrule_vxs_id_seq',(select max(id) from vaers_diagnosticseventrule_dxrule_vxs)+1);"),
        migrations.RunSQL("select setval('vaers_diagnosticseventrule_heuristic_defining_codes_id_seq',(select max(id) from vaers_diagnosticseventrule_heuristic_defining_codes)+1);"),
        migrations.RunSQL("select setval('vaers_diagnosticseventrule_heuristic_discarding_codes_id_seq',(select max(id) from vaers_diagnosticseventrule_heuristic_discarding_codes)+1);"),
        migrations.RunSQL("select setval('vaers_diagnosticseventrule_id_seq',(select max(id) from vaers_diagnosticseventrule)+1);"),
        migrations.RunSQL("select setval('vaers_excludeddx_code_id_seq',(select max(id) from vaers_excludeddx_code)+1);"),
        migrations.RunSQL("select setval('vaers_labrule_id_seq',(select max(id) from vaers_labrule)+1);"),
        migrations.RunSQL("select setval('vaers_prescriptionrule_id_seq',(select max(id) from vaers_prescriptionrule)+1);"),
        migrations.RunSQL("select setval('vaers_questionnaire_id_seq',(select max(id) from vaers_questionnaire)+1);"),
        migrations.RunSQL("select setval('vaers_report_sent_id_seq',(select max(id) from vaers_report_sent)+1);"),
        migrations.RunSQL("select setval('vaers_sender_id_seq',(select max(id) from vaers_sender)+1);"),
    ]

else:
  class Migration(migrations.Migration):
    dependencies = [
        ('vaers', '0004_auto_20200210_1729'),
        ('static', '0009_DXCodesforVaers'),
    ]

    operations = [
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'vaers', 'fixtures', 'vaers_fixtures') + ".json")),
    ]
