# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.conf.urls import include

from ESP.vaers import views

app_name = 'vaers'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^casetable$', views.list_cases),
    url(r'^notify/(?P<id>\d+)/$', views.notify),
    url(r'^report$', views.report, name='report'),

    # Vaccine and Manufacturer Mapping
    url(r'^vaccines/', include('ESP.vaers.vaccine.urls')),
    
    # ptype can be digest or case.  digest takes a digest value for id, case takes a questionnaire id for id.
    url(r'^(?P<ptype>case|digest)/(?P<id>\w*)/$', views.case_details, name='present_case'),

    #line listing report
    url(r'^download/', views.download_vae_listing, name='download_listing'),    
]

