# Generated by Django 2.2.10 on 2020-02-06 17:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conf', '0007_cdaerror_cdamapping_cdaxpath'),
    ]

    operations = [
        migrations.AlterField(
            model_name='labtestmap',
            name='excluded_indeterminate_strings',
            field=models.ManyToManyField(blank=True, limit_choices_to={'applies_to_all': True, 'indicates': 'ind'}, related_name='excluded_indeterminate_set', to='conf.ResultString'),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='excluded_negative_strings',
            field=models.ManyToManyField(blank=True, limit_choices_to={'applies_to_all': True, 'indicates': 'neg'}, related_name='excluded_negative_set', to='conf.ResultString'),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='excluded_positive_strings',
            field=models.ManyToManyField(blank=True, limit_choices_to={'applies_to_all': True, 'indicates': 'pos'}, related_name='excluded_positive_set', to='conf.ResultString'),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='extra_indeterminate_strings',
            field=models.ManyToManyField(blank=True, limit_choices_to={'applies_to_all': False, 'indicates': 'ind'}, related_name='extra_indeterminate_set', to='conf.ResultString'),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='extra_negative_strings',
            field=models.ManyToManyField(blank=True, limit_choices_to={'applies_to_all': False, 'indicates': 'neg'}, related_name='extra_negative_set', to='conf.ResultString'),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='extra_positive_strings',
            field=models.ManyToManyField(blank=True, limit_choices_to={'applies_to_all': False, 'indicates': 'pos'}, related_name='extra_positive_set', to='conf.ResultString'),
        ),
    ]
