'''
                              ESP Health Project
Configuration Module
Views

@authors: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@copyright: (c) 2009 Channing Laboratory
@license: LGPL
'''


import operator

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.urls import reverse
from django.shortcuts import render, redirect
from ESP.conf.models import IgnoredCode
from ESP.conf.models import LabTestMap
from ESP.utils.utils import log
from ESP.hef.base import BaseLabResultHeuristic, PrescriptionHeuristic, DiagnosisHeuristic
from ESP.ui.other_labs import STANDARD_LABS

@login_required
def heuristic_mapping_report(request):
    values = {'title': 'Abstract Lab Mapping Summary'}
    mapped = []
    unmapped = []
    test_names = []
    ## get all test names from heuristics and other labs
    lab_results = BaseLabResultHeuristic.get_all()
    for lrh in BaseLabResultHeuristic.get_all():
        test_names.append(lrh.test_name)
    for sname in STANDARD_LABS:
        if sname not in test_names:
            test_names.append(sname)

    for test_name in test_names: 
        maps = LabTestMap.objects.filter(test_name=test_name)
        if not maps:
            # if unmapped doesnt already have test name then add it
            if not unmapped.__contains__(test_name):
                unmapped.append( (test_name) )
            continue
        codes = maps.values_list('native_code', flat=True)
        # if mapped doesnt already contain (heuristic.test_name, codes) then add it
        # it is sorted already,for each in mapped get item compare with test name 
        # add first one
        if not mapped: mapped.append( (test_name, codes) )
        # loop through mapped make sure is not already there
        else:
            contains=False
            for index in range(len(mapped)):  
                if mapped.__getitem__(index).__contains__((test_name)):
                    contains=True
                    continue
            if not contains: 
                mapped.append( (test_name, codes) )              
        
    mapped.sort(key=operator.itemgetter(0))
    unmapped.sort()
    values['mapped'] = mapped
    values['unmapped'] = unmapped
    return render(request, 'conf/heuristic_mapping_report.html', values)

@login_required
def heuristic_reportables(request):
    values = {'title': 'Heuristic Reportables Summary'}
    
    reportable_rx = []
    reportable_dx = []
    reportable_lx = []
    
    for heuristic in BaseLabResultHeuristic.get_all():
        if heuristic.test_name not in reportable_lx:
            reportable_lx.append(heuristic.test_name)

    for sname in STANDARD_LABS:
        if sname not in reportable_lx:
            reportable_lx.append(sname)

    for heuristic in DiagnosisHeuristic.get_all():
        if isinstance(heuristic, DiagnosisHeuristic):
            for dx_code in heuristic.dx_code_queries:
                if dx_code not in reportable_dx:
                    reportable_dx.append(dx_code)
            
    for heuristic in PrescriptionHeuristic.get_all():
        if isinstance(heuristic, PrescriptionHeuristic):
            for drug in heuristic.drugs:
                if drug not in reportable_rx:
                    reportable_rx.append(drug)
    
    reportable_lx.sort()
    reportable_dx.sort(key=lambda x: x.verbose_name)
    reportable_rx.sort()
    
    values['reportable_lx'] = reportable_lx
    values['reportable_dx'] = reportable_dx
    values['reportable_rx'] = reportable_rx
    
    return render(request, 'conf/heuristic_reportables.html', values)


@user_passes_test(lambda u: u.is_staff)
def ignore_code(request, native_code):
    '''
    Display Unmapped Labs report generated from cache
    '''
    if request.method == 'POST':
        #ic_obj, created = IgnoredCode.objects.get_or_create(native_code=native_code.lower())
        ic_obj, created = IgnoredCode.objects.get_or_create(native_code=native_code) # Why was this lower() before?
        if created:
            ic_obj.save()
            msg = 'Native code "%s" has been added to the ignore list' % native_code
        else:
            msg = 'Native code "%s" is already on the ignore list' % native_code
        messages.add_message(request,messages.INFO,msg)
        log.debug(msg)
        return redirect(reverse('unmapped_labs_report'))
    else:
        values = {
            'title': 'Unmapped Lab Tests Report',
            "request":request,
            'native_code': native_code,
            }
        return render(request, 'conf/confirm_ignore_code.html', values)
    
