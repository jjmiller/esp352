'''
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                                  ESP Health
                                Django Settings

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'''

import logging
import os

from configobj import ConfigObj, flatten_errors
from validate import Validator

TOPDIR = os.path.dirname(__file__)
PACKAGE_ROOT = os.path.normpath(os.path.join(TOPDIR, '..'))
# One could also set CONFIG_FOLDER manually, to something like '/etc/esp', if 
# so desired.
CONFIG_FOLDER = os.path.join(PACKAGE_ROOT, 'etc')


#-------------------------------------------------------------------------------
#
# Process INI files
#
#-------------------------------------------------------------------------------
validator = Validator()
application_ini = os.path.join(CONFIG_FOLDER, 'application.ini')
application_spec = ConfigObj(os.path.join(TOPDIR, 'application.spec.ini'), interpolation=False, list_values=False)
secrets_ini = os.path.join(CONFIG_FOLDER, 'secrets.ini')
secrets_spec = os.path.join(TOPDIR, 'secrets.spec.ini')
secrets = ConfigObj(secrets_ini, configspec=secrets_spec)
config = ConfigObj(application_ini, configspec=application_spec, interpolation=False)

bad_config = False
for ini_file, conf_obj in [(secrets_ini, secrets), (application_ini, config)]:
    if not os.path.exists(ini_file):
        print('Could not find configuration file %s' % ini_file)
        bad_config = True
    results = conf_obj.validate(validator, copy=True)
    if not results:
        for (section_list, key, _) in flatten_errors(config, results):
            print('%s:' % ini_file)
            if key is not None:
                print('    The "%s" key in the section "%s" failed validation' % (key, ', '.join(section_list)))
            else:
                print('    The following section was missing:%s ' % ', '.join(section_list))
        bad_config = True
if bad_config:
    print()
    print('Configuration problems will limit ESP functionality.')
    print()

#===============================================================================
#
# Configuration Variables
#
#===============================================================================

version_path =  os.path.join(TOPDIR, 'version.txt')
VERSION = open(version_path).readline().strip()

SECRET_KEY = secrets['General']['secret_key']
DEBUG = config['General']['django_debug']
ICD10_SUPPORT = config['General']['icd10_support']
CODEDIR = TOPDIR
ADMINS = [(i,i) for i in config['General']['admins']]
MANAGERS = [(i,i) for i in config['General']['managers']]
SITE_NAME = config['General']['site_name']
SYSTEM_STATUS = config['General']['system_status']
DATA_DIR = config['General']['data_folder']
FAKE_PATIENT_SURNAME = config['Reporting']['fake_patient_surname']
FAKE_PATIENT_MRN = config['Reporting']['fake_patient_mrn']
NEVER_SEND_RESULT_STRINGS = config['Reporting']['never_send_result_strings']
DATABASE_OPTIONS = config['Database_options'] if 'Database_options' in config else {}
DATABASES = {
    'default': {
        'ENGINE': config['Database']['engine'],
        'NAME': config['Database']['db_name'],
        'USER': config['Database']['username'],
        'PASSWORD': secrets['General']['database_password'],
        'HOST': config['Database']['host'],
        'PORT': config['Database']['port'],
        'OPTIONS': DATABASE_OPTIONS
        },
    }
TRANSACTION_ROW_LIMIT = config['Database']['transaction_row_limit']
USE_FILENAME_DATE = config['ETL']['use_filename_date']
ETL_SOURCE = config['ETL']['source']
ETL_USE_FTP = config['ETL']['retrieve_files'] # Use built-in FTP function to retrieve Epic files
ETL_USE_SFTP = config['ETL']['use_sftp'] # Use built-in FTP function to retrieve Epic files
ETL_ARCHIVE = config['ETL']['archive'] # Should ETL files be archived after they have been loaded?
FTP_SERVER = config['ETL']['server']
FTP_USER = config['ETL']['username']
FTP_PASSWORD = secrets['General']['etl_server_password']
FTP_PATH = config['ETL']['path']
SFTP_PORT = config['ETL']['sftp_port']
SFTP_SERVER = config['ETL']['sftp_server']
SFTP_USER = config['ETL']['sftp_username']
SFTP_PASSWORD = secrets['General']['etl_sftp_server_password']
SFTP_PATH = config['ETL']['sftp_path']
ETL_MEDNAMEREVERSE = config['ETL']['medname_field7']
LOAD_REPORT_DIR = config['ETL']['load_report_dir']
MAX_BMI = config['ETL']['max_bmi']
MAX_WTKG = config['ETL']['max_wtkg']
MAX_HTCM = config['ETL']['max_htcm']
UPLOAD_SERVER = config['Reporting']['upload_server']
UPLOAD_USER = config['Reporting']['upload_username']
UPLOAD_PASSWORD = secrets['General']['upload_password']
UPLOAD_PATH = config['Reporting']['upload_path']
PHINMS_SERVER = config['Reporting']['phinms_server']
PHINMS_USER = config['Reporting']['phinms_username']
PHINMS_PASSWORD = secrets['General']['phinms_password']
PHINMS_PATH = config['Reporting']['phinms_path']
EMRUPDATE_SERVER = config['Reporting']['update_emr_server']
EMRUPDATE_USER = config['Reporting']['update_emr_username']
EMRUPDATE_PASSWORD = secrets['General']['update_password']
EMRUPDATE_PATH = config['Reporting']['update_emr_path']
VAERS_LINELIST_PATH = config['Reporting']['vaers_linelist_path']
VAERS_SEND_REPORT = config['Reporting']['vaers_send_report']
VAERS_UPDATE_EMR = config['Reporting']['vaers_update_emr']
VAERS_OVERRIDE_CLINICIAN_REVIEWER = config['Reporting']['vaers_override_clinician_reviewer']
VAERS_AUTOSENDER = config['Reporting']['vaers_autosender']
HL7_DIR = os.path.join(DATA_DIR, 'hl7')
SITE_ID = 1 # This probably does not need to be configurable
TIME_ZONE = config['General']['time_zone']
LANGUAGE_CODE = config['General']['language_code']
#in python, date formats are preceded by percent signs
#in django web templates, date formats are similar, but without the percent signs, 
#  and "date_format" is defined as the default source for date formating.
PY_DATE_FORMAT = config['General']['date_format']
DATE_FORMAT = (config['General']['date_format']).replace('%','')
ROWS_PER_PAGE = config['Web']['rows_per_page']
#ALLOWED HOSTS are needed when Debug = False as a security measure.
# Should be a list of hostname(s) that the application is served from
ALLOWED_HOSTS = [i for i in config['Web']['allowed_hosts']]
# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(TOPDIR, 'media')
MEDIA_URL = config['Web']['media_url']
STATIC_ROOT = os.path.join(MEDIA_ROOT,'static')
STATIC_URL = config['Web']['static_url']

MIDDLEWARE = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)
ROOT_URLCONF = 'ESP.urls'
LOGIN_URL = config['Web']['login_url']
LOGOUT_REDIRECT_URL = '/'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
                os.path.join(CONFIG_FOLDER, 'templates'),
                os.path.join(TOPDIR, 'templates'),
                os.path.join(TOPDIR, 'templates/esp'),
                os.path.join(TOPDIR, 'templates/pages/vaers'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'ESP.conf.context_processors.path_definitions'
            ],
        },
    },
]

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_tables2',
    'ESP.static',
    'ESP.conf',
    'ESP.emr',
    'ESP.hef',
    'ESP.nodis',
    'ESP.vaers',
    'ESP.ui',
    'rest_framework',
    'rest_framework_xml',
    'rest_framework.authtoken',
    'ESP.cda',
)

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 8,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

CASE_REPORT_OUTPUT_FOLDER = os.path.join(DATA_DIR, 'case_reports')
CASE_REPORT_MDPH = config['Reporting']['use_mdph_format']
CASE_REPORT_TEMPLATE = config['Reporting']['template']
CASE_REPORT_FILENAME_FORMAT = config['Reporting']['filename_format']
CASE_REPORT_BATCH_SIZE = config['Reporting']['cases_per_message']
CASE_REPORT_TRANSMIT = config['Reporting']['transport']
CASE_REPORT_TRANSPORT_SCRIPT = config['Reporting']['transport_script']
FILTER_CENTERS = config['Reporting']['filter_centers']
SHOW_SURVEYS = config['Reporting']['show_surveys']
REQUEUE_LAG_DAYS = config['Reporting']['lag_days']
REQUEUE_REF_DATE = config['Reporting']['requeue_ref_date']

TEST_RUNNER = 'django.test.runner.DiscoverRunner'

EMAIL_HOST = config['Email']['host']
EMAIL_HOST_USER = config['Email']['username']
EMAIL_HOST_PASSWORD = secrets['General']['email_password']
EMAIL_PORT = config['Email']['port']
EMAIL_USE_TLS = config['Email']['use_tls']
SERVER_EMAIL = config['Email']['server_email']
DEFAULT_FROM_EMAIL = config['Email']['default_from_email']
EMAIL_SUBJECT_PREFIX = config['Email']['subject_prefix']
VAERS_NOTIFICATION_RECIPIENT = 'someone@example.com'
BATCH_ETL = config['Batch']['etl']
BATCH_MAIL_STATUS_REPORT = config['Batch']['mail_status_report']
STATUS_REPORT_TYPE = config['Batch']['status_report_type']
BATCH_GENERATE_CASE_REPORT = config['Batch']['generate_case_report']
BATCH_TRANSMIT_CASE_REPORT = config['Batch']['transmit_case_report']
# Logging levels, so we can use strings for configuration
_levels = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL,
    }
ROW_LOG_COUNT = config['Logging']['row_log_count']
LOG_FILE = config['Logging']['log_file'] # Used only if LOG_LEVEL_FILE != None
LOG_FORMAT_CONSOLE = config['Logging']['log_format_console']
LOG_FORMAT_FILE = config['Logging']['log_format_file']
LOG_FORMAT_SYSLOG = config['Logging']['log_format_syslog']
# BEWARE: If you set the log level to DEBUG, *copious* info will be logged!
LOG_LEVEL_CONSOLE = _levels[config['Logging']['log_level_console']]
LOG_LEVEL_FILE = _levels[config['Logging']['log_level_file']]
LOG_LEVEL_SYSLOG = _levels[config['Logging']['log_level_syslog']]

# Sphinx 0.9.9
SPHINX_API_VERSION = 0x116

# queryset_iterator() support
QUERYSET_ITERATOR_CHUNKSIZE = config['General']['queryset_iterator_chunksize']

#
#--- HEF
#
HEF_THREAD_COUNT = config['HEF']['thread_count']

#
#--- Site
#
SITE_HEADER = config['Site']['site_header']
CASE_REPORT_SITE_NAME = config['Site']['case_report_site_name']
SITE_CLIA = config['Site']['site_clia']
SITE_LAST_NAME = config['Site']['site_last_name']
SITE_FIRST_NAME = config['Site']['site_first_name']
SITE_ADDRESS1 = config['Site']['site_address1']
SITE_ADDRESS2 = config['Site']['site_address2']
SITE_CITY = config['Site']['site_city']
SITE_STATE = config['Site']['site_state']
SITE_ZIP = config['Site']['site_zip']
SITE_COUNTRY = config['Site']['site_country']
SITE_EMAIL = config['Site']['site_email']
SITE_AREA_CODE = config['Site']['site_area_code']
SITE_TEL_NUMERIC = config['Site']['site_tel_numeric']
SITE_TEL_EXT = config['Site']['site_tel_ext']
SITE_APP_NAME = config['Site']['site_app_name']
SITE_SENDING_FACILITY = config['Site']['site_sending_facility']
SITE_COMMENTS = config['Site']['site_comments']

#--- Make Fakes 
LOAD_DRIVER_LABS = config['Make_Fakes']['load_driver_labs']
START_DATE = config['Make_Fakes']['start_date']
END_DATE = config['Make_Fakes']['end_date']
POPULATION_SIZE = config['Make_Fakes']['population_size']

MIN_ENCOUNTERS_PER_PATIENT = config['Make_Fakes']['min_encounters_per_patient']
ENCOUNTERS_PER_PATIENT = config['Make_Fakes']['encounters_per_patient']

MAXDX_CODE = config['Make_Fakes']['maxdx_code']
DX_CODE_PCT = config['Make_Fakes']['dx_code_pct']

MIN_LAB_TESTS_PER_PATIENT = config['Make_Fakes']['min_lab_tests_per_patient']
LAB_TESTS_PER_PATIENT = config['Make_Fakes']['lab_tests_per_patient']

MIN_LAB_ORDERS_PER_PATIENT = config['Make_Fakes']['min_lab_orders_per_patient']
LAB_ORDERS_PER_PATIENT = config['Make_Fakes']['lab_orders_per_patient']

MIN_MEDS_PER_PATIENT = config['Make_Fakes']['min_meds_per_patient']
MEDS_PER_PATIENT = config['Make_Fakes']['meds_per_patient']

IMMUNIZATION_PCT = config['Make_Fakes']['immunization_pct']
IMMUNIZATIONS_PER_PATIENT = config['Make_Fakes']['immunizations_per_patient']

MAX_PREGNANCIES = config['Make_Fakes']['max_pregnancies']
CURRENTLY_PREG_PCT = config['Make_Fakes']['currently_preg_pct']

MAX_ALLERGIES = config['Make_Fakes']['max_allergies']
MAX_PROBLEMS = config['Make_Fakes']['max_problems']
MAX_SOCIALHISTORY = config['Make_Fakes']['max_socialhistory']

MAX_DIABETES = config['Make_Fakes']['max_diabetes']
MAX_ILI = config['Make_Fakes']['max_ili']
MAX_DIABETES_ILI = config['Make_Fakes']['max_diabetes_ili']


REST_FRAMEWORK = {
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework_xml.parsers.XMLParser',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
}
