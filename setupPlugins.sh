#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                                 ESP Health
#                                Setup Script
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This script must be run by a user with write permission on the 'esphealth'
# folder.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

USAGE_MSG="usage: Run setupPlugins.sh without options to configure and run ESP plugin installation.
You will be provided an action list for your ESP version, which you may configure to install, update or uninstall plugins available for your ESP version.

-?  Show this usage message."

function usage () {
    # Must be in quotes for multiline formatting
    echo "$USAGE_MSG"
}

function activate_virtualenv () {
    . bin/activate
    export PIP_RESPECT_VIRTUALENV=true
    export PIP_REQUIRE_VIRTUALENV=true
}

function get_compatible () {
    while IFS=. read major minor maint; do         
        wget -O ./tempSetup/modules.txt "https://gitlab.com/ESP-Project/ESP-Plugin-Compatibility/raw/master/$major.$minor-plugins.txt"
    done < ESP/version.txt #TODO: script needs to break here if it can't reach bitbucket
    sort ./tempSetup/modules.txt -o ./tempSetup/modules.txt
}

function get_installed () {
    activate_virtualenv
    pip list -e --format=columns > ./tempSetup/installedPlugins.txt
    sort ./tempSetup/installedPlugins.txt -o ./tempSetup/installedPlugins.txt
    deactivate
}

function make_configfile () {
    cat /dev/null >| ./tempSetup/instUpdtPlugins.txt
    while IFS="=" read pname spare ver; do
        status="not"
        updt="$ver"
        pname=${pname//_/-}
        if [[ $pname == "" ]]
        then
            continue
        fi
        grepname="-$pname"
        while read line; do
            if [[ $line =~ $grepname ]]
            then
                if grep -q "${ver:1}" <<< "$line"
                then
                    IFS=' ,' read egg status src <<< "${line//[()]/v}"
                    status="$status"
                    updt="Current"
                else
                    IFS=' ,' read egg status src <<< "${line//[()]/v}"
                    status="$status"
                    updt="$ver"
                fi
            fi
        done < ./tempSetup/installedPlugins.txt
        IFS="=" 
        echo "$pname, $status, $updt" >> ./tempSetup/instUpdtPlugins.txt       
    done < ./tempSetup/modules.txt
}

function install () {
    if [ -s ./tempSetup/installplugins.txt ]
    then
        activate_virtualenv
        pip install -q -r ./tempSetup/installplugins.txt
        deactivate
    fi
}

function uninstall () {
    if [ -s ./tempSetup/uninstallplugins.txt ]
    then
        activate_virtualenv
        while read line; do
            pip uninstall -y -q $line
        done < ./tempSetup/uninstallplugins.txt &&\
        ./tempSetup/rmPlugins.sh  ## this only runs if the pip uninstall runs without error
        deactivate
    fi
}

function runit () {
    mkdir ./tempSetup &&\
    get_compatible &&\
    get_installed &&\
    make_configfile &&\
    activate_virtualenv &&\
    ./bin/python instUpdtPlugins.py &&\
    deactivate &&\
    install &&\
    uninstall
    rm -rf ./tempSetup
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Main Logic
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# TODO: Add sanity checks for write permission on relevant folders
#
while getopts "" options; do
  case $options in
    \? ) usage
         exit 1;;
  esac
done
runit
